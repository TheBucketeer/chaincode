package trackTopology.errors.blockchain.unchecked.invaliddata;

public class BlockchainInvalidDataException extends RuntimeException {

    private BlockchainInvalidDataException(String s) {
        super("Error retrieving data from blockchain: " + s);
    }

    BlockchainInvalidDataException(String dataName, String stateStr) {
        super("Invalid " + dataName + ": " + stateStr);
    }

    BlockchainInvalidDataException(String dataName, String currentStateStr, String intendedStateStr) {
        super("Invalid " + dataName + ", current: " + currentStateStr + ", intended" + intendedStateStr);
    }

    public BlockchainInvalidDataException(Throwable cause) {
        this(cause.getMessage());
    }
}
