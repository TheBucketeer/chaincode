package trackTopology.errors.tracklayout.setup;

import trackTopology.Section;
import trackTopology.Sensor;
import trackTopology.errors.tracklayout.TrackLayoutException;

public class MissingSensorSideException extends TrackLayoutException {
    public MissingSensorSideException(Sensor sensor, Section section) {
        super(sensor.getAssetId() + "is delimiter of " + section.getAssetId() + " but does not have it registered as one its sides");
    }
}
