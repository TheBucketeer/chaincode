package trackTopology.errors.runtime.checked;

import org.hyperledger.fabric.shim.ChaincodeStub;
import trackTopology.Sensor;
import trackTopology.Train;

import java.util.LinkedList;

public class SensorReadingConflictException extends TrainManagementException {
    public SensorReadingConflictException(ChaincodeStub chaincodeStub, Sensor s, LinkedList<Train> candidates) {
        super("Could not determine which train triggered " + s.getAssetId() + ", >1 candidates:" + "\n" + dumpTrains(chaincodeStub, candidates));
    }

    private static String dumpTrains(ChaincodeStub chaincodeStub, LinkedList<Train> trains) {
        StringBuilder trainDump = new StringBuilder();
        for (Train train : trains) {
            trainDump.append(train.dump(chaincodeStub)).append("\n");
        }
        return trainDump.toString();
    }
}
