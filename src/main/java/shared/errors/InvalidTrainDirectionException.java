package shared.errors;

public class InvalidTrainDirectionException extends InvalidDataException {
    public InvalidTrainDirectionException(String directionStr) {
        super("train direction", directionStr);
    }
}
