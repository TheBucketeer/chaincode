package trackTopology;

import trackTopology.errors.tracklayout.NotAdjacentException;
import trackTopology.errors.tracklayout.setup.IncorrectNumberOfSensorSidesException;

import java.util.List;

import static shared.GlobalConfig.SENSOR_AID_PREFIX;

public class Sensor extends TrackLayoutComponent {
    private List<Section> sides;

    Sensor(int sId) {
        this.aId = String.format("%s%03d", SENSOR_AID_PREFIX, sId);
    }

    Sensor(Sensor old) {
        this.aId = old.aId;
    }

    void setSides(List<Section> sides) throws IncorrectNumberOfSensorSidesException {
        this.sides = sides;
        if (this.sides.size() != 2) {
            throw new IncorrectNumberOfSensorSidesException(this.sides.size());
        }
    }

    List<Section> getSides() {
        return this.sides;
    }

    public Section getNext(Section sIn) throws IncorrectNumberOfSensorSidesException, NotAdjacentException {
        if (this.sides.size() != 2) {
            throw new IncorrectNumberOfSensorSidesException(this.sides.size());
        }
        if (this.sides.get(0).equals(sIn)) {
            return this.sides.get(1);
        } else if (this.sides.get(1).equals(sIn)) {
            return this.sides.get(0);
        } else {
            throw new NotAdjacentException(this.sides.get(0), sIn);
        }
    }

    public String toString() {
        return this.aId + " has sides " + this.sides.get(0).getAssetId() + " and " + this.sides.get(1).getAssetId();
    }
}