package trackTopology.errors.blockchain.checked.datanotfound;

import trackTopology.Section;


public class SectionReserveeNotFoundException extends BlockchainDataNotFoundException {
    public SectionReserveeNotFoundException(Section section) {
        super("Reservee of " + section.getAssetId() + " not found on blockchain.");
    }
}
