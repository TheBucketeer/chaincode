package shared.errors;

public class InvalidLevelCrossingStateException extends InvalidDataException {
    public InvalidLevelCrossingStateException(boolean state) {
        this(String.valueOf(state));
    }

    public InvalidLevelCrossingStateException(String stateStr) {
        super("level crossing state", stateStr);
    }
}
