package trackTopology.errors.blockchain.checked.datanotfound;

import trackTopology.Switch;

public class SwitchStateNotFoundException extends BlockchainDataNotFoundException {
    public SwitchStateNotFoundException(Switch sw) {
        super("Switch state of " + sw.getAssetId() + " not found on blockchain.");
    }
}
