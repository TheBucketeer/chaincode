package chaincode.blockchainVariables;

import org.hyperledger.fabric.shim.ChaincodeStub;
import shared.assetdata.TrainConfig.TRAIN_SPEED;
import shared.errors.InvalidTrainSpeedException;
import trackTopology.Train;
import trackTopology.errors.blockchain.unchecked.invaliddata.InvalidTrainSpeedOnBCException;

import static shared.GlobalConfig.ASSET_INTENDED_STATE_POSTFIX;
import static shared.GlobalConfig.TRAIN_SPEED_KEY_POSTFIX;

public class TrainSpeedVariableSplit extends BlockchainVariable<TRAIN_SPEED> {
    private Train train;
    private boolean isIntended;

    public TrainSpeedVariableSplit(Train train) {
        this(train, false);
    }

    public TrainSpeedVariableSplit(Train train, boolean isIntended) {
        super();
        this.train = train;
        this.isIntended = isIntended;
    }

    public TrainSpeedVariableSplit(TrainSpeedVariableSplit old) {
        super(old);
        this.train = old.train;
    }

    TRAIN_SPEED fromBcString(ChaincodeStub chaincodeStub, String bcString) {
        try {
            return TRAIN_SPEED.fromString(bcString);
        } catch (InvalidTrainSpeedException e) {
            throw new InvalidTrainSpeedOnBCException(bcString);
        }
    }

    String toBcString(TRAIN_SPEED speed) {
        return speed.toString();
    }

    String getKey() {
        return this.train.getAssetId() + TRAIN_SPEED_KEY_POSTFIX + (this.isIntended ? ASSET_INTENDED_STATE_POSTFIX : "");
    }
}
