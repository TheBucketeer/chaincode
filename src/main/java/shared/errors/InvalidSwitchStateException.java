package shared.errors;

public class InvalidSwitchStateException extends InvalidDataException {
    public InvalidSwitchStateException(String stateStr) {
        super("switch state", stateStr);
    }

    public InvalidSwitchStateException(boolean state) {
        this(String.valueOf(state));
    }
}
