package chaincode.blockchainVariables;

import org.hyperledger.fabric.shim.ChaincodeStub;
import shared.assetdata.SectionConfig.SECTION_WEIGHT;
import shared.errors.InvalidSectionWeightException;
import trackTopology.Section;
import trackTopology.errors.blockchain.unchecked.invaliddata.InvalidSectionWeightOnBCException;

import static shared.GlobalConfig.SECTION_WEIGHT_KEY_POSTFIX;

public class SectionWeightVariable extends BlockchainVariable<SECTION_WEIGHT> {
    private Section section;

    public SectionWeightVariable(Section section) {
        super();
        this.section = section;
    }

    SECTION_WEIGHT fromBcString(ChaincodeStub chaincodeStub, String bcString) {
        try {
            return SECTION_WEIGHT.fromString(bcString);
        } catch (InvalidSectionWeightException e) {
            throw new InvalidSectionWeightOnBCException(bcString);
        }
    }

    String toBcString(SECTION_WEIGHT weight) {
        return weight.toString();
    }

    String getKey() {
        return this.section.getAssetId() + SECTION_WEIGHT_KEY_POSTFIX;
    }
}
