package trackTopology;

import chaincode.BlocktrainCC;
import chaincode.EventHelper;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import shared.assetdata.SectionConfig.SECTION_WEIGHT;
import trackTopology.errors.runtime.unchecked.TrainManagementRuntimeException;
import trackTopology.errors.tracklayout.NotAdjacentException;
import trackTopology.errors.tracklayout.TrackLayoutException;
import trackTopology.errors.tracklayout.assetnotfound.SectionNotFoundException;

import java.util.HashMap;

import static chaincode.BlocktrainCC.ignoreBc;
import static chaincode.BlocktrainCC.timestamps;
import static org.assertj.core.api.AssertionsForClassTypes.failBecauseExceptionWasNotThrown;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static shared.GlobalConfig.TEST_TRACK_LAYOUT_JSON;
import static trackTopology.Section.getWeightOfRoute;
import static trackTopology.Section.routeIsCyclic;

//import chaincode.blockchainVariables.BlockchainVariable;

@RunWith(MockitoJUnitRunner.class)
public class SectionTest {
    private TrackLayout tracklayout;
    private final HashMap<String, String> blockchainStateMock = new HashMap<>();
    @Spy
    BlocktrainCC blocktrainCC;
    @Mock
    ChaincodeStub chaincodeStub;

    @Before
    public void doBefore() {
        timestamps.put(this.chaincodeStub, (long) 0);
        ignoreBc.put(this.chaincodeStub, true);
//        BlockchainVariable.setBlocktrainCC(this.blocktrainCC);
        EventHelper.setParent(this.blocktrainCC);

        this.tracklayout = new TrackLayout(this.chaincodeStub, TEST_TRACK_LAYOUT_JSON);
        // Test cases are based on a known weight of 1 for all sections
        for (Section section : this.tracklayout.getSections()) {
            section.setWeight(this.chaincodeStub, SECTION_WEIGHT.fromInt(1));
        }
        BlocktrainCC.trackLayouts.put(chaincodeStub, this.tracklayout);
    }

    @Test
    public void getRouteWeightTest() throws TrackLayoutException {
        Section a, b, c;
        SECTION_WEIGHT oldWeightA, oldWeightB, oldWeightC;
        a = this.tracklayout.getSectionByAId("Sc009");
        b = this.tracklayout.getSectionByAId("Sc010");
        c = this.tracklayout.getSectionByAId("Sc011");
        Section[] route = {a, b, c};
        oldWeightA = a.getWeight(this.chaincodeStub);
        oldWeightB = b.getWeight(this.chaincodeStub);
        oldWeightC = c.getWeight(this.chaincodeStub);
        a.setWeight(this.chaincodeStub, SECTION_WEIGHT.fromInt(5));
        b.setWeight(this.chaincodeStub, SECTION_WEIGHT.fromInt(2));
        c.setWeight(this.chaincodeStub, SECTION_WEIGHT.fromInt(17));
        assertThat(getWeightOfRoute(this.chaincodeStub, route)).isEqualTo(24);
        a.setWeight(this.chaincodeStub, oldWeightA);
        b.setWeight(this.chaincodeStub, oldWeightB);
        c.setWeight(this.chaincodeStub, oldWeightC);
    }

    @Test
    public void routeIsCyclicShouldReturnTrue() throws TrackLayoutException {
        Section[] route = {
                tracklayout.getSectionByAId("Sc009"),
                tracklayout.getSectionByAId("Sc010"),
                tracklayout.getSectionByAId("Sc011"),
                tracklayout.getSectionByAId("Sc012"),
                tracklayout.getSectionByAId("Sc008"),
                tracklayout.getSectionByAId("Sc000"),
                tracklayout.getSectionByAId("Sc001"),
                tracklayout.getSectionByAId("Sc002"),
                tracklayout.getSectionByAId("Sc003"),
                tracklayout.getSectionByAId("Sc004"),
                tracklayout.getSectionByAId("Sc005"),
                tracklayout.getSectionByAId("Sc006"),
                tracklayout.getSectionByAId("Sc009"),
                tracklayout.getSectionByAId("Sc010")
        };
        assertThat(routeIsCyclic(route)).isEqualTo(true);
    }

    @Test
    public void routeIsCyclicShouldReturnFalse() throws TrackLayoutException {
        Section[] route = new Section[]{
                tracklayout.getSectionByAId("Sc009"),
                tracklayout.getSectionByAId("Sc010"),
                tracklayout.getSectionByAId("Sc011"),
                tracklayout.getSectionByAId("Sc012")
        };
        assertThat(routeIsCyclic(route)).isEqualTo(false);
        route = new Section[]{
                tracklayout.getSectionByAId("Sc009"),
                tracklayout.getSectionByAId("Sc010"),
                tracklayout.getSectionByAId("Sc011"),
                tracklayout.getSectionByAId("Sc012"),
                tracklayout.getSectionByAId("Sc008"),
                tracklayout.getSectionByAId("Sc000"),
                tracklayout.getSectionByAId("Sc001"),
                tracklayout.getSectionByAId("Sc002"),
                tracklayout.getSectionByAId("Sc003"),
                tracklayout.getSectionByAId("Sc004"),
                tracklayout.getSectionByAId("Sc005"),
                tracklayout.getSectionByAId("Sc006"),
                tracklayout.getSectionByAId("Sc009")
        };
        assertThat(routeIsCyclic(route)).isEqualTo(false);
    }

    private void routeTestHelper(Section[] route, Section[] correctRoute) {
        assertThat(route.length).isEqualTo(correctRoute.length);
        for (int i = 0; i < route.length; i++) {
            assertThat(route[i]).isEqualTo(correctRoute[i]);
        }
    }

    @Test
    public void routeTest() throws TrackLayoutException {
        Section[] route, correctRoute;

        route = Section.route(
                this.chaincodeStub,
                tracklayout.getSectionByAId("Sc001"),
                tracklayout.getSectionByAId("Sc000"),
                null
        );
        correctRoute = new Section[]{};
        routeTestHelper(route, correctRoute);

        route = Section.route(
                this.chaincodeStub,
                tracklayout.getSectionByAId("Sc001"),
                tracklayout.getSectionByAId("Sc000"),
                tracklayout.getSectionByAId("Sc009")
        );
        correctRoute = new Section[]{
                tracklayout.getSectionByAId("Sc001"),
                tracklayout.getSectionByAId("Sc000"),
                tracklayout.getSectionByAId("Sc008"),
                tracklayout.getSectionByAId("Sc012"),
                tracklayout.getSectionByAId("Sc011"),
                tracklayout.getSectionByAId("Sc010"),
                tracklayout.getSectionByAId("Sc009")
        };
        routeTestHelper(route, correctRoute);
        route = Section.route(
                this.chaincodeStub,
                tracklayout.getSectionByAId("Sc004"),
                tracklayout.getSectionByAId("Sc013"),
                tracklayout.getSectionByAId("Sc009")
        );
        correctRoute = new Section[]{
                tracklayout.getSectionByAId("Sc004"),
                tracklayout.getSectionByAId("Sc013"),
                tracklayout.getSectionByAId("Sc011"),
                tracklayout.getSectionByAId("Sc012"),
                tracklayout.getSectionByAId("Sc008"),
                tracklayout.getSectionByAId("Sc000"),
                tracklayout.getSectionByAId("Sc001"),
                tracklayout.getSectionByAId("Sc002"),
                tracklayout.getSectionByAId("Sc014"),
                tracklayout.getSectionByAId("Sc010"),
                tracklayout.getSectionByAId("Sc009"),
        };
        routeTestHelper(route, correctRoute);
        route = Section.route(
                this.chaincodeStub,
                tracklayout.getSectionByAId("Sc006"),
                tracklayout.getSectionByAId("Sc007"),
                tracklayout.getSectionByAId("Sc013")
        );
        correctRoute = new Section[]{
                tracklayout.getSectionByAId("Sc006"),
                tracklayout.getSectionByAId("Sc007"),
                tracklayout.getSectionByAId("Sc008"),
                tracklayout.getSectionByAId("Sc000"),
                tracklayout.getSectionByAId("Sc001"),
                tracklayout.getSectionByAId("Sc002"),
                tracklayout.getSectionByAId("Sc014"),
                tracklayout.getSectionByAId("Sc010"),
                tracklayout.getSectionByAId("Sc009"),
                tracklayout.getSectionByAId("Sc006"),
                tracklayout.getSectionByAId("Sc005"),
                tracklayout.getSectionByAId("Sc004"),
                tracklayout.getSectionByAId("Sc013")
        };
        routeTestHelper(route, correctRoute);
    }

    @Test
    public void calculateNextPredictedSectionTest() throws SectionNotFoundException {
        Section testSectionA = this.tracklayout.getSectionByAId("Sc011");
        Section testSectionB = this.tracklayout.getSectionByAId("Sc001");
        try {
            testSectionA.calculateExpectedNextSection(this.chaincodeStub, testSectionB);
            failBecauseExceptionWasNotThrown(TrainManagementRuntimeException.class);
        } catch (TrainManagementRuntimeException e) {
            assertThat(e).hasCauseExactlyInstanceOf(NotAdjacentException.class);
        }
    }
}
