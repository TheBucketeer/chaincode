package trackTopology.errors.blockchain.unchecked.invaliddata;

public class InvalidLevelCrossingStateOnBCException extends BlockchainInvalidDataException {
    public InvalidLevelCrossingStateOnBCException(String lcStateStr) {
        super("level crossing state", lcStateStr);
    }

    public InvalidLevelCrossingStateOnBCException(String currentStateStr, String intendedStateStr) {
        super("level crossing state", currentStateStr, intendedStateStr);
    }
}
