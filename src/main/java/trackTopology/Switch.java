package trackTopology;

import chaincode.blockchainVariables.SwitchStateVariableSplit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.shim.ChaincodeStub;
import shared.assetdata.SwitchConfig.SWITCH_STATE;
import trackTopology.errors.tracklayout.setup.AdjacentOutOfBoundsException;

import java.util.List;

import static chaincode.EventHelper.queueSwitchCommand;
import static shared.GlobalConfig.SWITCH_AID_PREFIX;

public class Switch extends TrackLayoutComponent {
    private static final Log LOG = LogFactory.getLog(Switch.class);
    private Section container;

    private SwitchStateVariableSplit currentState = new SwitchStateVariableSplit(this);
    private SwitchStateVariableSplit intendedState = new SwitchStateVariableSplit(this, true);

    public Switch(int sId) {
        this.aId = String.format("%s%03d", SWITCH_AID_PREFIX, sId);
    }

    Switch(Switch old) {
        this.aId = old.aId;
        this.container = old.container;
        this.currentState = new SwitchStateVariableSplit(old.currentState);
        this.intendedState = new SwitchStateVariableSplit(old.intendedState);
    }

    Section getContainer() {
        return this.container;
    }

    void setContainer(Section s) throws AdjacentOutOfBoundsException {
        this.container = s;

        List<Section> side0 = this.container.getAllAdjacent().get(0);
        List<Section> side1 = this.container.getAllAdjacent().get(1);
        if (!(side0.size() == 2 && side1.size() == 1) && !(side0.size() == 1 && side1.size() == 2)) {
            throw new AdjacentOutOfBoundsException(side0.size(), side1.size(), side0.size() + side1.size());
        }
    }

    public void initSwitchStateChange(ChaincodeStub chaincodeStub, SWITCH_STATE state, boolean forceResend) {
        // TODO: adapt tests and turn on
        /*if (this.getContainer().getReservee(chaincodeStub) != null &&
                this.getContainer().getReservee(chaincodeStub).getCurrentSection(chaincodeStub) != this.getContainer()) {
            logInfo(chaincodeStub, this.format("Will not switch because there's a train inside us!"));
            //return;
        }*/
        if (this.getIntendedState(chaincodeStub) != state || forceResend) {
            this.setIntendedState(chaincodeStub, state);
            queueSwitchCommand(chaincodeStub, this, state);
        }
    }

    void initSwitchStateChange(ChaincodeStub chaincodeStub, SWITCH_STATE direction) {
        this.initSwitchStateChange(chaincodeStub, direction, false);
    }

    public void updateState(ChaincodeStub chaincodeStub, SWITCH_STATE state) {
        assert (state != null);
        this.setCurrentState(chaincodeStub, state);
        LOG.info(this.format("Switched " + state.getName()));
    }

    SwitchStateVariableSplit getCurrentStateBcVariable() {
        return this.currentState;
    }

    SwitchStateVariableSplit getIntendedStateBcVariable() {
        return this.intendedState;
    }

    public SWITCH_STATE getCurrentState(ChaincodeStub chaincodeStub) {
        return this.currentState.getValue(chaincodeStub);
    }

    void setCurrentState(ChaincodeStub chaincodeStub, SWITCH_STATE state) {
        this.currentState.setValue(chaincodeStub, state);
    }

    void flipCurrentState(ChaincodeStub chaincodeStub) {
        this.setCurrentState(chaincodeStub, this.getCurrentState(chaincodeStub).getOpposite());
    }

    SWITCH_STATE getIntendedState(ChaincodeStub chaincodeStub) {
        return this.intendedState.getValue(chaincodeStub);
    }

    void setIntendedState(ChaincodeStub chaincodeStub, SWITCH_STATE state) {
        this.intendedState.setValue(chaincodeStub, state);
    }

    public String toString(ChaincodeStub chaincodeStub) {
        SWITCH_STATE current = this.getCurrentState(chaincodeStub);
        SWITCH_STATE intended = this.getIntendedState(chaincodeStub);
        return this.aId + ", in " + (this.container == null ? "???" : this.container.getAssetId()) + ", switched " + (current == null ? "???" : current.getName()) + ", switching " + (intended == null ? "???" : intended.getName());
    }
}
