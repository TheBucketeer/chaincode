package trackTopology;

import chaincode.BlocktrainCC;
import chaincode.EventHelper;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import shared.assetdata.SwitchConfig.SWITCH_STATE;
import shared.assetdata.TrainConfig.TRAIN_DIRECTION;
import shared.assetdata.TrainConfig.TRAIN_SPEED;
import trackTopology.errors.tracklayout.TrackLayoutException;
import trackTopology.errors.tracklayout.assetnotfound.SectionNotFoundException;
import trackTopology.errors.tracklayout.assetnotfound.SensorNotFoundException;
import trackTopology.errors.tracklayout.assetnotfound.SwitchNotFoundException;

import static chaincode.BlocktrainCC.*;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static shared.GlobalConfig.TEST_TRACK_LAYOUT_JSON;
import static shared.assetdata.SwitchConfig.SWITCH_STATE.INWARDS;
import static shared.assetdata.SwitchConfig.SWITCH_STATE.OUTWARDS;
import static trackTopology.Train.SENSOR_TRIGGER_LIKELIHOOD;
import static trackTopology.Train.SENSOR_TRIGGER_LIKELIHOOD.*;

//import chaincode.blockchainVariables.BlockchainVariable;

@RunWith(MockitoJUnitRunner.class)
public class TrainTest {
    private TrackLayout tracklayout;
    private Train a, b;
    @Spy
    BlocktrainCC blocktrainCC;
    @Mock
    ChaincodeStub chaincodeStub;

    private Section SECTION(String aId) throws SectionNotFoundException {
        return this.tracklayout.getSectionByAId(aId);
    }

    private Sensor SENSOR(String aId) throws SensorNotFoundException {
        return this.tracklayout.getSensorByAId(aId);
    }

    private Switch SWITCH(String aId) throws SwitchNotFoundException {
        return this.tracklayout.getSwitchByAId(aId);
    }

    private void processSensorTriggerHelper(Train train, SENSOR_TRIGGER_LIKELIHOOD score, String sensorAssetId, String currentSectionAssetId, String nextSectionAssetId) throws SensorNotFoundException, SectionNotFoundException {
        assertThat(train.getLikelihoodOfSensorTrigger(this.chaincodeStub, SENSOR(sensorAssetId))).isEqualTo(score);
        train.processSensorReading(this.chaincodeStub, SENSOR(sensorAssetId));
        assertThat(train.getCurrentSection(this.chaincodeStub)).isEqualTo(SECTION(nextSectionAssetId));
        assertThat(train.getSectionsPassed(this.chaincodeStub).getFirst()).isEqualTo(SECTION(currentSectionAssetId));
        assertThat(train.getIntendedSpeed(this.chaincodeStub).toDouble()).isLessThanOrEqualTo(SECTION(nextSectionAssetId).getVMax().toDouble());
        this.tracklayout.prepareSectionsForReservees(this.chaincodeStub);
        this.tracklayout.updateTrainSpeeds(this.chaincodeStub);
    }

    private void checkIntendedSwitchStateHelper(Train train, Switch sw, SWITCH_STATE state) {
        assertThat(sw.getIntendedState(this.chaincodeStub)).isEqualTo(state);
        assertThat(train.getDesiredSwitchPosition(this.chaincodeStub, sw)).isEqualTo(state);
    }


    @Before
    public void doBefore() throws TrackLayoutException {
        timestamps.put(this.chaincodeStub, (long) 0);
        ignoreBc.put(this.chaincodeStub, true);
        doNothing().when(this.blocktrainCC).queueEvent(any(ChaincodeStub.class), anyString(), any(JSONObject.class));
        EventHelper.setParent(this.blocktrainCC);
//        BlockchainVariable.setBlocktrainCC(this.blocktrainCC);
        this.tracklayout = new TrackLayout(this.chaincodeStub, TEST_TRACK_LAYOUT_JSON);
        trackLayouts.put(this.chaincodeStub, this.tracklayout);

        // Do nothing, it's just that I don't want to deal with Mockito whining about unnecessary stubs
        this.blocktrainCC.queueEvent(this.chaincodeStub, "null", new JSONObject());

        this.a = this.tracklayout.getTrainByAId("Tr000");
        this.b = this.tracklayout.getTrainByAId("Tr001");
        this.a.setInitialised(this.chaincodeStub, true);
        this.b.setInitialised(this.chaincodeStub, true);
    }

    private void setDefaultPositions() throws SectionNotFoundException, SwitchNotFoundException {
        // a: 8 -> [0] -> (1) -> 2
        // b: 4 -> [5] -> 6 -> (9)
        this.a.setDestinationSection(this.chaincodeStub, SECTION("Sc001"));
        this.a.position(this.chaincodeStub, SECTION("Sc008"), SECTION("Sc000"));

        this.b.setDestinationSection(this.chaincodeStub, SECTION("Sc009"));
        this.b.position(this.chaincodeStub, SECTION("Sc004"), SECTION("Sc005"));
        this.tracklayout.prepareSectionsForReservees(this.chaincodeStub);
        this.checkIntendedSwitchStateHelper(this.b, SWITCH("Sw002"), INWARDS);
    }

    @Test
    public void longRegularRunShouldSucceed() throws SectionNotFoundException, SwitchNotFoundException, SensorNotFoundException {
        this.setDefaultPositions();

        SWITCH("Sw002").setCurrentState(this.chaincodeStub, INWARDS);
        SWITCH("Sw004").setCurrentState(this.chaincodeStub, INWARDS);
        this.b.calculatePredictedTrack(this.chaincodeStub);
        // b: 5 -> [6] -> (9) -> 10
        this.processSensorTriggerHelper(this.b, EXPECTED, "Sn006", "Sc005", "Sc006");
        this.b.setDestinationSection(this.chaincodeStub, SECTION("Sc002"));
        this.b.calculatePredictedTrack(this.chaincodeStub);
        // b: 6 -> [9] -> 10 -> 14
        this.processSensorTriggerHelper(this.b, EXPECTED, "Sn009", "Sc006", "Sc009");
        this.checkIntendedSwitchStateHelper(this.b, SWITCH("Sw004"), INWARDS);
        // b: 9 -> [10] -> 14 -> <(2)>
        this.processSensorTriggerHelper(this.b, EXPECTED, "Sn010", "Sc009", "Sc010");
        assertThat(SECTION("Sc002").getReservee(this.chaincodeStub)).isEqualTo(this.a);
        assertTrue(SECTION("Sc002").hasInWaitingQueue(this.chaincodeStub, this.b));
        assertThat(this.b.getIntendedSpeed(this.chaincodeStub)).isEqualTo(TRAIN_SPEED.getStopSpeed());
        this.a.setDestinationSection(this.chaincodeStub, SECTION("Sc005"));
        // a: 8 -> [0] -> 1 -> 2
        this.a.calculatePredictedTrack(this.chaincodeStub);
        // a: 0 -> [1] -> 2 -> 3
        this.processSensorTriggerHelper(this.a, EXPECTED, "Sn001", "Sc000", "Sc001");
        this.checkIntendedSwitchStateHelper(this.a, SWITCH("Sw000"), OUTWARDS);
        // Switches are flipped outwards by default
        assertThat(SWITCH("Sw000").getCurrentState(this.chaincodeStub)).isEqualTo(OUTWARDS);

        // a: 1 -> [2] -> 3 -> 4
        this.processSensorTriggerHelper(this.a, EXPECTED, "Sn002", "Sc001", "Sc002");
        // a: 2 -> [3] -> 4 -> (5)
        this.processSensorTriggerHelper(this.a, EXPECTED, "Sn003", "Sc002", "Sc003");
        this.checkIntendedSwitchStateHelper(this.a, SWITCH("Sw001"), OUTWARDS);
        // a: 3 -> [4] -> (5) -> 6
        this.processSensorTriggerHelper(this.a, EXPECTED, "Sn004", "Sc003", "Sc004");
        // Section Sc002 should be released for train b now, but the switch must be switched inwards
        SWITCH("Sw000").setCurrentState(this.chaincodeStub, INWARDS);
        // And update the train speeds to adapt to this topology change
        this.tracklayout.updateTrainSpeeds(this.chaincodeStub);
        assertThat(SECTION("Sc002").getReservee(this.chaincodeStub)).isEqualTo(this.b);
        assertFalse(SECTION("Sc002").hasInWaitingQueue(this.chaincodeStub, this.b));
        assertThat(this.b.getIntendedSpeed(this.chaincodeStub)).isNotEqualTo(TRAIN_SPEED.getStopSpeed());
    }

    private void setSwitchFailurePositions() throws SectionNotFoundException, SwitchNotFoundException {
        // a: 09 -> [10] -> (14) ->  02
        // b: 01 -> [00] ->  08  -> (07)
        // Train a about to traverse a failing switch
        this.a.setDestinationSection(this.chaincodeStub, SECTION("Sc014"));
        this.a.position(this.chaincodeStub, SECTION("Sc009"), SECTION("Sc010"));
        // Just get train b out of the way
        this.b.setDestinationSection(this.chaincodeStub, SECTION("Sc007"));
        this.b.position(this.chaincodeStub, SECTION("Sc001"), SECTION("Sc000"));
        this.tracklayout.prepareSectionsForReservees(this.chaincodeStub);
        this.checkIntendedSwitchStateHelper(this.a, SWITCH("Sw004"), INWARDS);
        this.checkIntendedSwitchStateHelper(this.b, SWITCH("Sw003"), OUTWARDS);
    }

    private void setTrainsAfterOneAnother() throws SectionNotFoundException, SwitchNotFoundException {
        // a: 05 -> [06] -> (07) -> 08
        // b: 00 -> [01] -> 02 -> 03
        this.a.setDestinationSection(this.chaincodeStub, SECTION("Sc001"));
        this.a.position(this.chaincodeStub, SECTION("Sc005"), SECTION("Sc006"));
        this.b.setDestinationSection(this.chaincodeStub, SECTION("Sc007"));
        this.b.position(this.chaincodeStub, SECTION("Sc000"), SECTION("Sc001"));
        this.tracklayout.prepareSectionsForReservees(this.chaincodeStub);
        this.checkIntendedSwitchStateHelper(this.a, SWITCH("Sw002"), OUTWARDS);
        this.checkIntendedSwitchStateHelper(this.a, SWITCH("Sw003"), OUTWARDS);
        this.checkIntendedSwitchStateHelper(this.b, SWITCH("Sw000"), OUTWARDS);
    }

    private void setOneTrainInMiddle() throws SectionNotFoundException, SwitchNotFoundException {
        // a: 05 -> [06] -> 09 -> 10
        // b: 00 -> [08] -> (07) -> 06
        this.a.setDestinationSection(this.chaincodeStub, SECTION("Sc014"));
        this.a.position(this.chaincodeStub, SECTION("Sc005"), SECTION("Sc006"));
        this.b.setDestinationSection(this.chaincodeStub, SECTION("Sc007"));
        this.b.position(this.chaincodeStub, SECTION("Sc000"), SECTION("Sc008"));
        SWITCH("Sw002").setCurrentState(this.chaincodeStub, INWARDS);
        this.a.calculatePredictedTrack(this.chaincodeStub);
        assertThat(this.a.getDesiredSwitchPosition(this.chaincodeStub, SWITCH("Sw002"))).isEqualTo(INWARDS);
        assertThat(this.b.getDesiredSwitchPosition(this.chaincodeStub, SWITCH("Sw003"))).isEqualTo(OUTWARDS);
    }

    @Test
    public void ensureCorrectReservationsAfterPositioning() throws SectionNotFoundException, SwitchNotFoundException {
        this.setDefaultPositions();
        for (Section section : this.tracklayout.getSections()) {
            // No sections should have a waiting queue
            assertThat(section.getWaitingQueue(this.chaincodeStub).size()).isEqualTo(0);
            switch (section.getAssetId()) {
                case "Sc004":
                case "Sc005":
                case "Sc006":
                case "Sc009":
                    assertThat(section.getReservee(this.chaincodeStub)).isEqualTo(this.b);
                    break;
                case "Sc008":
                case "Sc000":
                case "Sc001":
                case "Sc002":
                    assertThat(section.getReservee(this.chaincodeStub)).isEqualTo(this.a);
                    break;
                default:
                    assertThat(section.getReservee(this.chaincodeStub)).isEqualTo(null);
                    break;
            }
        }
    }

    @Test
    public void ensureCorrectReservationsInBundle() throws SectionNotFoundException, SwitchNotFoundException {
        this.setOneTrainInMiddle();
        this.a.advancePositionAndReserve(this.chaincodeStub);
        // Train a should now have both middle sections reserved
        assertThat(this.a.getCurrentSection(this.chaincodeStub)).isEqualTo(SECTION("Sc009"));
        assertThat(SECTION("Sc014").getReservee(this.chaincodeStub)).isEqualTo(this.a);
        assertThat(SECTION("Sc013").getReservee(this.chaincodeStub)).isEqualTo(this.a);
    }

    @Test
    public void checkSensorTriggerScores() throws SectionNotFoundException, SwitchNotFoundException, SensorNotFoundException {
        this.setSwitchFailurePositions();
        // Expected
        SWITCH("Sw004").setCurrentState(this.chaincodeStub, INWARDS);
        this.a.calculatePredictedTrack(this.chaincodeStub);
        assertThat(this.a.getLikelihoodOfSensorTrigger(this.chaincodeStub, SENSOR("Sn014"))).isEqualTo(EXPECTED);
        // Missed sensor
        assertThat(this.a.getLikelihoodOfSensorTrigger(this.chaincodeStub, SENSOR("Sn017"))).isEqualTo(EXPECTED_WITH_MISSED_SENSOR);
        // Expected duplicate
        assertThat(this.a.getLikelihoodOfSensorTrigger(this.chaincodeStub, SENSOR("Sn010"))).isEqualTo(DUPLICATE);
        // Delayed switch flip
        SWITCH("Sw004").setCurrentState(this.chaincodeStub, OUTWARDS);
        this.a.calculatePredictedTrack(this.chaincodeStub);
        assertThat(this.a.getLikelihoodOfSensorTrigger(this.chaincodeStub, SENSOR("Sn011"))).isEqualTo(EXPECTED_WITH_DELAYED_SWITCH_FLIP);
        // Correct unnoticed switch flip
        SWITCH("Sw004").setCurrentState(this.chaincodeStub, OUTWARDS);
        this.a.calculatePredictedTrack(this.chaincodeStub);
        assertThat(this.a.getLikelihoodOfSensorTrigger(this.chaincodeStub, SENSOR("Sn014"))).isEqualTo(EXPECTED_WITH_CORRECT_UNNOTICED_SWITCH_FLIP);
        // Incorrect unnoticed switch flip
        SWITCH("Sw004").setCurrentState(this.chaincodeStub, INWARDS);
        this.a.calculatePredictedTrack(this.chaincodeStub);
        assertThat(this.a.getLikelihoodOfSensorTrigger(this.chaincodeStub, SENSOR("Sn011"))).isEqualTo(EXPECTED_WITH_INCORRECT_UNNOTICED_SWITCH_FLIP);
        // Completely unexpected sensor reading
        assertThat(this.a.getLikelihoodOfSensorTrigger(this.chaincodeStub, SENSOR("Sn000"))).isEqualTo(NOT_EXPECTED);
    }

    @Test
    public void singleAdvanceShouldSucceed() throws SectionNotFoundException, SwitchNotFoundException {
        this.setDefaultPositions();
        this.b.advancePositionAndReserve(this.chaincodeStub);
        assertThat(this.b.getCurrentSection(this.chaincodeStub)).isEqualTo(SECTION("Sc006"));
    }

    @Test
    public void trainShouldStopAndContinue() throws SectionNotFoundException, SwitchNotFoundException, SensorNotFoundException {
        this.setTrainsAfterOneAnother();
        this.processSensorTriggerHelper(this.a, EXPECTED, "Sn007", "Sc006", "Sc007");
        // Train b should still have section 0 reserved, and train a should be waiting for it
        assertThat(SECTION("Sc000").getReservee(this.chaincodeStub)).isEqualTo(this.b);
        assertTrue(SECTION("Sc000").hasInWaitingQueue(this.chaincodeStub, this.a));
        assertThat(this.a.getIntendedSpeed(this.chaincodeStub)).isEqualTo(TRAIN_SPEED.getStopSpeed());
        this.processSensorTriggerHelper(this.b, EXPECTED, "Sn002", "Sc001", "Sc002");
        // Train b should have given up section 0, train a should now have it and continue driving
        assertThat(SECTION("Sc000").getReservee(this.chaincodeStub)).isEqualTo(this.a);
        assertFalse(SECTION("Sc000").hasInWaitingQueue(this.chaincodeStub, this.a));
        assertThat(this.a.getIntendedSpeed(this.chaincodeStub)).isNotEqualTo(TRAIN_SPEED.getStopSpeed());
    }

    @Test
    public void missedSensorShouldSucceed() throws SectionNotFoundException, SensorNotFoundException, SwitchNotFoundException {
        this.setDefaultPositions();
        assertThat(this.a.getLikelihoodOfSensorTrigger(this.chaincodeStub, SENSOR("Sn002"))).isEqualTo(EXPECTED_WITH_MISSED_SENSOR);
        this.a.processSensorReading(this.chaincodeStub, SENSOR("Sn002"));
        assertThat(this.a.getCurrentSection(this.chaincodeStub)).isEqualTo(SECTION("Sc002"));
    }

    @Test
    public void duplicateReadingShouldSucceed() throws SectionNotFoundException, SensorNotFoundException, SwitchNotFoundException {
        this.setDefaultPositions();
        assertThat(this.a.getLikelihoodOfSensorTrigger(this.chaincodeStub, SENSOR("Sn002"))).isEqualTo(EXPECTED_WITH_MISSED_SENSOR);
        this.a.processSensorReading(this.chaincodeStub, SENSOR("Sn000"));
        assertThat(this.a.getCurrentSection(this.chaincodeStub)).isEqualTo(SECTION("Sc000"));
    }

    @Test
    public void switchFailingShouldSucceed() throws SectionNotFoundException, SwitchNotFoundException, SensorNotFoundException {
        // Set up
        this.setSwitchFailurePositions();
        // Delayed switch flip
        SWITCH("Sw004").setCurrentState(this.chaincodeStub, OUTWARDS);
        this.a.calculatePredictedTrack(this.chaincodeStub);
        this.processSensorTriggerHelper(this.a, EXPECTED_WITH_DELAYED_SWITCH_FLIP, "Sn011", "Sc010", "Sc011");

        // Reset
        this.setSwitchFailurePositions();
        // Correct unnoticed switch flip
        SWITCH("Sw004").setCurrentState(this.chaincodeStub, OUTWARDS);
        this.a.calculatePredictedTrack(this.chaincodeStub);
        this.processSensorTriggerHelper(this.a, EXPECTED_WITH_CORRECT_UNNOTICED_SWITCH_FLIP, "Sn014", "Sc010", "Sc014");
        assertThat(SWITCH("Sw004").getCurrentState(this.chaincodeStub)).isEqualTo(INWARDS);

        // Reset
        this.setSwitchFailurePositions();
        // Incorrect unnoticed switch flip
        SWITCH("Sw004").setCurrentState(this.chaincodeStub, INWARDS);
        this.a.calculatePredictedTrack(this.chaincodeStub);
        this.processSensorTriggerHelper(this.a, EXPECTED_WITH_INCORRECT_UNNOTICED_SWITCH_FLIP, "Sn011", "Sc010", "Sc011");
        assertThat(SWITCH("Sw004").getCurrentState(this.chaincodeStub)).isEqualTo(OUTWARDS);
    }

    @Test
    public void deadLockResolveShouldTurnAroundTrain() throws SectionNotFoundException, SwitchNotFoundException {
        this.a.setDestinationSection(this.chaincodeStub, SECTION("Sc001"));
        this.a.position(this.chaincodeStub, SECTION("Sc006"), SECTION("Sc005"));
        this.b.setDestinationSection(this.chaincodeStub, SECTION("Sc005"));
        this.b.position(this.chaincodeStub, SECTION("Sc008"), SECTION("Sc000"));
        this.a.setInitialised(this.chaincodeStub, true);
        this.b.setInitialised(this.chaincodeStub, true);
        assertThat(SWITCH("Sw000").getIntendedState(this.chaincodeStub)).isEqualTo(OUTWARDS);
        assertThat(SWITCH("Sw001").getIntendedState(this.chaincodeStub)).isEqualTo(OUTWARDS);

        this.a.advancePositionAndReserve(this.chaincodeStub);
        assertThat(this.a.getCurrentSection(this.chaincodeStub)).isEqualTo(SECTION("Sc004"));

        TRAIN_DIRECTION currentDirectionB = this.b.getCurrentDirection(this.chaincodeStub);

        this.b.advancePositionAndReserve(this.chaincodeStub);
        assertThat(this.b.getCurrentSection(this.chaincodeStub)).isEqualTo(SECTION("Sc001"));
        // Train b should have been ordered to turn around
        assertThat(this.b.getIntendedDirection(this.chaincodeStub)).isEqualTo(currentDirectionB.getOpposite());
    }
}
