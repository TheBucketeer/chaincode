package trackTopology.errors.runtime.unchecked;

import trackTopology.Switch;
import trackTopology.Train;

public class TrainDoesNotCareAboutSwitchException extends TrainManagementRuntimeException {
    public TrainDoesNotCareAboutSwitchException(Train train, Switch sw) {
        super(train.getAssetId() + ".getDesiredSwitchPosition(" + sw.getAssetId() + ") called but it does not care either way.\nSwitch involved: " + sw.toString() + "\nTrain involved: " + train.toString());
    }
}
