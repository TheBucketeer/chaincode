package trackTopology;

import chaincode.blockchainVariables.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.shim.ChaincodeStub;
import shared.assetdata.SwitchConfig.SWITCH_STATE;
import shared.assetdata.TrainConfig.TRAIN_DIRECTION;
import shared.assetdata.TrainConfig.TRAIN_SPEED;
import trackTopology.errors.runtime.checked.SectionAlreadyReservedException;
import trackTopology.errors.runtime.unchecked.*;
import trackTopology.errors.tracklayout.TrackLayoutException;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static chaincode.BlocktrainCC.logInfo;
import static chaincode.BlocktrainCC.logWarn;
import static chaincode.EventHelper.*;
import static shared.GlobalConfig.*;
import static trackTopology.Train.SENSOR_TRIGGER_LIKELIHOOD.*;

public class Train extends TrackLayoutComponent {
    private static final Log LOG = LogFactory.getLog(Train.class);

    //private TrainSpeedVariable speed = new TrainSpeedVariable(this);
    private TrainSpeedVariableSplit currentSpeed = new TrainSpeedVariableSplit(this);
    private TrainSpeedVariableSplit intendedSpeed = new TrainSpeedVariableSplit(this, true);
    //    private TrainDirectionVariable direction = new TrainDirectionVariable(this);
    private TrainDirectionVariableSplit currentDirection = new TrainDirectionVariableSplit(this);
    private TrainDirectionVariableSplit intendedDirection = new TrainDirectionVariableSplit(this, true);
    // Stores whether the client has sent an initialisation packet yet
    private TrainInitialisedStatusVariable initialised = new TrainInitialisedStatusVariable(this);
    // Destination and current position
    private TrainDestinationVariable destination = new TrainDestinationVariable(this);
    // Passed/current sections
    private TrainPositionVariable position = new TrainPositionVariable(this);

    // These lists represent the predicted sections, assuming everything goes to plan
    // First element (head) of lists will be reached the soonest
    private LinkedList<Section> sectionsAheadRouted = new LinkedList<>();
    private LinkedList<Sensor> sensorsAheadRouted = new LinkedList<>();
    // These lists represent the predicted sections, based on the current track layout
    private LinkedList<Section> sectionsAheadReal = new LinkedList<>();
    private LinkedList<Sensor> sensorsAheadReal = new LinkedList<>();

    // A list of all existing sections, used for removing reservations from sections we no longer need
    private List<Section> allSections;

    public enum SENSOR_TRIGGER_LIKELIHOOD {
        EXPECTED(6, "Expected"),
        DUPLICATE(5, "Duplicate"),
        EXPECTED_WITH_DELAYED_SWITCH_FLIP(4, "Delayed switch flip"),
        EXPECTED_WITH_CORRECT_UNNOTICED_SWITCH_FLIP(3, "Correct unnoticed switch flip"),
        EXPECTED_WITH_INCORRECT_UNNOTICED_SWITCH_FLIP(2, "Incorrect unnoticed switch flip"),
        EXPECTED_WITH_MISSED_SENSOR(1, "Missed sensor"),
        NOT_EXPECTED(0, "Not expected");


        private final int score;
        private final String name;

        SENSOR_TRIGGER_LIKELIHOOD(int score, String name) {
            this.score = score;
            this.name = name;
        }

        boolean involvesUnexpectedSwitchFlipUnderTrain() {
            return (this == EXPECTED_WITH_DELAYED_SWITCH_FLIP ||
                    this == EXPECTED_WITH_CORRECT_UNNOTICED_SWITCH_FLIP ||
                    this == EXPECTED_WITH_INCORRECT_UNNOTICED_SWITCH_FLIP);
        }

        public int toInt() {
            return this.score;
        }

        public String toString() {
            return this.name;
        }
    }

    public Train(int tId) {
        this.aId = String.format("%s%03d", TRAIN_AID_PREFIX, tId);
    }

    Train(Train old) {
        this.aId = old.aId;
        this.currentDirection = new TrainDirectionVariableSplit(old.currentDirection);
        this.intendedDirection = new TrainDirectionVariableSplit(old.intendedDirection);
        this.currentSpeed = new TrainSpeedVariableSplit(old.currentSpeed);
        this.intendedSpeed = new TrainSpeedVariableSplit(old.intendedSpeed);
        this.initialised = old.initialised;
        this.position = new TrainPositionVariable(old.position);
        this.destination = old.destination;
        this.allSections = old.allSections;
        this.sectionsAheadReal = new LinkedList<>(old.sectionsAheadReal);
        this.sectionsAheadRouted = new LinkedList<>(old.sectionsAheadRouted);
        this.sensorsAheadReal = new LinkedList<>(old.sensorsAheadReal);
        this.sensorsAheadRouted = new LinkedList<>(old.sensorsAheadRouted);
    }

    // General control actions (to be executed from BlockTrainCC)

    public void processSensorReading(ChaincodeStub chaincodeStub, Sensor sensorReading) {
        boolean sensorMissed = false;
        Sensor previous;
        SENSOR_TRIGGER_LIKELIHOOD score;

        this.assertCompletePositionData(chaincodeStub);
        // Check if this is a duplicate reading
        try {
            previous = this.getCurrentSection(chaincodeStub).getDelimitingSensor(this.getSectionsPassed(chaincodeStub).getFirst());
        } catch (TrackLayoutException e) {
            throw new TrainManagementRuntimeException(e);
        }
        if (previous.equals(sensorReading)) {
            // Do nothing
            LOG.warn(this.format("Detected duplicate reading of " + sensorReading.getAssetId()));
            return;
        }
        // Check what kind of sensor reading this is
        score = this.getLikelihoodOfSensorTrigger(chaincodeStub, sensorReading);
        LOG.info(this.format("Score for " + this.getAssetId() + ", " + sensorReading.getAssetId() + " = " + score.toString()));
        if (!this.getSensorsAheadReal().getFirst().equals(sensorReading)) {
            // Not a regular sensor reading, did we miss one?
            if (TRAIN_N_FUTURE_SECTIONS_PREDICTED > 1 && this.getSensorsAheadReal().get(1).equals(sensorReading)) {
                LOG.warn(this.format("Detecting missed sensor reading " + this.getSensorsAheadReal().getFirst().getAssetId() + ", got " + sensorReading.getAssetId() + " instead."));
                sensorMissed = true;
            } else if (score.involvesUnexpectedSwitchFlipUnderTrain()) {
                // Did a switch fail to do its, one and only, job?
                Switch wrongSwitch = this.getCurrentSection(chaincodeStub).getSwitch();
                assert (wrongSwitch != null);
                LOG.warn(this.format("Switching " + wrongSwitch.getAssetId() + " since it was in an unexpected state."));
                wrongSwitch.flipCurrentState(chaincodeStub);
                // Recalculate predictions after updating the switch's state
                this.calculatePredictedTrack(chaincodeStub);
            } else {
                throw new InvalidSensorReadingException(chaincodeStub, this, sensorReading);
            }
        }
        // Reading is valid, switch has been flipped if necessary: ready to advance
        this.advancePositionAndReserve(chaincodeStub);
        // If we missed a sensor, we must advance again
        if (sensorMissed) {
            this.advancePositionAndReserve(chaincodeStub);
        }
    }

    private void turnAround(ChaincodeStub chaincodeStub) {
        LOG.info(this.format("Turning around!"));
        this.setCurrentDirection(chaincodeStub, this.getCurrentDirection(chaincodeStub).getOpposite());
        // In case this had not been done yet
        //this.calculatePredictedTrack(chaincodeStub);
        this.turnAroundPositionAndReserve(chaincodeStub);
    }

    public void position(ChaincodeStub chaincodeStub, LinkedList<Section> passedSections, Section currentSection) {
        this.setPositionAndReserve(chaincodeStub, passedSections, currentSection);
    }

    public void position(ChaincodeStub chaincodeStub, Section lastSection, Section currentSection) {
        LinkedList<Section> passedSections = Section.expandPassedSections(chaincodeStub, lastSection, currentSection);
        this.setPositionAndReserve(chaincodeStub, passedSections, currentSection);
    }

    // Send either a go or a stop command, based on whether or not the train must wait for a section/switch to be freed/flipped
    void goIfSafeOrStop(ChaincodeStub chaincodeStub) {
        this.assertCompletePositionData(chaincodeStub);
        boolean mustWait = false;
        for (Section section : this.getSectionsToReserve(chaincodeStub)) {
            // Reservation not obtained yet
            if (!this.hasReservation(chaincodeStub, section)) {
                assert (section.hasInWaitingQueue(chaincodeStub, this));
                LOG.warn(this.format("Must wait for section " + section.getAssetId() + ", which is currently held by " + section.getReservee(chaincodeStub).getAssetId()));
                mustWait = true;
                break;
            }
            // Reserved section not ready yet
            if (!section.isReadyForReservee(chaincodeStub)) {
                mustWait = true;
                LOG.warn(this.format("Must wait for section " + section.getAssetId() + ", which is reserved but not ready yet! (" + section.toString(chaincodeStub) + ")"));
                break;
            }
        }
        if (mustWait) {
            // Can not move yet
            this.stop(chaincodeStub);
        } else if (this.getIntendedSpeed(chaincodeStub).equals(TRAIN_SPEED.getStopSpeed())) {
            // If we stopped before; it's now safe to move again
            this.initSpeedChange(chaincodeStub, this.getCurrentSection(chaincodeStub).getVMax());
        }
    }

    public void stop(ChaincodeStub chaincodeStub) {
        this.initSpeedChange(chaincodeStub, TRAIN_SPEED.getStopSpeed());
    }

    // Path prediction and routing functions

    // Recalculates future predictions based on the train's destination and the track topology,
    // requires currentSection to be set and at least 1 element in sectionsPassed
    // For the intended future data, first takes into consideration the route to our current destination,
    // then continues manually calculating more sections if needed
    // Does not handle reservations
    public void calculatePredictedTrack(ChaincodeStub chaincodeStub) {
        assert (this.getSectionsPassed(chaincodeStub) != null && this.getSectionsPassed(chaincodeStub).size() >= 1);
        Section intendedPrevious, intendedCurrent, intendedNext, realPrevious, realCurrent, realNext;
        Section[] route;

        //logWarn(chaincodeStub, this.format("calculatePredictedTrack, passed " + dumpSections(this.getSectionsPassed(chaincodeStub))) + ", current " + this.getCurrentDirection(chaincodeStub));
        // Erase previous data
        this.getSectionsAheadRouted().clear();
        this.getSectionsAheadReal().clear();
        this.getSensorsAheadRouted().clear();
        this.getSensorsAheadReal().clear();
        // Obtain route based on intended destination
        if (this.getDestinationSection(chaincodeStub) != null) {
            route = this.getRouteToDestination(chaincodeStub);
        } else {
            // No destination means no route
            route = new Section[]{};
        }
        for (int i = 0; i < TRAIN_N_FUTURE_SECTIONS_PREDICTED; i++) {
            // Sections
            if (i == 0) {
                intendedCurrent = realCurrent = this.getCurrentSection(chaincodeStub);
                intendedPrevious = realPrevious = this.getSectionsPassed(chaincodeStub).getFirst();
            } else {
                intendedCurrent = this.getSectionsAheadRouted().getLast();
                realCurrent = this.getSectionsAheadReal().getLast();
                // get(i - 1) == getLast(), get(i - 2) == "getSecondToLast()";
                intendedPrevious = (i == 1) ? this.getCurrentSection(chaincodeStub) : this.getSectionsAheadRouted().get(i - 2);
                realPrevious = (i == 1) ? this.getCurrentSection(chaincodeStub) : this.getSectionsAheadReal().get(i - 2);
            }
            // Intended route: first copy the entire route; if we need to predict more sections, calculate (using currently known track topology)
            intendedNext = i < route.length ? route[i] : intendedCurrent.calculateExpectedNextSection(chaincodeStub, intendedPrevious);
            // Real route: always calculate based on currently known topology
            realNext = realCurrent.calculateExpectedNextSection(chaincodeStub, realPrevious);
            this.getSectionsAheadRouted().addLast(intendedNext);
            this.getSectionsAheadReal().addLast(realNext);
            // Sensors
            try {
                this.getSensorsAheadRouted().addLast(intendedCurrent.getDelimitingSensor(intendedNext));
                this.getSensorsAheadReal().addLast(realCurrent.getDelimitingSensor(realNext));
            } catch (TrackLayoutException e) {
                // Serious, unexpected error; cannot continue
                throw new TrackPredictionException(this, e);
            }
        }
    }

    // Returns the shortest route to our destination, given our current and most recently passed section
    // If no destination is known, returns an empty route
    private Section[] getRouteToDestination(ChaincodeStub chaincodeStub) {
        if (this.getDestinationSection(chaincodeStub) == null) {
            return new Section[]{};
        }
        this.assertNonFuturePositionData(chaincodeStub);
        Section[] route = Section.route(chaincodeStub, this.getSectionsPassed(chaincodeStub).getFirst(), this.getCurrentSection(chaincodeStub), this.getDestinationSection(chaincodeStub));
        // Both the most recently passed as well as the current section will be in this route; remove them
        route = Arrays.copyOfRange(route, 2, route.length);
        return route;
    }

    // Used to check if a train has already passed a section in its route
    // Switches do not need to be switched if a train has already passed them
    boolean willPassSection(ChaincodeStub chaincodeStub, Section section) {
        return this.getCurrentSection(chaincodeStub).equals(section) || this.getSectionsAheadRouted().contains(section);
    }

    // Returns the number of section crossings to go before we reach our destination
    private int getDistanceToDestination(ChaincodeStub chaincodeStub) {
        return this.getRouteToDestination(chaincodeStub).length;
    }

    // Returns the position this switch should be in for this train, with respect to its destination
    SWITCH_STATE getDesiredSwitchPosition(ChaincodeStub chaincodeStub, Switch sw) {
        assert (sw != null);
        this.assertCompletePositionData(chaincodeStub);
        Section from = null, in = null, to = null;
        // Determine the required incoming/outgoing sections from/to this switch in our route
        if (this.getCurrentSection(chaincodeStub).getSwitch() != null && this.getCurrentSection(chaincodeStub).getSwitch().equals(sw)) {
            if (this.getSectionsPassed(chaincodeStub).size() > 0) {
                from = this.getSectionsPassed(chaincodeStub).getFirst();
                in = this.getCurrentSection(chaincodeStub);
                to = this.getSectionsAheadRouted().getFirst();
            }
        } else if (this.getSectionsAheadRouted().getFirst().getSwitch() != null && this.getSectionsAheadRouted().getFirst().getSwitch().equals(sw)) {
            if (this.getSectionsAheadRouted().size() > 1) {
                from = this.getCurrentSection(chaincodeStub);
                in = this.getSectionsAheadRouted().getFirst();
                to = this.getSectionsAheadRouted().get(1);
            }
        } else {
            for (int i = 1; i < this.getSectionsAheadRouted().size() - 1; i++) {
                if (this.getSectionsAheadRouted().get(i).getSwitch() != null && this.getSectionsAheadRouted().get(i).getSwitch().equals(sw)) {
                    from = this.getSectionsAheadRouted().get(i - 1);
                    in = this.getSectionsAheadRouted().get(i);
                    to = this.getSectionsAheadRouted().get(i + 1);
                    break;
                }
            }
        }
        // If this train does not have both an incoming and outgoing section stored, we don't care about the switch
        if (from == null || to == null) {
            throw new TrainDoesNotCareAboutSwitchException(this, sw);
        }
        LOG.info(this.format(this.aId + " wants " + sw.getAssetId() + " to be " + in.getRequiredSwitchPosition(from, to).getName() + "!"));
        return in.getRequiredSwitchPosition(from, to);
    }

    // Returns how many extra sections this train will have to cross if it turned around
    // This number can be negative if the new route would be shorter
    int getDistancePenaltyOfTurnAround(ChaincodeStub chaincodeStub) {
        this.assertCompletePositionData(chaincodeStub);
        int currentRouteLength = this.getDistanceToDestination(chaincodeStub);
        Train clone = new Train(this);
        clone.turnAroundPosition(chaincodeStub);
        int routeLengthAfterTurnAround = clone.getDistanceToDestination(chaincodeStub);
        return routeLengthAfterTurnAround - currentRouteLength;
    }

    // Returns true if this train could process a sensor reading a section without stopping, after turning around
    boolean canAdvanceWhenTurnedAround(ChaincodeStub chaincodeStub) {
        this.assertCompletePositionData(chaincodeStub);
        Train clone = new Train(this);
        clone.turnAroundPosition(chaincodeStub);
        return clone.canAdvance(chaincodeStub);
    }

    // Returns true if this train could process a sensor reading a section without stopping
    private boolean canAdvance(ChaincodeStub chaincodeStub) {
        this.assertCompletePositionData(chaincodeStub);
        Train clone = new Train(this);
        clone.advancePosition(chaincodeStub);
        return clone.canReserveAllWithoutStopping(chaincodeStub);
    }

    // Returns true if this train could reserve all necessary sections without having to wait
    private boolean canReserveAllWithoutStopping(ChaincodeStub chaincodeStub) {
        for (Section section : this.getSectionsToReserve(chaincodeStub)) {
            // We ignore sections that have already been passed, since we don't stop for them
            if (this.willPassSection(chaincodeStub, section)) {
                Train reservee = section.getReservee(chaincodeStub);
                if (reservee != null && !reservee.equals(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    /*
     * Position change functions that also handle section reservations
     */

    void advancePositionAndReserve(ChaincodeStub chaincodeStub) {
        this.assertCompletePositionData(chaincodeStub);
        this.advancePosition(chaincodeStub);
        this.performSectionReservations(chaincodeStub);
        this.cleanUpOldReservations(chaincodeStub);
    }

    private void turnAroundPositionAndReserve(ChaincodeStub chaincodeStub) {
        this.assertCompletePositionData(chaincodeStub);
        this.turnAroundPosition(chaincodeStub);
        // Perform any newly required reservations
        this.performSectionReservations(chaincodeStub);
        // Remove any reservations we no longer need
        this.cleanUpOldReservations(chaincodeStub);
    }

    public void setPositionAndReserve(ChaincodeStub chaincodeStub, LinkedList<Section> sectionsPassed, Section currentSection) {
        this.setPosition(chaincodeStub, sectionsPassed, currentSection);
        this.performSectionReservations(chaincodeStub);
        this.cleanUpOldReservations(chaincodeStub);
    }

    /*
     * Position change functions that don't handle section reservations or anything else
     */

    // Advances the position of the train by one sections, using the predictions based on the current track topology (and not the route)
    private void advancePosition(ChaincodeStub chaincodeStub) {
        this.assertCompletePositionData(chaincodeStub);
        this.getSectionsPassed(chaincodeStub).addFirst(this.getCurrentSection(chaincodeStub));
        // Remove last element from list
        this.getSectionsPassed(chaincodeStub).removeLast();

        // Shift current section and recalculate predicted track
        if (!this.getSectionsAheadReal().getFirst().equals(this.getSectionsAheadRouted().getFirst())) {
            LOG.warn(this.format("Could not follow route, went to " + this.getSectionsAheadReal().getFirst().getAssetId() + " instead of " + this.getSectionsAheadRouted().getFirst().getAssetId()));
        }
        this.setCurrentSection(chaincodeStub, this.getSectionsAheadReal().pollFirst());
        this.calculatePredictedTrack(chaincodeStub);
    }

    private void turnAroundPosition(ChaincodeStub chaincodeStub) throws TrackPredictionException {
        assert (this.getSectionsAheadRouted() != null && this.getCurrentSection(chaincodeStub) != null);
        Section newLastSection = this.getSectionsAheadRouted().getFirst();
        logWarn(chaincodeStub, this.format("Turning around, new last section is " + newLastSection.getAssetId() + ", current is " + this.getCurrentDirection(chaincodeStub).getName()));
        this.setSectionsPassed(chaincodeStub, Section.expandPassedSections(chaincodeStub, newLastSection, this.getCurrentSection(chaincodeStub)));
        this.calculatePredictedTrack(chaincodeStub);
    }

    private void setPosition(ChaincodeStub chaincodeStub, LinkedList<Section> sectionsPassed, Section currentSection) {
        this.setSectionsPassed(chaincodeStub, sectionsPassed);
        this.setCurrentSection(chaincodeStub, currentSection);
        this.calculatePredictedTrack(chaincodeStub);
    }

    // Returns a score representing the likelihood of this train having triggered this sensor
    SENSOR_TRIGGER_LIKELIHOOD getLikelihoodOfSensorTrigger(ChaincodeStub chaincodeStub, Sensor triggeredSensor) {
        assertCompletePositionData(chaincodeStub);
        Sensor nextRouted = this.getSensorsAheadRouted().getFirst();
        Sensor nextReal = this.getSensorsAheadReal().getFirst();
        Sensor nextNextRouted, nextNextReal;
        if (TRAIN_N_FUTURE_SECTIONS_PREDICTED > 1) {
            nextNextRouted = this.getSensorsAheadRouted().get(1);
            nextNextReal = this.getSensorsAheadReal().get(1);
        }

        // Normal, expected
        if (nextRouted.equals(triggeredSensor) && nextReal.equals(triggeredSensor)) {
            return EXPECTED;
        }

        // Expected if we assume we triggered a sensor twice
        try {
            Sensor previous = this.getCurrentSection(chaincodeStub).getDelimitingSensor(this.getSectionsPassed(chaincodeStub).getFirst());
            if (previous.equals(triggeredSensor)) {
                return DUPLICATE;
            }
        } catch (TrackLayoutException e) {
            throw new TrainManagementRuntimeException(e);
        }

        // Expected if switch has not yet switched to its intended position
        if (nextReal.equals(triggeredSensor)) {
            return EXPECTED_WITH_DELAYED_SWITCH_FLIP;
        }
        // Expected if switch has switched correctly without notifying the chaincode
        if (nextRouted.equals(triggeredSensor)) {
            return EXPECTED_WITH_CORRECT_UNNOTICED_SWITCH_FLIP;
        }

        // Expected if a switch has switched incorrectly without notifying the chaincode
        Section nextSectionAssumingFailedSwitch = this.getCurrentSection(chaincodeStub).calculateExpectedNextSectionWithSwitchFailure(chaincodeStub, this.getSectionsPassed(chaincodeStub).getFirst());
        Sensor nextSensorAssumingFailedSwitch;
        try {
            nextSensorAssumingFailedSwitch = nextSectionAssumingFailedSwitch.getDelimitingSensor(this.getCurrentSection(chaincodeStub));
        } catch (TrackLayoutException e) {
            throw new TrainManagementRuntimeException(e);
        }
        if (nextSensorAssumingFailedSwitch.equals(triggeredSensor)) {
            return EXPECTED_WITH_INCORRECT_UNNOTICED_SWITCH_FLIP;
        }

        if (nextNextRouted != null && nextNextReal != null) {
            // Expected if the train has missed a sensor
            if (nextNextRouted.equals(triggeredSensor) && nextNextReal.equals(triggeredSensor)) {
                return EXPECTED_WITH_MISSED_SENSOR;
            }
        }
        // Not expected at all
        return NOT_EXPECTED;
    }

    /*
     * Reservation functions
     */

    // Helper function for getSectionsToReserve, it adds a section either to the start or to the  end of a list, and
    // outputs a warning if the section was already in there (without adding the duplicate section to the list).
    // It can also add all sections that are bundled with this section to the list. (The parameter addBundled is needed
    // to avoid infinite recursion.)
    private static void addSectionToReserveListHelper(Section section, LinkedList<Section> sectionsToReserve, boolean asFirst, boolean addBundled) {
        if (sectionsToReserve.contains(section)) {
            LOG.warn("sectionsToReserve contains duplicate " + section);
        } else if (asFirst) {
            sectionsToReserve.addFirst(Objects.requireNonNull(section));
        } else {
            sectionsToReserve.addLast(Objects.requireNonNull(section));
        }
        if (addBundled) {
            for (Section bundledWith : section.getBundledWith()) {
                addSectionToReserveListHelper(bundledWith, sectionsToReserve, asFirst, false);
            }
        }
    }

    // Returns a list of all sections that should be reserved, given the train's current position
    private LinkedList<Section> getSectionsToReserve(ChaincodeStub chaincodeStub) {
        this.assertCompletePositionData(chaincodeStub);
        LinkedList<Section> sectionsToReserve = new LinkedList<>();
        // Past:
        for (int i = 0; i < TRAIN_N_PASSED_SECTIONS_RESERVED; i++) {
            addSectionToReserveListHelper(this.getSectionsPassed(chaincodeStub).get(i), sectionsToReserve, true, true);
        }
        // Present:
        addSectionToReserveListHelper(this.getCurrentSection(chaincodeStub), sectionsToReserve, false, true);
        // Future:
        for (int i = 0; i < TRAIN_N_FUTURE_SECTIONS_RESERVED; i++) {
            addSectionToReserveListHelper(this.getSectionsAheadRouted().get(i), sectionsToReserve, false, true);
        }
        return sectionsToReserve;
    }

    // Returns a list of all sections that this train is waiting for
    LinkedList<Section> getWaitingForSections(ChaincodeStub chaincodeStub) {
        LinkedList<Section> result = new LinkedList<>();
        for (Section section : this.getSectionsToReserve(chaincodeStub)) {
            assert (!(this.hasReservation(chaincodeStub, section) && section.hasInWaitingQueue(chaincodeStub, this)));
            assert (this.hasReservation(chaincodeStub, section) || section.hasInWaitingQueue(chaincodeStub, this));
            if (section.hasInWaitingQueue(chaincodeStub, this)) {
                result.add(section);
            }
        }
        return result;
    }

    // Attempts to reserve a section, and updates relevant queues upon failure. Does not send a stop command directly.
    private void reserveSection(ChaincodeStub chaincodeStub, Section section, boolean stopIfTaken) {
        assert (section != null);
        try {
            section.reserve(chaincodeStub, this);
        } catch (SectionAlreadyReservedException e) {
            if (stopIfTaken) {
                if (!section.hasInWaitingQueue(chaincodeStub, this)) {
                    section.addTrainToQueue(chaincodeStub, this);
                    section.detectAndResolveDeadlock(chaincodeStub);
                    LOG.info(this.format("Stopping, waiting for " + section.getAssetId()));
                } else {
                    LOG.info(this.format("Stopping, were already waiting for " + section.getAssetId()));
                }
            } else {
                // If this section was not critical (ie. it was behind the train), just output a warning
                LOG.warn(this.format(e.getMessage() + " (ignored, section already passed)"));
            }
        }
    }

    private boolean hasReservation(ChaincodeStub chaincodeStub, Section section) {
        return section.getReservee(chaincodeStub) != null && section.getReservee(chaincodeStub).equals(this);
    }

    // Checks if all necessary sections are reserved; can log warnings and perform reservations if they are not.
    private void performSectionReservations(ChaincodeStub chaincodeStub) {
        assert (this.getCurrentSection(chaincodeStub) != null && this.getSectionsAheadRouted() != null);
        assert (this.getSectionsAheadRouted().size() == TRAIN_N_FUTURE_SECTIONS_PREDICTED);

        // Check necessary reservations
        for (Section section : this.getSectionsToReserve(chaincodeStub)) {
            assert (section != null);
            if (!this.hasReservation(chaincodeStub, section)) {
                LOG.info(this.format("Attempting to reserve " + section.getAssetId()));
                // Only stop if the problematic section is under/in front of us
                if (this.getCurrentSection(chaincodeStub).equals(section) || this.getSectionsAheadRouted().contains(section)) {
                    this.reserveSection(chaincodeStub, section, true);
                } else {
                    this.reserveSection(chaincodeStub, section, false);
                }
            }
        }
    }

    // Finishes all standing reservations/waits that we no longer need
    private void cleanUpOldReservations(ChaincodeStub chaincodeStub) {
        assert (this.allSections != null);
        LinkedList<Section> sectionsToReserve = this.getSectionsToReserve(chaincodeStub);

        for (Section section : this.allSections) {
            if (!sectionsToReserve.contains(section)) {
                // We don't need this reservation anymore, but do we have it?
                Train reservee = section.getReservee(chaincodeStub);
                if (reservee != null && reservee.equals(this)) {
                    // We do!
                    LOG.info(this.format("Giving up reservation for " + section.getAssetId()));
                    section.finishReservation(chaincodeStub);
                } else if (section.hasInWaitingQueue(chaincodeStub, this)) {
                    // We don't, but we are in its queue, let's remove ourselves from it
                    LOG.info(this.format("Removing spot in queue for " + section.getAssetId()));
                    section.removeTrainFromQueue(chaincodeStub, this);
                }
            }
        }
    }

    private void checkReady(ChaincodeStub chaincodeStub) {
        if (!this.isReady(chaincodeStub)) {
            throw new TrainNotReadyException(this);
        }
    }

    // Send commands to client
    public void sendInitCommand(ChaincodeStub chaincodeStub) {
        queueTrainInitCommand(chaincodeStub, this);
    }

    void initDirectionChange(ChaincodeStub chaincodeStub) {
        this.initDirectionChange(chaincodeStub, this.getCurrentDirection(chaincodeStub).getOpposite());
    }

    private void initDirectionChange(ChaincodeStub chaincodeStub, TRAIN_DIRECTION direction) {
        this.initDirectionChange(chaincodeStub, direction, false);
    }

    public void initDirectionChange(ChaincodeStub chaincodeStub, TRAIN_DIRECTION direction, boolean forceResend) {
        checkReady(chaincodeStub);
        if (this.getIntendedDirection(chaincodeStub) != direction || forceResend) {
            this.setIntendedDirection(chaincodeStub, direction);
            queueTrainDirectionCommand(chaincodeStub, this, direction);
        }
    }

    private void initSpeedChange(ChaincodeStub chaincodeStub, TRAIN_SPEED speed) {
        this.initSpeedChange(chaincodeStub, speed, false);
    }

    public void initSpeedChange(ChaincodeStub chaincodeStub, TRAIN_SPEED speed, boolean forceResend) {
        checkReady(chaincodeStub);
        if (!this.getIntendedSpeed(chaincodeStub).equals(speed) || forceResend) {
            this.setIntendedSpeed(chaincodeStub, speed);
            queueTrainSpeedCommand(chaincodeStub, this, speed);
        }
    }

    // Upon feedback from client
    public void updateSpeed(ChaincodeStub chaincodeStub, TRAIN_SPEED speed) {
        this.setCurrentSpeed(chaincodeStub, speed);
        LOG.info("Speed has changed to " + speed.toDouble());
    }

    public void updateDirection(ChaincodeStub chaincodeStub, TRAIN_DIRECTION direction) {
        assert (direction != null);
        boolean turnAround = this.getCurrentDirection(chaincodeStub).getOpposite() == direction;
        LOG.info(this.format(this.aId + " received direction update, " + (turnAround ? "" : "not ") + "turning around."));
        LOG.info("Direction has changed to " + direction.getName());
        if (turnAround) {
            this.turnAround(chaincodeStub);
        }
    }

    public void updateSetInitialised(ChaincodeStub chaincodeStub) {
        this.initialised.setValue(chaincodeStub, true);
    }

    // Getters and setters
    TrainSpeedVariableSplit getCurrentSpeedBcVariable() {
        return this.currentSpeed;
    }

    TrainSpeedVariableSplit getIntendedSpeedBcVariable() {
        return this.intendedSpeed;
    }

    public TRAIN_SPEED getCurrentSpeed(ChaincodeStub chaincodeStub) {
        return this.currentSpeed.getValue(chaincodeStub);
    }

    void setCurrentSpeed(ChaincodeStub chaincodeStub, TRAIN_SPEED speed) {
        this.currentSpeed.setValue(chaincodeStub, speed);
    }

    public TRAIN_SPEED getIntendedSpeed(ChaincodeStub chaincodeStub) {
        return this.intendedSpeed.getValue(chaincodeStub);
    }

    void setIntendedSpeed(ChaincodeStub chaincodeStub, TRAIN_SPEED speed) {
        this.intendedSpeed.setValue(chaincodeStub, speed);
    }

    void setAllSections(List<Section> allSections) {
        assert (allSections != null);
        this.allSections = allSections;
    }

    TrainDirectionVariableSplit getCurrentDirectionBcVariable() {
        return this.currentDirection;
    }

    TrainDirectionVariableSplit getIntendedDirectionBcVariable() {
        return this.intendedDirection;
    }

    public TRAIN_DIRECTION getCurrentDirection(ChaincodeStub chaincodeStub) {
        return this.currentDirection.getValue(chaincodeStub);
    }

    void setCurrentDirection(ChaincodeStub chaincodeStub, TRAIN_DIRECTION direction) {
        this.currentDirection.setValue(chaincodeStub, direction);
    }

    TRAIN_DIRECTION getIntendedDirection(ChaincodeStub chaincodeStub) {
        return this.intendedDirection.getValue(chaincodeStub);
    }

    void setIntendedDirection(ChaincodeStub chaincodeStub, TRAIN_DIRECTION direction) {
        this.intendedDirection.setValue(chaincodeStub, direction);
    }

    TrainPositionVariable getPositionBcVariable() {
        return this.position;
    }

    private TrainPosition getPositionVariable(ChaincodeStub chaincodeStub) {
        return this.getPositionBcVariable().getValue(chaincodeStub);
    }

    private boolean hasPosition(ChaincodeStub chaincodeStub) {
        return this.getPositionVariable(chaincodeStub).hasData();
    }

    TrainInitialisedStatusVariable getInitialisedBcVariable() {
        return this.initialised;
    }

    public boolean isInitialised(ChaincodeStub chaincodeStub) {
        return this.initialised.getValue(chaincodeStub);
    }

    boolean isReady(ChaincodeStub chaincodeStub) {
        return this.isInitialised(chaincodeStub) && this.hasPosition(chaincodeStub);
    }

    public LinkedList<Section> getSectionsPassed(ChaincodeStub chaincodeStub) {
        return this.getPositionVariable(chaincodeStub).getSectionsPassed();
    }

    void setSectionsPassed(ChaincodeStub chaincodeStub, LinkedList<Section> sectionsPassed) {
        if (sectionsPassed.size() > 0) {
            logInfo(chaincodeStub, this.format("First section of sectionspassed = " + sectionsPassed.getFirst().getAssetId()));
        } else {
            logInfo(chaincodeStub, this.format("size of sectionspassed is now 0"));
        }
        this.getPositionBcVariable().setSectionsPassed(chaincodeStub, sectionsPassed);
    }

    Section getCurrentSection(ChaincodeStub chaincodeStub) {
        return this.getPositionVariable(chaincodeStub).getCurrentSection();
    }

    private void setCurrentSection(ChaincodeStub chaincodeStub, Section currentSection) {
        this.setCurrentSection(chaincodeStub, currentSection, true);
    }

    void setCurrentSection(ChaincodeStub chaincodeStub, Section currentSection, boolean correctSpeed) {
        logInfo(chaincodeStub, "Setting current section to " + currentSection.getAssetId());
        this.getPositionBcVariable().setCurrentSection(chaincodeStub, currentSection);
        // Lower our speed if our new section's vmax is lower than our current speed
        if (correctSpeed && this.getIntendedSpeed(chaincodeStub).toDouble() > this.getCurrentSection(chaincodeStub).getVMax().toDouble()) {
            this.initSpeedChange(chaincodeStub, this.getCurrentSection(chaincodeStub).getVMax());
        }
    }

    TrainDestinationVariable getDestinationSectionBcVariable() {
        return this.destination;
    }

    public Section getDestinationSection(ChaincodeStub chaincodeStub) {
        return this.destination.getValue(chaincodeStub);
    }

    public void setDestinationSection(ChaincodeStub chaincodeStub, Section destinationSection) {
        assert (destinationSection != null);
        this.destination.setValue(chaincodeStub, destinationSection);
    }

    LinkedList<Section> getSectionsAheadRouted() {
        return this.sectionsAheadRouted;
    }

    void setSectionsAheadRouted(LinkedList<Section> sectionsAheadRouted) {
        assert (sectionsAheadRouted != null);
        assert (sectionsAheadRouted.size() == TRAIN_N_FUTURE_SECTIONS_PREDICTED);
        this.sectionsAheadRouted = sectionsAheadRouted;
    }

    LinkedList<Section> getSectionsAheadReal() {
        return this.sectionsAheadReal;
    }

    void setSectionsAheadReal(LinkedList<Section> sectionsAheadReal) {
        assert (sectionsAheadReal != null);
        assert (sectionsAheadReal.size() == TRAIN_N_FUTURE_SECTIONS_PREDICTED);
        this.sectionsAheadReal = sectionsAheadReal;
    }

    LinkedList<Sensor> getSensorsAheadRouted() {
        return this.sensorsAheadRouted;
    }

    void setSensorsAheadRouted(LinkedList<Sensor> sensorsAheadRouted) {
        assert (sensorsAheadRouted != null);
        assert (sensorsAheadRouted.size() == TRAIN_N_FUTURE_SECTIONS_PREDICTED);
        this.sensorsAheadRouted = sensorsAheadRouted;
    }

    LinkedList<Sensor> getSensorsAheadReal() {
        return this.sensorsAheadReal;
    }

    void setSensorsAheadReal(LinkedList<Sensor> sensorsAheadReal) {
        assert (sensorsAheadReal != null);
        assert (sensorsAheadReal.size() == TRAIN_N_FUTURE_SECTIONS_PREDICTED);
        this.sensorsAheadReal = sensorsAheadReal;
    }

    public void setInitialised(ChaincodeStub chaincodeStub, boolean initialised) {
        this.initialised.setValue(chaincodeStub, initialised);
    }

    private void assertNonFuturePositionData(ChaincodeStub chaincodeStub) {
        assert (this.getSectionsPassed(chaincodeStub) != null && this.getCurrentSection(chaincodeStub) != null);
        assert (this.getSectionsPassed(chaincodeStub).size() == TRAIN_N_PASSED_SECTIONS_TRACKED);
    }

    private void assertRoutedPositionData(ChaincodeStub chaincodeStub) {
        assert (this.getSectionsAheadRouted() != null);
        assert (this.getSensorsAheadRouted() != null);
        assert (this.getSectionsAheadRouted().size() == TRAIN_N_FUTURE_SECTIONS_PREDICTED);
        assert (this.getSensorsAheadRouted().size() == TRAIN_N_FUTURE_SECTIONS_PREDICTED);
    }

    private void assertRealPositionData(ChaincodeStub chaincodeStub) {
        assert (this.getSectionsAheadReal() != null);
        assert (this.getSensorsAheadReal() != null);
        assert (this.getSectionsAheadReal().size() == TRAIN_N_FUTURE_SECTIONS_PREDICTED);
        assert (this.getSensorsAheadReal().size() == TRAIN_N_FUTURE_SECTIONS_PREDICTED);
    }

    private void assertCompletePositionData(ChaincodeStub chaincodeStub) {
        this.assertNonFuturePositionData(chaincodeStub);
        this.assertRoutedPositionData(chaincodeStub);
        this.assertRealPositionData(chaincodeStub);
    }

    public String dump(ChaincodeStub chaincodeStub) {
        assertCompletePositionData(chaincodeStub);
        StringBuilder result = new StringBuilder(this.aId + " dump:\n\tpassedSections: ");
        for (Section section : this.getSectionsPassed(chaincodeStub)) {
            result.append(section.getAssetId()).append(" ");
        }
        result.append("\n\tcurrentSection: ").append(this.getCurrentSection(chaincodeStub).getAssetId()).append("\n\tsectionsAheadRouted: ");
        for (Section section : this.getSectionsAheadRouted()) {
            result.append(section.getAssetId()).append(" ");
        }
        result.append("\n\tsectionsAheadReal: ");
        for (Section section : this.getSectionsAheadReal()) {
            result.append(section.getAssetId()).append(" ");
        }
        result.append("\n\tsensorsAheadRouted: ");
        for (Sensor sensor : this.getSensorsAheadRouted()) {
            result.append(sensor.getAssetId()).append(" ");
        }
        result.append("\n\tsensorsAheadReal: ");
        for (Sensor sensor : this.getSensorsAheadReal()) {
            result.append(sensor.getAssetId()).append(" ");
        }
        result.append("\n\tdestinationSection: ").append(this.getDestinationSection(chaincodeStub) == null ? "null" : this.getDestinationSection(chaincodeStub).getAssetId()).append("\n");
        return result.toString();
    }

    // Helper function for formatting
    private String toStringFormatSectionAssetId(ChaincodeStub chaincodeStub, Section section) {
        if (section == null) {
            return "???";
        }
        String assetId = section.getAssetId();
        if (this.getDestinationSection(chaincodeStub).equals(section)) {
            assetId = "(" + assetId + ")";
        }
        if (this.getCurrentSection(chaincodeStub).equals(section)) {
            assetId = "[" + assetId + "]";
        }
        if (this.hasReservation(chaincodeStub, section)) {
            assetId = "{" + assetId + "}";
        } else if (section.getReservee(chaincodeStub) != null) {
            assetId = "<" + assetId + ">";
        }
        return assetId;
    }

    public String toString(ChaincodeStub chaincodeStub) {
        StringBuilder result = new StringBuilder(this.aId);
        result.append(", initialised: ").append(this.isInitialised(chaincodeStub));
        result.append(", ready: ").append(this.isReady(chaincodeStub));
        result.append(", current speed: ").append(this.getCurrentSpeed(chaincodeStub).toString());
        result.append(", intended speed: ").append(this.getIntendedSpeed(chaincodeStub).toString());
        result.append(", current direction: ").append(this.getCurrentDirection(chaincodeStub).getName());
        result.append(", intended direction: ").append(this.getIntendedDirection(chaincodeStub).getName());
        result.append(", trajectory: ");

        if (this.getSectionsPassed(chaincodeStub) != null) {
            // Traverse in reverse order, because the first element is the most recently passed section
            for (int i = this.getSectionsPassed(chaincodeStub).size() - 1; i >= 0; i--) {
                Section section = this.getSectionsPassed(chaincodeStub).get(i);
                result.append(this.toStringFormatSectionAssetId(chaincodeStub, section)).append(" > ");
            }
        }
        result.append(this.toStringFormatSectionAssetId(chaincodeStub, this.getCurrentSection(chaincodeStub))).append(" > ");
        if (this.getSectionsAheadRouted() != null) {
            for (Section section : this.getSectionsAheadRouted()) {
                result.append(this.toStringFormatSectionAssetId(chaincodeStub, section));
                if (!this.getSectionsAheadRouted().getLast().equals(section)) {
                    result.append(" > ");
                }
            }
        }
        logInfo(chaincodeStub, this.format("toString(): " + result.toString()));
        return result.toString();
    }
}
