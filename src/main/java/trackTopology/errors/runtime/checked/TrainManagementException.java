package trackTopology.errors.runtime.checked;

// Class used for easy catching of checked Train Management Exceptions
public class TrainManagementException extends Exception {
    TrainManagementException() {
        super();
    }

    public TrainManagementException(Throwable cause) {
        this(cause.getMessage());
    }

    TrainManagementException(String message) {
        super("Train management exception: " + message);
    }

    public TrainManagementException(String message, Throwable cause) {
        super(message, cause);
    }
}
