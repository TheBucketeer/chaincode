package chaincode.blockchainVariables;

import chaincode.DualVariable;
import org.hyperledger.fabric.shim.ChaincodeStub;
import shared.errors.InvalidTrainSpeedException;
import trackTopology.Train;
import trackTopology.errors.blockchain.checked.InvalidDualVariableDataOnBCException;
import trackTopology.errors.blockchain.unchecked.invaliddata.InvalidTrainSpeedOnBCException;

import java.lang.reflect.InvocationTargetException;

import static shared.GlobalConfig.TRAIN_SPEED_KEY_POSTFIX;
import static shared.assetdata.TrainConfig.TRAIN_SPEED;

public class TrainSpeedVariable extends DualBlockchainVariable<TRAIN_SPEED> {
    private Train train;

    public TrainSpeedVariable(Train train) {
        super();
        this.train = train;
    }

    public TrainSpeedVariable(TrainSpeedVariable trainSpeedVariable) {
        super(trainSpeedVariable);
        this.train = trainSpeedVariable.train;
    }

    DualVariable<TRAIN_SPEED> fromBcString(ChaincodeStub chaincodeStub, String bcStringCurrent, String bcStringIntended) {
        try {
            return getDualVariableFromBcString(TRAIN_SPEED.class, bcStringCurrent, bcStringIntended);
        } catch (InvalidDualVariableDataOnBCException e) {
            throw new InvalidTrainSpeedOnBCException(e.getCurrent(), e.getIntended());
        } catch (InvocationTargetException e) {
            if (e.getCause() instanceof InvalidTrainSpeedException) {
                InvalidTrainSpeedException cause = (InvalidTrainSpeedException) e.getCause();
                throw new InvalidTrainSpeedOnBCException(cause.getStateStr());
            } else {
                throw new IllegalStateException(e.getCause());
            }
        }
    }

    String getKey() {
        return this.train.getAssetId() + TRAIN_SPEED_KEY_POSTFIX;
    }
}
