package trackTopology.errors.runtime.unchecked;

import org.hyperledger.fabric.shim.ChaincodeStub;
import trackTopology.Train;

import static shared.GlobalConfig.TRAIN_N_PASSED_SECTIONS_TRACKED;

public class InvalidNumberOfSectionsTrackedException extends TrainManagementRuntimeException {
    public InvalidNumberOfSectionsTrackedException(ChaincodeStub chaincodeStub, Train train) {
        super("Train was tracking " + train.getSectionsPassed(chaincodeStub).size() + " passed sections, but should have tracked " + TRAIN_N_PASSED_SECTIONS_TRACKED);
    }
}
