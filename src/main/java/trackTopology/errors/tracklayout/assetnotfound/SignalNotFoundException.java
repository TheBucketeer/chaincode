package trackTopology.errors.tracklayout.assetnotfound;

public class SignalNotFoundException extends AssetNotFoundException {
    public SignalNotFoundException(String aId) {
        super("Signal not found: " + aId);
    }
}
