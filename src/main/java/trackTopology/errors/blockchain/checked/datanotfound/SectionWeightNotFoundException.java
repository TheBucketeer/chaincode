package trackTopology.errors.blockchain.checked.datanotfound;

import trackTopology.Section;


public class SectionWeightNotFoundException extends BlockchainDataNotFoundException {
    public SectionWeightNotFoundException(Section section) {
        super("Weight of " + section.getAssetId() + " not found on blockchain.");
    }
}
