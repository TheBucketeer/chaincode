package shared.assetdata;

import shared.errors.InvalidTrainCommandException;
import shared.errors.InvalidTrainDirectionException;
import shared.errors.InvalidTrainSpeedException;

import static shared.GlobalConfig.SECTION_DEFAULT_VMAX_DOUBLE;
import static shared.GlobalConfig.stringIsBoolean;

/*
 * Contains general train configuration used by both the clients and chaincode
 */
public class TrainConfig {
    // Train control command types
    public enum TRAIN_CMD_TYPE {
        INIT("initTrain"),
        SET_DIRECTION("setTrainDirection"),
        SET_SPEED("setTrainSpeed");

        private final String name;

        TRAIN_CMD_TYPE(String name) {
            this.name = name;
        }

        public static TRAIN_CMD_TYPE strToTrainCommand(String name) throws InvalidTrainCommandException {
            for (TRAIN_CMD_TYPE trainCmd : TRAIN_CMD_TYPE.values()) {
                if (trainCmd.toString().equals(name)) {
                    return trainCmd;
                }
            }
            throw new InvalidTrainCommandException(name);
        }

        public String toString() {
            return this.name;
        }
    }

    public enum TRAIN_DIRECTION {
        FORWARDS(true, "forwards"),
        BACKWARDS(false, "backwards");

        private final boolean booleanRepr;
        private final String name;

        TRAIN_DIRECTION(boolean booleanRepr, String name) {
            this.booleanRepr = booleanRepr;
            this.name = name;
        }

        public static TRAIN_DIRECTION fromBool(boolean forwards) {
            return forwards ? FORWARDS : BACKWARDS;
        }

        public static TRAIN_DIRECTION fromString(String forwardsBoolean) throws InvalidTrainDirectionException {
            if (!stringIsBoolean(forwardsBoolean)) {
                throw new InvalidTrainDirectionException(forwardsBoolean);
            }
            return fromBool(Boolean.parseBoolean(forwardsBoolean));
        }

        public TRAIN_DIRECTION getOpposite() {
            return fromBool(!this.booleanRepr);
        }

        public boolean toBool() {
            return this.booleanRepr;
        }

        public String getName() {
            return this.name;
        }

        public String toString() {
            return String.valueOf(this.booleanRepr);
        }
    }

    // Class for train speed to enforce bounds checks
    public static class TRAIN_SPEED {
        private final double speedDouble;

        private TRAIN_SPEED(double speedDouble) throws InvalidTrainSpeedException {
            // Round speed to two decimals
            this.speedDouble = Math.round(100 * speedDouble) / 100.0;
            this.checkBounds();
        }

        private TRAIN_SPEED(String speedStr) throws InvalidTrainSpeedException {
            try {
                this.speedDouble = Math.round(100 * Double.parseDouble(speedStr)) / 100.0;
            } catch (NumberFormatException e) {
                throw new InvalidTrainSpeedException(speedStr);
            }
            this.checkBounds();
        }

        public static TRAIN_SPEED getDefaultSpeed() {
            try {
                return new TRAIN_SPEED(SECTION_DEFAULT_VMAX_DOUBLE);
            } catch (InvalidTrainSpeedException e) {
                // This should never happen, so we don't want to have to explicitly check for it
                throw new IllegalStateException(e);
            }
        }

        public static TRAIN_SPEED getStopSpeed() {
            try {
                return new TRAIN_SPEED(-1.0);
            } catch (InvalidTrainSpeedException e) {
                // This should never happen, so we don't want to have to explicitly check for it
                throw new IllegalStateException(e);
            }
        }

        public static TRAIN_SPEED getMaximumSpeed() {
            try {
                return new TRAIN_SPEED(1.0);
            } catch (InvalidTrainSpeedException e) {
                // This should never happen, so we don't want to have to explicitly check for it
                throw new IllegalStateException(e);
            }
        }

        private void checkBounds() throws InvalidTrainSpeedException {
            if (this.speedDouble != -1.0 && (this.speedDouble < 0.0 || this.speedDouble > 1.0)) {
                throw new InvalidTrainSpeedException(this.speedDouble);
            }
        }

        public static TRAIN_SPEED fromDouble(double speedDouble) throws InvalidTrainSpeedException {
            return new TRAIN_SPEED(speedDouble);
        }

        public static TRAIN_SPEED fromString(String speedStr) throws InvalidTrainSpeedException {
            return new TRAIN_SPEED(speedStr);
        }

        public double toDouble() {
            return this.speedDouble;
        }

        public String toString() {
            return String.valueOf(this.speedDouble);
        }

        public boolean equals(TRAIN_SPEED other) {
            return this.speedDouble == other.speedDouble;
        }

        public boolean equals(Object o) {
            return (o instanceof TRAIN_SPEED && this.equals((TRAIN_SPEED) o));
        }
    }
}
