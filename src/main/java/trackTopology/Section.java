package trackTopology;

import chaincode.blockchainVariables.SectionQueueVariable;
import chaincode.blockchainVariables.SectionReserveeVariable;
import chaincode.blockchainVariables.SectionWeightVariable;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.shim.ChaincodeStub;
import shared.assetdata.SectionConfig.SECTION_WEIGHT;
import shared.assetdata.SwitchConfig.SWITCH_STATE;
import shared.assetdata.TrainConfig.TRAIN_SPEED;
import trackTopology.errors.blockchain.unchecked.invaliddata.BlockchainInvalidDataException;
import trackTopology.errors.runtime.checked.SectionAlreadyReservedException;
import trackTopology.errors.runtime.unchecked.InvalidAdditionToWaitingListException;
import trackTopology.errors.runtime.unchecked.InvalidRemovalFromWaitingListException;
import trackTopology.errors.runtime.unchecked.SwitchDoesNotConnectSectionsException;
import trackTopology.errors.runtime.unchecked.TrainManagementRuntimeException;
import trackTopology.errors.tracklayout.NotAdjacentException;
import trackTopology.errors.tracklayout.assetnotfound.TrainNotFoundException;
import trackTopology.errors.tracklayout.setup.AdjacentOutOfBoundsException;
import trackTopology.errors.tracklayout.setup.IncorrectNumberOfDelimitersException;
import trackTopology.errors.tracklayout.setup.MultipleDelimitersInAdjacentSectionsException;
import trackTopology.errors.tracklayout.setup.NoDelimitersInAdjacentSectionsException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static chaincode.BlocktrainCC.logInfo;
import static chaincode.BlocktrainCC.trackLayouts;
import static shared.GlobalConfig.*;


public class Section extends TrackLayoutComponent {
    private static final Log LOG = LogFactory.getLog(Section.class);
    private List<Section> side0, side1;
    private Sensor[] delimiters;
    private Switch sw;
    private LevelCrossing levelCrossing;
    private TRAIN_SPEED vMax;
    private SectionWeightVariable weight = new SectionWeightVariable(this);
    private SectionReserveeVariable reservee = new SectionReserveeVariable(this);
    // First element (head) of list is the first train in line
    private SectionQueueVariable waitingQueue = new SectionQueueVariable(this);
    // List of sections this section is bundled with (ie. sections that must always be reserved with this one)
    private ArrayList<Section> bundledWith = new ArrayList<>();

    Section(int sId) {
        this.aId = String.format("%s%03d", SECTION_AID_PREFIX, sId);
    }

    Section(Section old) {
        this.aId = old.aId;
        this.vMax = old.vMax;
        this.weight = old.weight;
    }

    public static String dumpSections(LinkedList<Section> l) {
        String output = "";
        for (Section section : l) {
            output += " " + section.getAssetId();
        }
        return output;
    }

    // Expands a tuple of sections (last, current) to a list of passed sections of the correct length
    static LinkedList<Section> expandPassedSections(ChaincodeStub chaincodeStub, Section lastSection, Section currentSection) {
        assert (lastSection != null && currentSection != null);
        LinkedList<Section> passedSections = new LinkedList<>();
        passedSections.add(lastSection);
        while (passedSections.size() < TRAIN_N_PASSED_SECTIONS_TRACKED) {
            // Work out the train's supposed movements by calculating backwards
            Section leastRecentlyPassedSection = passedSections.getLast();
            Section secondLeastRecentlyPassedSection;
            if (passedSections.size() == 1) {
                secondLeastRecentlyPassedSection = currentSection;
            } else {
                secondLeastRecentlyPassedSection = passedSections.get(passedSections.size() - 2);
            }
            Section nextInHistory = leastRecentlyPassedSection.calculateExpectedNextSection(chaincodeStub, secondLeastRecentlyPassedSection);
            assert (nextInHistory != null);
            passedSections.addLast(nextInHistory);
        }
        return passedSections;
    }

    // Returns the total weight of a route
    static int getWeightOfRoute(ChaincodeStub chaincodeStub, Section[] route) {
        int result = 0;
        for (Section section : route) {
            assert (section != null);
            result += section.getWeight(chaincodeStub).toInt();
        }
        return result;
    }

    // Returns true if any combination of two consecutive sections occurs more than once in this section array
    static boolean routeIsCyclic(Section[] route) {
        for (int i = 1; i < route.length; i++) {
            for (int j = 1; j < route.length; j++) {
                if (i != j) {
                    assert (route[j - 1] != null);
                    assert (route[i - 1] != null);
                    assert (route[j] != null);
                    assert (route[i] != null);
                    if (route[j - 1].equals(route[i - 1]) && route[j].equals(route[i])) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private static Section[] routeHelper(ChaincodeStub chaincodeStub, Section destination, Section[] currentRoute) {
        assert (currentRoute != null && currentRoute.length >= 2);
        if (routeIsCyclic(currentRoute)) {
            return null;
        }
        Section furthest = currentRoute[currentRoute.length - 1];
        Section secondFurthest = currentRoute[currentRoute.length - 2];
        if (furthest.equals(destination)) {
            // Very elementary routing logic
            return currentRoute;
        } else {
            // Slightly less elementary routing logic
            List<Section> continuations = furthest.getAllOppositeAdjacent(secondFurthest);
            Section[] result = null;
            for (Section option : continuations) {
                // Find shortest, non-cyclic option
                Section[] newCurrentRoute = ArrayUtils.addAll(currentRoute, option);
                Section[] possibleResult = routeHelper(chaincodeStub, destination, newCurrentRoute);
                if (possibleResult != null && (result == null || getWeightOfRoute(chaincodeStub, possibleResult) < getWeightOfRoute(chaincodeStub, result))) {
                    result = possibleResult;
                }
            }
            return result;
        }
    }

    // Given a current section, a next section and a destination, returns the shortest route from current to destination
    static Section[] route(ChaincodeStub chaincodeStub, Section current, Section next, Section destination) {
        assert (current != null && next != null);
        if (destination == null) {
            return new Section[]{};
        } else {
            return routeHelper(chaincodeStub, destination, new Section[]{current, next});
        }
    }

    // Given an adjacent section sIn, this method returns which section (on the other side) trains incoming from sIn
    // will arrive at when exiting this section
    Section calculateExpectedNextSection(ChaincodeStub chaincodeStub, Section sIn) {
        if (!this.isAdjacent(sIn)) {
            throw new TrainManagementRuntimeException(new NotAdjacentException(this, sIn));
        }
        List<Section> incomingSide = this.side0.contains(sIn) ? this.side0 : this.side1;
        List<Section> outgoingSide = this.side0.contains(sIn) ? this.side1 : this.side0;
        assert (incomingSide != outgoingSide);
        if (this.sw == null) {
            // No switch in section
            assert (incomingSide.size() == 1 && outgoingSide.size() == 1);
            return outgoingSide.get(0);
        } else {
            // Switch in section
            assert (incomingSide.size() == 1 && outgoingSide.size() == 2) || (incomingSide.size() == 2 && outgoingSide.size() == 1);
            if (incomingSide.size() == 2 || this.sw.getCurrentState(chaincodeStub) == SWITCH_STATE.OUTWARDS) {
                return outgoingSide.get(0);
            } else {
                return outgoingSide.get(1);
            }
        }
    }

    Section calculateExpectedNextSectionWithSwitchFailure(ChaincodeStub chaincodeStub, Section sIn) {
        Section result;
        if (this.sw != null) {
            this.sw.flipCurrentState(chaincodeStub);
            result = this.calculateExpectedNextSection(chaincodeStub, sIn);
            this.sw.flipCurrentState(chaincodeStub);
        } else {
            result = this.calculateExpectedNextSection(chaincodeStub, sIn);
        }
        return result;
    }

    // Returns the required position of this section's switch to end up in sOut, given that we entered this Section from sIn
    SWITCH_STATE getRequiredSwitchPosition(Section sIn, Section sOut) {
        if (!this.isAdjacent(sIn)) {
            throw new TrainManagementRuntimeException(new NotAdjacentException(this, sIn));
        }
        if (!this.isAdjacent(sOut)) {
            throw new TrainManagementRuntimeException(new NotAdjacentException(this, sOut));
        }
        if (this.sw == null) {
            return null;
        }
        List<Section> incomingSide = this.side0.contains(sIn) ? this.side0 : this.side1;
        List<Section> outgoingSide = this.side0.contains(sOut) ? this.side0 : this.side1;
        if (incomingSide == outgoingSide) {
            throw new SwitchDoesNotConnectSectionsException(this.sw, sIn, sOut);
        }
        assert (incomingSide.size() == 1 && outgoingSide.size() == 2) || (incomingSide.size() == 2 && outgoingSide.size() == 1);
        if (outgoingSide.size() == 2) {
            // Divergent direction
            if (outgoingSide.get(0).equals(sOut)) {
                return SWITCH_STATE.OUTWARDS;
            } else {
                return SWITCH_STATE.INWARDS;
            }
        } else {
            // Convergent direction
            if (incomingSide.get(0).equals(sIn)) {
                return SWITCH_STATE.OUTWARDS;
            } else {
                return SWITCH_STATE.INWARDS;
            }
        }
    }

    // If this section is reserved and another train tried to reserve it, make sure the trains don't wait infinitely for each other
    void detectAndResolveDeadlock(ChaincodeStub chaincodeStub) {
        // Check which sections this section's reservee is waiting for
        LinkedList<Section> reserveeIsWaitingFor = this.getReservee(chaincodeStub).getWaitingForSections(chaincodeStub);
        // For each of these, check if its reservee is waiting for this section
        for (Section otherSection : reserveeIsWaitingFor) {
            Train trainInOtherSection = otherSection.getReservee(chaincodeStub);
            for (Train waitingTrain : this.getWaitingQueue(chaincodeStub)) {
                if (waitingTrain.equals(trainInOtherSection)) {
                    // this.getReservee(chaincodeStub) is in this section and waiting for otherSection
                    // waitingTrain is in otherSection and waiting fo r us
                    boolean reserveeCanTurn = this.getReservee(chaincodeStub).canAdvanceWhenTurnedAround(chaincodeStub) && this.getReservee(chaincodeStub).isReady(chaincodeStub);
                    boolean otherTrainCanTurn = trainInOtherSection.canAdvanceWhenTurnedAround(chaincodeStub) && trainInOtherSection.isReady(chaincodeStub);
                    if (!reserveeCanTurn && !otherTrainCanTurn) {
                        // No train can turn, so do nothing
                        continue;
                    } else if (reserveeCanTurn && !otherTrainCanTurn) {
                        // Turn reservee and return; all trains waiting for this section will be able to move sooner or later
                        logInfo(chaincodeStub, "Turning around " + this.getReservee(chaincodeStub) + " to resolve deadlock with " + trainInOtherSection.getAssetId());
                        this.getReservee(chaincodeStub).initDirectionChange(chaincodeStub);
                        return;
                    } else if (!reserveeCanTurn && otherTrainCanTurn) {
                        // Turn the train in the section section, since it is the only one that will be able to move after a turn
                        // Do not return yet, since there may be more deadlock going on
                        logInfo(chaincodeStub, "Turning around " + trainInOtherSection.getAssetId() + " to resolve deadlock with " + this.getReservee(chaincodeStub));
                        trainInOtherSection.initDirectionChange(chaincodeStub);
                    } else {
                        // Calculate which train would incur the lowest cost after turning around
                        int reserveeCost = this.getReservee(chaincodeStub).getDistancePenaltyOfTurnAround(chaincodeStub);
                        int trainInOtherSectionCost = trainInOtherSection.getDistancePenaltyOfTurnAround(chaincodeStub);
                        if (reserveeCost <= trainInOtherSectionCost) {
                            // If this section's reservee would incur the lowest/an equal cost, turn it around
                            logInfo(chaincodeStub, "Turning around " + this.getReservee(chaincodeStub) + " to resolve deadlock with " + trainInOtherSection.getAssetId());
                            this.getReservee(chaincodeStub).initDirectionChange(chaincodeStub);
                            // Return, all trains waiting for this section will be able to move sooner or later
                            return;
                        } else {
                            // Turn the train in the section otherSection
                            // Do not return yet, since there may be more deadlock going on
                            logInfo(chaincodeStub, "Turning around " + trainInOtherSection.getAssetId() + " to resolve deadlock with " + this.getReservee(chaincodeStub));
                            trainInOtherSection.initDirectionChange(chaincodeStub);
                        }
                    }
                    // If neither train would be able to move after turning, do nothing
                }
            }

        }
    }

    // Reservation handling functions

    void reserve(ChaincodeStub chaincodeStub, Train train) throws SectionAlreadyReservedException {
        if (this.getReservee(chaincodeStub) == null) {
            // Good; asset is free, let's reserve it
            // The caller should ensure this gets updated on the blockchain, through the train's reserved sections
            this.setReservee(chaincodeStub, train);
            LOG.info(train.getAssetId() + " got " + this.aId + ", Section was free.");
        } else if (!this.getReservee(chaincodeStub).equals(train)) {
            // Reject reservation
            throw new SectionAlreadyReservedException(this, train, this.getReservee(chaincodeStub));
        } else {
            LOG.info(train.getAssetId() + " already had " + this.aId + ".");
        }
    }

    // Opens the level crossing (if we have one) and initiates a switch flip (if necessary)
    void prepareForReservee(ChaincodeStub chaincodeStub) {
        assert (this.getReservee(chaincodeStub) != null);
        LOG.info("Preparing for reservee " + this.getReservee(chaincodeStub).getAssetId());
        if (this.levelCrossing != null) {
            LOG.info("Opening " + this.levelCrossing.getAssetId());
            this.levelCrossing.openForTrains(chaincodeStub);
        }
        if (this.sw != null && this.getReservee(chaincodeStub).willPassSection(chaincodeStub, this)) {
            SWITCH_STATE correct = this.getReservee(chaincodeStub).getDesiredSwitchPosition(chaincodeStub, this.sw);
            LOG.info("Switching " + this.sw.getAssetId() + " " + correct.getName());
            this.sw.initSwitchStateChange(chaincodeStub, correct);
        }
    }

    // Returns whether or not this section is reserved for a train, and if it is ready to be entered
    boolean isReadyForReservee(ChaincodeStub chaincodeStub) {
        assert (this.getReservee(chaincodeStub) != null);
        if (this.levelCrossing != null) {
            if (!this.levelCrossing.isOpenForTrains(chaincodeStub)) {
                return false;
            }
        }
        if (this.sw != null && this.getReservee(chaincodeStub).willPassSection(chaincodeStub, this)) {
            return this.sw.getCurrentState(chaincodeStub) == this.getReservee(chaincodeStub).getDesiredSwitchPosition(chaincodeStub, this.sw);
        }
        return true;
    }

    void finishReservation(ChaincodeStub chaincodeStub) {
        assert (this.getReservee(chaincodeStub) != null);
        this.setReservee(chaincodeStub, null);
        if (this.getWaitingQueue(chaincodeStub).size() > 0) {
            // New reservee found
            try {
                this.reserve(chaincodeStub, this.getWaitingQueue(chaincodeStub).pollFirst());
                LOG.info(this.format("Reservation finished, now reserved for " + this.getReservee(chaincodeStub).getAssetId()));
            } catch (SectionAlreadyReservedException e) {
                LOG.error(this.format("Impossible: SectionAlreadyReservedException after this.getReservee(chaincodeStub) = null"));
                throw new IllegalStateException(e);
            }
        } else {
            // No more reservee, open level crossing
            LOG.info(this.format("Reservation finished"));
            if (this.levelCrossing != null) {
                this.levelCrossing.closeForTrains(chaincodeStub);
            }
        }
    }

    void addTrainToQueue(ChaincodeStub chaincodeStub, Train train) {
        if (!this.getWaitingQueue(chaincodeStub).contains(train)) {
            this.getWaitingQueue(chaincodeStub).addLast(train);
        } else {
            throw new InvalidAdditionToWaitingListException(train, this);
        }
    }

    void removeTrainFromQueue(ChaincodeStub chaincodeStub, Train train) {
        if (!this.getWaitingQueue(chaincodeStub).remove(train)) {
            throw new InvalidRemovalFromWaitingListException(train, this);
        }
    }

    boolean hasInWaitingQueue(ChaincodeStub chaincodeStub, Train train) {
        return this.getWaitingQueue(chaincodeStub).contains(train);
    }

    // Getters and setters

    List<List<Section>> getAllAdjacent() {
        return Arrays.asList(this.side0, this.side1);
    }

    Sensor getDelimitingSensor(Section other) throws NotAdjacentException, NoDelimitersInAdjacentSectionsException, MultipleDelimitersInAdjacentSectionsException {
        if (!this.isAdjacent(other)) {
            throw new NotAdjacentException(this, other);
        }
        LinkedList<Sensor> sharedDelimiters = new LinkedList<>();
        for (Sensor s1 : other.delimiters) {
            for (Sensor s2 : this.delimiters) {
                if (s1.equals(s2)) {
                    sharedDelimiters.add(s1);
                }
            }
        }
        if (sharedDelimiters.size() == 0) {
            throw new NoDelimitersInAdjacentSectionsException(this, other);
        } else if (sharedDelimiters.size() > 1) {
            throw new MultipleDelimitersInAdjacentSectionsException(this, other, sharedDelimiters.toArray(new Sensor[0]));
        }
        return sharedDelimiters.getFirst();
    }

    Sensor[] getDelimiters() {
        return this.delimiters;
    }

    public SectionReserveeVariable getReserveeBcVariable() {
        return this.reservee;
    }

    public Train getReservee(ChaincodeStub chaincodeStub) {
        return this.reservee.getValue(chaincodeStub);
    }

    public void setReservee(ChaincodeStub chaincodeStub, Train train) {
        this.reservee.setValue(chaincodeStub, train);
    }

    public SectionQueueVariable getWaitingQueueBcVariable() {
        return this.waitingQueue;
    }

    LinkedList<Train> getWaitingQueue(ChaincodeStub chaincodeStub) {
        return this.waitingQueue.getValue(chaincodeStub);
    }

    void setWaitingQueue(ChaincodeStub chaincodeStub, LinkedList<Train> waitingQueue) {
        assert (waitingQueue != null);
        this.waitingQueue.setValue(chaincodeStub, waitingQueue);
    }

    public void setWaitingQueue(ChaincodeStub chaincodeStub, String[] waitingQueueStr) {
        assert (waitingQueueStr != null);
        LinkedList<Train> queue = new LinkedList<>();
        for (String trainAId : waitingQueueStr) {
            try {
                queue.add(trackLayouts.get(chaincodeStub).getTrainByAId(trainAId));
            } catch (TrainNotFoundException e) {
                throw new BlockchainInvalidDataException(e);
            }
        }
        this.setWaitingQueue(chaincodeStub, queue);
    }

    public String getWaitingQueueAsString(ChaincodeStub chaincodeStub) {
        LinkedList<String> queuedAIds = new LinkedList<>();
        for (Train train : this.getWaitingQueue(chaincodeStub)) {
            queuedAIds.add(train.getAssetId());
        }
        return createCompositeString(queuedAIds.toArray(new String[0]));
    }

    TRAIN_SPEED getVMax() {
        return this.vMax;
    }

    void setVMax(TRAIN_SPEED vMax) {
        assert (vMax != null);
        this.vMax = vMax;
    }

    public SectionWeightVariable getWeightBcVariable() {
        return this.weight;
    }

    public SECTION_WEIGHT getWeight(ChaincodeStub chaincodeStub) {
        return this.weight.getValue(chaincodeStub);
    }

    public void setWeight(ChaincodeStub chaincodeStub, SECTION_WEIGHT weight) {
        assert (weight != null);
        this.weight.setValue(chaincodeStub, weight);
    }

    private List<Section> getAllOppositeAdjacent(Section sIn) {
        if (this.side0.contains(sIn)) {
            return this.side1;
        } else if (this.side1.contains(sIn)) {
            return this.side0;
        } else {
            throw new TrainManagementRuntimeException(new NotAdjacentException(this, sIn));
        }
    }

    private boolean isAdjacent(Section other) {
        return this.side0.contains(other) || this.side1.contains(other);
    }

    List<Section> getSide0() {
        return this.side0;
    }

    void setSide0(List<Section> s0) throws AdjacentOutOfBoundsException {
        assert (s0 != null);
        if (this.side1 != null) {
            if (s0.size() > 2 || s0.size() + this.side1.size() > 3 || s0.size() + this.side1.size() < 2) {
                throw new AdjacentOutOfBoundsException(s0.size(), this.side1.size(), s0.size() + this.side1.size());
            }
        } else {
            if (s0.size() > 2 || s0.size() < 1) {
                throw new AdjacentOutOfBoundsException(s0.size(), 0, s0.size());
            }
        }
        this.side0 = s0;
    }

    List<Section> getSide1() {
        return this.side1;
    }


    void setSide1(List<Section> s1) throws AdjacentOutOfBoundsException {
        assert (s1 != null);
        if (this.side0 != null) {
            if (s1.size() > 2 || s1.size() + this.side0.size() > 3 || s1.size() + this.side0.size() < 2) {
                throw new AdjacentOutOfBoundsException(s1.size(), this.side0.size(), s1.size() + this.side0.size());
            }
        } else {
            if (s1.size() > 2 || s1.size() < 1) {
                throw new AdjacentOutOfBoundsException(0, s1.size(), s1.size());
            }
        }
        this.side1 = s1;
    }

    void setDelimiters(Sensor[] delimiters) throws IncorrectNumberOfDelimitersException {
        assert (delimiters != null);
        if (delimiters.length != 2 && delimiters.length != 3) {
            throw new IncorrectNumberOfDelimitersException(delimiters.length);
        }
        this.delimiters = delimiters;
    }

    void setDelimiters(List<Sensor> delimiters) throws IncorrectNumberOfDelimitersException {
        assert (delimiters != null);
        this.setDelimiters(delimiters.toArray(new Sensor[0]));
    }

    ArrayList<Section> getBundledWith() {
        return this.bundledWith;
    }

    void setBundledWith(ArrayList<Section> bundledWith) {
        assert (bundledWith != null);
        this.bundledWith = bundledWith;
    }

    Switch getSwitch() {
        return this.sw;
    }

    void setSw(Switch sw) {
        assert (sw != null);
        this.sw = sw;
    }

    LevelCrossing getLevelCrossing() {
        return this.levelCrossing;
    }

    void setLevelCrossing(LevelCrossing levelCrossing) {
        assert (levelCrossing != null);
        this.levelCrossing = levelCrossing;
    }

    public String toString(ChaincodeStub chaincodeStub) {
        StringBuilder result = new StringBuilder(this.aId);
        if (this.getReservee(chaincodeStub) != null) {
            result.append(", reserved for ").append(this.getReservee(chaincodeStub).getAssetId());
        }
        if (this.sw != null) {
            result.append(", has switch ").append(this.sw.getAssetId());
        }
        if (this.levelCrossing != null) {
            result.append(", has level crossing ").append(this.levelCrossing.getAssetId());
        }
        result.append(", borders ");
        List<List<Section>> borders = Arrays.asList(this.side0, this.side1);
        for (int j = 0; j < borders.size(); j++) {
            result.append("[");
            for (int k = 0; k < borders.get(j).size(); k++) {
                result.append(borders.get(j).get(k).getAssetId());
                if (k < borders.get(j).size() - 1) {
                    result.append(", ");
                }
            }
            result.append("]");
            if (j < borders.size() - 1) {
                result.append(" and ");
            }
        }
        return result.toString();
    }
}
