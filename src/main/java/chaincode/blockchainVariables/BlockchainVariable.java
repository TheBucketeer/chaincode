package chaincode.blockchainVariables;

import org.hyperledger.fabric.shim.ChaincodeStub;
import trackTopology.errors.blockchain.checked.datanotfound.BlockchainDataNotFoundException;

import static chaincode.BlocktrainCC.*;
import static shared.GlobalConfig.LOG_CC_CHANGES;

public abstract class BlockchainVariable<T> {
    // Store the current value as well as the most recently read value from the blockchain
    T value = null;
    T valueOnBc = null;
    // Reason for this variable: if we get a value while the blockchain has to be ignored, but never get read from the blockchain,
    // we have to know (since we don't want to write to the blockchain)
    private boolean wasReadFromBc = false;

    abstract T fromBcString(ChaincodeStub chaincodeStub, String bcString);

    abstract String toBcString(T value);

    abstract String getKey();

    BlockchainVariable() {
    }

    BlockchainVariable(BlockchainVariable<T> old) {
        this.value = old.value;
        this.valueOnBc = old.valueOnBc;
        this.wasReadFromBc = old.wasReadFromBc;
    }

    T getValueFromBc(ChaincodeStub chaincodeStub) throws BlockchainDataNotFoundException {
        return this.fromBcString(chaincodeStub, getStringStateWrapper(chaincodeStub, this.getKey()));
    }

    T copyValue(T value) {
        return value;
    }

    private void updateFromBc(ChaincodeStub chaincodeStub) {
        try {
            this.value = this.getValueFromBc(chaincodeStub);
            this.valueOnBc = this.copyValue(this.value);
            this.wasReadFromBc = true;
            this.afterBcUpdate(chaincodeStub);
        } catch (BlockchainDataNotFoundException e) {
            logWarn(chaincodeStub, e.getMessage());
        }
    }

    void afterBcUpdate(ChaincodeStub chaincodeStub) {
    }

    boolean mustUpdateBc(ChaincodeStub chaincodeStub) {
        assert (chaincodeStub != null);
        // Update if we must not ignore the blockchain, we have a value and we have been read from the blockchain at least once
        if (!ignoreBc.get(chaincodeStub)) {
            if (forceWriteToBc.get(chaincodeStub)) {
                return true;
            } else {
                if (!this.hasEmptyValue() && this.wasReadFromBc) {
                    // We have read from the blockchain but our current value may be different
                    return !this.value.equals(this.valueOnBc);
                }
            }
        }
        return false;
    }

    public void updateToBcIfNeeded(ChaincodeStub chaincodeStub) {
        String output = "updateToBcIfNeeded() with key " + this.getKey();
        if (this.mustUpdateBc(chaincodeStub)) {
            output += " (will update!)";
            this.updateToBC(chaincodeStub);
        } else {
            output += " (will not update)";
        }
        logInfo(chaincodeStub, output);
    }

    void updateToBC(ChaincodeStub chaincodeStub) {
        putStringStateWrapper(chaincodeStub, this.getKey(), this.toBcString(this.value));
    }

    static String getStringStateWrapper(ChaincodeStub chaincodeStub, String key) throws BlockchainDataNotFoundException {
        String result = chaincodeStub.getStringState(key);
        if (LOG_CC_CHANGES) {
            logInfo(chaincodeStub, "getStringState(" + key + ") = " + result);
        }
        if (result == null || result.length() == 0) {
            throw new BlockchainDataNotFoundException(key);
        }
        return result;
    }

    // Reads value from blockchain, and only writes new value if it is different
    static void putStringStateWrapper(ChaincodeStub chaincodeStub, String key, String value) {
        boolean shouldUpdate;
        try {
            String current = getStringStateWrapper(chaincodeStub, key);
            shouldUpdate = !current.equals(value);
        } catch (BlockchainDataNotFoundException e) {
            shouldUpdate = true;
        }
        if (shouldUpdate) {
            if (LOG_CC_CHANGES) {
                logInfo(chaincodeStub, "putStringState(" + key + ", " + value + ")");
            }
            chaincodeStub.putStringState(key, value);
        }
    }

    public T getValue(ChaincodeStub chaincodeStub) {
        // Update from the blockchain if this has not been done yet, and we must not ignore it
        if (!this.wasReadFromBc && !ignoreBc.get(chaincodeStub)) {
            this.updateFromBc(chaincodeStub);
        }
        return this.value;
    }

    public void setValue(ChaincodeStub chaincodeStub, T value) {
        this.setValue(chaincodeStub, value, false);
    }

    // Fallback values are values that are not read from the blockchain, but rather serve as defaults (fallbacks)
    private void setValue(ChaincodeStub chaincodeStub, T value, boolean isFallBack) {
        // To guarantee that no write is ever done before reading from the blockchain
        T tmp = this.getValue(chaincodeStub);
        logInfo(chaincodeStub, "Updating " + this.getKey() + " from " + tmp + " to " + value);
        // Update if A) this is not a fallback value or B) it is, but we haven't read from the BC yet
        if (!isFallBack || this.hasEmptyValueOnBc()) {
            this.value = value;
        }
    }

    boolean hasEmptyValue() {
        return this.value == null;
    }

    boolean hasEmptyValueOnBc() {
        return this.valueOnBc == null;
    }

    public boolean equals(BlockchainVariable<?> other) {
        return ((this.hasEmptyValue() && other.hasEmptyValue()) || this.value.equals(other.value)) &&
                ((this.hasEmptyValueOnBc() && other.hasEmptyValueOnBc()) || this.valueOnBc.equals(other.valueOnBc)) &&
                this.wasReadFromBc == other.wasReadFromBc;
    }

    public boolean equals(Object other) {
        return other instanceof BlockchainVariable && this.equals((BlockchainVariable<?>) other);
    }
}