package trackTopology;

import org.hyperledger.fabric.shim.ChaincodeStub;

import java.util.LinkedList;

import static shared.GlobalConfig.TRAIN_N_PASSED_SECTIONS_TRACKED;

public class TrainPosition {
    // Current section
    private Section currentSection;
    // First element (head) of list is most recently passed
    private LinkedList<Section> sectionsPassed = new LinkedList<>();

    public TrainPosition() {
    }

    public TrainPosition(ChaincodeStub chaincodeStub, Section lastSection, Section currentSection) {
        this(Section.expandPassedSections(chaincodeStub, lastSection, currentSection), currentSection);
    }

    public TrainPosition(LinkedList<Section> sectionsPassed, Section currentSection) {
        this.setSectionsPassed(sectionsPassed);
        this.setCurrentSection(currentSection);
    }

    public TrainPosition(TrainPosition old) {
        this.currentSection = old.currentSection;
        this.sectionsPassed = new LinkedList<>(old.sectionsPassed);
    }

    boolean hasData() {
        return this.currentSection != null && this.sectionsPassed != null && this.sectionsPassed.size() == TRAIN_N_PASSED_SECTIONS_TRACKED;
    }

    public Section getCurrentSection() {
        return this.currentSection;
    }

    public void setCurrentSection(Section currentSection) {
        assert (currentSection != null);
        this.currentSection = currentSection;
    }

    public LinkedList<Section> getSectionsPassed() {
        return this.sectionsPassed;
    }

    public void setSectionsPassed(LinkedList<Section> sectionsPassed) {
        assert (sectionsPassed != null && sectionsPassed.size() == TRAIN_N_PASSED_SECTIONS_TRACKED);
        for (Section section : sectionsPassed) {
            assert (section != null);
        }
        this.sectionsPassed = sectionsPassed;
    }

    // Since we only store the current and passed sections on the blockchain, we only compare those
    public boolean equals(TrainPosition other) {
        return this.currentSection.equals(other.currentSection) && this.sectionsPassed.equals(other.sectionsPassed);
    }

    public boolean equals(Object other) {
        return other instanceof TrainPosition && this.equals((TrainPosition) other);
    }

    public String[] toStringArray() {
        LinkedList<String> position = new LinkedList<>();
        for (Section section : this.sectionsPassed) {
            position.add(section.getAssetId());
        }
        // Append current section to head of list of tracked passed sections
        position.addFirst(this.currentSection.getAssetId());
        return position.toArray(new String[0]);
    }
}
