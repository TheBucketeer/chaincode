package trackTopology.errors.blockchain.checked.datanotfound;


import trackTopology.LevelCrossing;

public class LevelCrossingStateNotFoundException extends BlockchainDataNotFoundException {
    public LevelCrossingStateNotFoundException(LevelCrossing levelCrossing) {
        super("State of " + levelCrossing.getAssetId() + " not found on blockchain.");
    }
}
