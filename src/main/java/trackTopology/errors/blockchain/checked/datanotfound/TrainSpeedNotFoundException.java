package trackTopology.errors.blockchain.checked.datanotfound;


import trackTopology.Train;

public class TrainSpeedNotFoundException extends BlockchainDataNotFoundException {
    public TrainSpeedNotFoundException(Train train) {
        super("Speed of " + train.getAssetId() + " not found on blockchain.");
    }
}
