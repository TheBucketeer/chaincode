package chaincode.blockchainVariables;

import chaincode.DualVariable;
import org.hyperledger.fabric.shim.ChaincodeStub;
import shared.assetdata.TrainConfig.TRAIN_DIRECTION;
import shared.errors.InvalidTrainDirectionException;
import trackTopology.Train;
import trackTopology.errors.blockchain.checked.InvalidDualVariableDataOnBCException;
import trackTopology.errors.blockchain.unchecked.invaliddata.InvalidTrainDirectionOnBCException;

import java.lang.reflect.InvocationTargetException;

import static shared.GlobalConfig.TRAIN_DIRECTION_KEY_POSTFIX;

public class TrainDirectionVariable extends DualBlockchainVariable<TRAIN_DIRECTION> {
    private Train train;

    public TrainDirectionVariable(Train train) {
        super();
        this.train = train;
    }

    public TrainDirectionVariable(TrainDirectionVariable old) {
        super(old);
        this.train = old.train;
    }

    DualVariable<TRAIN_DIRECTION> fromBcString(ChaincodeStub chaincodeStub, String bcStringCurrent, String bcStringIntended) {
        try {
            return getDualVariableFromBcString(TRAIN_DIRECTION.class, bcStringCurrent, bcStringIntended);
        } catch (InvalidDualVariableDataOnBCException e) {
            throw new InvalidTrainDirectionOnBCException(e.getCurrent(), e.getIntended());
        } catch (InvocationTargetException e) {
            if (e.getCause() instanceof InvalidTrainDirectionException) {
                InvalidTrainDirectionException cause = (InvalidTrainDirectionException) e.getCause();
                throw new InvalidTrainDirectionOnBCException(cause.getStateStr());
            } else {
                throw new IllegalStateException(e.getCause());
            }
        }
    }

    String getKey() {
        return this.train.getAssetId() + TRAIN_DIRECTION_KEY_POSTFIX;
    }
}
