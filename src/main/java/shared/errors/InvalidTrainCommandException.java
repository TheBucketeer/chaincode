package shared.errors;

public class InvalidTrainCommandException extends InvalidDataException {
    public InvalidTrainCommandException(String command) {
        super("train command", command);
    }
}
