package trackTopology.errors.tracklayout;

import trackTopology.Section;

public class NotAdjacentException extends TrackLayoutException {
    public NotAdjacentException(Section s0, Section s1) {
        super("Section " + s0.getAssetId() + " is not adjacent to section " + s1.getAssetId());
    }
}
