package trackTopology.errors.tracklayout.setup;


import trackTopology.errors.tracklayout.TrackLayoutException;

public class IncorrectNumberOfSensorSidesException extends TrackLayoutException {
    public IncorrectNumberOfSensorSidesException(int nSides) {
        super("Sensor has " + nSides + " sides registered (2 required)");
    }
}
