package trackTopology.errors.blockchain.unchecked.invaliddata;


public class InvalidTrainSpeedOnBCException extends BlockchainInvalidDataException {
    public InvalidTrainSpeedOnBCException(String speedState) {
        super("train speed", speedState);
    }

    public InvalidTrainSpeedOnBCException(String currentStateStr, String intendedStateStr) {
        super("train speed", currentStateStr, intendedStateStr);
    }
}
