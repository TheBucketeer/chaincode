package chaincode;

import org.hyperledger.fabric.shim.ChaincodeStub;

import static chaincode.BlocktrainCC.timestamps;
import static shared.GlobalConfig.STATE_CONFIRMATION_TIMEOUT;
import static shared.GlobalConfig.createCompositeString;

public class DualVariable<T> {
    // Last confirmed state (by client)
    private T currentState;
    // Last state for which the chaincode sent a control command to the client
    private T intendedState;
    // Timestamp of the last control command
    private long timestamp;

    public DualVariable() {
    }

    public DualVariable(DualVariable<T> other) {
        this.currentState = other.currentState;
        this.intendedState = other.intendedState;
        this.timestamp = other.timestamp;
    }

    // Returns true if this variable's state change has not yet expired, false otherwise
    public boolean hasExpiredStateChange(ChaincodeStub chaincodeStub) {
        if ((this.currentState != null && !this.currentState.equals(this.intendedState)) ||
                (this.intendedState != null && !this.intendedState.equals(this.currentState))) {
            return timestamps.get(chaincodeStub) - this.timestamp > STATE_CONFIRMATION_TIMEOUT;
        }
        return false;
    }

    public T getCurrentState() {
        return this.currentState;
    }

    public void setCurrentState(T currentState) {
        assert (currentState != null);
        this.currentState = currentState;
    }

    public T getIntendedState() {
        return this.intendedState;
    }

    public void setIntendedState(T intendedState, long timestamp) {
        assert (intendedState != null);
        this.intendedState = intendedState;
        this.timestamp = timestamp;
    }

    public void setIntendedState(ChaincodeStub chaincodeStub, T intendedState) {
        this.setIntendedState(intendedState, timestamps.get(chaincodeStub));
    }

    public String getCurrentStateBcValue() {
        return this.currentState.toString();
    }

    public String getIntendedStateBcValue() {
        return createCompositeString(this.intendedState.toString(), String.valueOf(this.timestamp));
    }

    private boolean equals(DualVariable<?> other) {
        return ((this.intendedState == null && other.intendedState == null) || (this.intendedState != null && this.intendedState.equals(other.intendedState))) &&
                ((this.currentState == null && other.currentState == null) || (this.currentState != null && this.currentState.equals(other.currentState))) &&
                this.timestamp == other.timestamp;
    }

    public boolean equals(Object other) {
        return other instanceof DualVariable && this.equals((DualVariable<?>) other);
    }

    public String toString() {
        return "Current state " + (this.currentState == null ? "???" : this.currentState.toString()) + ", intended state " + (this.intendedState == null ? "???" : this.intendedState.toString()) + ", last command sent at " + this.timestamp;
    }
}
