package chaincode.blockchainVariables;

import org.hyperledger.fabric.shim.ChaincodeStub;
import trackTopology.Train;

import static shared.GlobalConfig.TRAIN_INITIALISED_KEY_POSTFIX;

public class TrainInitialisedStatusVariable extends BlockchainVariable<Boolean> {
    private Train train;

    public TrainInitialisedStatusVariable(Train train) {
        super();
        this.train = train;
        this.value = false;
    }

    public TrainInitialisedStatusVariable(TrainInitialisedStatusVariable old) {
        super(old);
        this.train = old.train;
    }

    Boolean fromBcString(ChaincodeStub chaincodeStub, String bcString) {
        return Boolean.parseBoolean(bcString);
    }

    String toBcString(Boolean initialised) {
        return String.valueOf(initialised);
    }

    String getKey() {
        return this.train.getAssetId() + TRAIN_INITIALISED_KEY_POSTFIX;
    }
}
