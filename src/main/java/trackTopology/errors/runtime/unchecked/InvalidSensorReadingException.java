package trackTopology.errors.runtime.unchecked;

import org.hyperledger.fabric.shim.ChaincodeStub;
import trackTopology.Sensor;
import trackTopology.Train;

public class InvalidSensorReadingException extends TrainManagementRuntimeException {
    public InvalidSensorReadingException(ChaincodeStub chaincodeStub, Train train, Sensor sensor) {
        super(sensor.getAssetId() + " is an invalid next reading for " + train.getAssetId() + "!\n" + train.dump(chaincodeStub));
    }
}
