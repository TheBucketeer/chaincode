package trackTopology.errors.blockchain.checked.datanotfound;


import trackTopology.Train;

public class TrainDestinationNotFoundException extends BlockchainDataNotFoundException {
    public TrainDestinationNotFoundException(Train train) {
        super("Destination of " + train.getAssetId() + " not found on blockchain.");
    }
}
