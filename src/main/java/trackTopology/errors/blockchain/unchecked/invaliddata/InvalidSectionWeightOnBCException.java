package trackTopology.errors.blockchain.unchecked.invaliddata;


public class InvalidSectionWeightOnBCException extends BlockchainInvalidDataException {
    public InvalidSectionWeightOnBCException(String sectionWeightStr) {
        super("section weight", sectionWeightStr);
    }
}
