package shared.errors;

public class InvalidSectionWeightException extends InvalidDataException {
    public InvalidSectionWeightException(String weightStr) {
        super("section weight", weightStr);
    }

    public InvalidSectionWeightException(int weight) {
        this(String.valueOf(weight));
    }
}
