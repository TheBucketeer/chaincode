package chaincode;

import org.hyperledger.fabric.shim.ChaincodeStub;
import org.hyperledger.fabric.shim.ledger.KeyModification;
import org.hyperledger.fabric.shim.ledger.QueryResultsIterator;
import trackTopology.Train;
import trackTopology.errors.blockchain.checked.datanotfound.BlockchainDataNotFoundException;
import trackTopology.errors.blockchain.checked.datanotfound.TrainPositionNotFoundException;

import static chaincode.BlocktrainCC.logInfo;
import static shared.GlobalConfig.LOG_CC_CHANGES;
import static shared.GlobalConfig.TRAIN_POSITION_KEY_POSTFIX;

/*
 * This class contains functions to help with reading values from/updating values to the blockchain
 */
class CCHelper {
    static String getRawTrainPositionFromBC(ChaincodeStub chaincodeStub, Train train) throws TrainPositionNotFoundException {
        return getRawTrainPositionFromBC(chaincodeStub, train, -1, null);
    }

    private static String getRawTrainPositionFromBC(ChaincodeStub chaincodeStub, Train train, long untilTimestampMilli, String txId) throws TrainPositionNotFoundException {
        try {
            return getStringStateWrapper(chaincodeStub, train.getAssetId() + TRAIN_POSITION_KEY_POSTFIX, untilTimestampMilli, txId);
        } catch (BlockchainDataNotFoundException e) {
            throw new TrainPositionNotFoundException(train);
        }
    }

    static String getStringStateWrapper(ChaincodeStub chaincodeStub, String key) throws BlockchainDataNotFoundException {
        return getStringStateWrapper(chaincodeStub, key, -1, null);
    }

    private static String getStringStateWrapper(ChaincodeStub chaincodeStub, String key, long untilTimestampMilli, String untilTxID) throws BlockchainDataNotFoundException {
        String result = null;
        if (untilTxID == null) {
            // Retrieve latest value for key
            result = chaincodeStub.getStringState(key);
            if (LOG_CC_CHANGES) {
                logInfo(chaincodeStub, "getStringState(" + key + ") = " + result);
            }
        } else {
            // Retrieve latest value before given timestamp/txId
            QueryResultsIterator<KeyModification> keyHistory = chaincodeStub.getHistoryForKey(key);
            long resultTimestampMilli = -1;
            String resultTxId = null;
            for (KeyModification k : keyHistory) {
                long timestamp = k.getTimestamp().toEpochMilli();
                if (timestamp <= untilTimestampMilli && timestamp > resultTimestampMilli) {
                    // Assert we don't have two transactions with the same timestamp, since this could lead to errors
                    assert timestamp != untilTimestampMilli || (k.getTxId().equals(untilTxID));
                    resultTimestampMilli = timestamp;
                    resultTxId = k.getTxId();
                    result = k.getStringValue();
                } else if (timestamp > untilTimestampMilli) {
                    logInfo(chaincodeStub, "getStringStateWrapper(" + result + ", timestamp=" + untilTimestampMilli + ") ignoring too new a value " + k.getStringValue() + " (timestamp=" + timestamp + ")");
                }
            }
            if (LOG_CC_CHANGES) {
                logInfo(chaincodeStub, "getStringState(" + key + ", " + untilTimestampMilli + ", " + untilTxID + ") = " + result + " (" + resultTimestampMilli + ", " + resultTxId + ")");
            }
        }
        if (result == null || result.length() == 0) {
            throw new BlockchainDataNotFoundException(key);
        }
        return result;
    }

    static void putStringStateWrapper(ChaincodeStub chaincodeStub, String key, String value) {
        boolean shouldUpdate = false;
        try {
            String current = getStringStateWrapper(chaincodeStub, key);
            shouldUpdate = !current.equals(value);
        } catch (BlockchainDataNotFoundException e) {
            shouldUpdate = true;
        }
        if (shouldUpdate) {
            if (LOG_CC_CHANGES) {
                logInfo(chaincodeStub, "putStringState(" + key + ", " + value + ")");
            }
            chaincodeStub.putStringState(key, value);
        }
    }
}