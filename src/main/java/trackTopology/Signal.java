package trackTopology;

import chaincode.blockchainVariables.SignalAspectVariableSplit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.shim.ChaincodeStub;
import shared.assetdata.SignalConfig.SIGNAL_ASPECT;
import shared.assetdata.TrainConfig.TRAIN_SPEED;

import java.util.List;

import static chaincode.EventHelper.queueSignalCommand;
import static shared.GlobalConfig.SIGNAL_AID_PREFIX;

public class Signal extends TrackLayoutComponent {
    private static final Log LOG = LogFactory.getLog(Signal.class);
    private Section leaving;
    private Section[] entering;
    private Sensor attached;
    private SignalAspectVariableSplit currentAspect = new SignalAspectVariableSplit(this);
    private SignalAspectVariableSplit intendedAspect = new SignalAspectVariableSplit(this, true);

    Signal(ChaincodeStub chaincodeStub, int sId) {
        this.aId = String.format("%s%03d", SIGNAL_AID_PREFIX, sId);
        this.setCurrentAspect(chaincodeStub, SIGNAL_ASPECT.RED);
    }

    Signal(Signal old) {
        this.aId = old.aId;
        this.leaving = old.leaving;
        this.entering = old.entering.clone();
        this.attached = old.attached;
        this.currentAspect = new SignalAspectVariableSplit(old.currentAspect);
        this.intendedAspect = new SignalAspectVariableSplit(old.intendedAspect);
    }

    void determineAndUpdateAspect(ChaincodeStub chaincodeStub) {
        SIGNAL_ASPECT correct = this.determineAspect(chaincodeStub);
        this.initAspectChange(chaincodeStub, correct);
    }

    // Returns the correct aspect for this signal, based on the presence of trains in leaving/entering sections
    private SIGNAL_ASPECT determineAspect(ChaincodeStub chaincodeStub) {
        Train train = this.leaving.getReservee(chaincodeStub);
        if (train != null) {
            for (Section guardedSection : this.entering) {
                if (!train.equals(guardedSection.getReservee(chaincodeStub)) || !guardedSection.isReadyForReservee(chaincodeStub)) {
                    // Train incoming, but one of the sections guarded by this signal is not yet available
                    LOG.info(this.format("Determined to be red because " + guardedSection.getAssetId() + " is not yet available for " + train.getAssetId() + " in " + this.leaving.getAssetId()));
                    return SIGNAL_ASPECT.RED;
                }
            }
            // Determine the lowest maximum speed of any section in this.entering
            TRAIN_SPEED maximumSafeSpeed = null;
            for (Section guardedSection : this.entering) {
                TRAIN_SPEED vMax = guardedSection.getVMax();
                if (maximumSafeSpeed == null || vMax.toDouble() < maximumSafeSpeed.toDouble()) {
                    maximumSafeSpeed = vMax;
                }
            }
            assert (maximumSafeSpeed != null);
            LOG.info(this.format("Determined to be green because all guarded sections are available for " + train.getAssetId() + " in " + this.leaving.getAssetId() + ", and the allowed speed is " + maximumSafeSpeed.toString()));
            if (maximumSafeSpeed.equals(TRAIN_SPEED.getMaximumSpeed())) {
                // Maximum speed
                return SIGNAL_ASPECT.GREEN;
            } else {
                // Lower than maximum speed
                return SIGNAL_ASPECT.RED_GREEN;
            }
        } else {
            LOG.info(this.format("Determined to be red because no train in " + this.leaving.getAssetId()));
            // No train incoming, default state is red
            return SIGNAL_ASPECT.RED;
        }
    }

    public void initAspectChange(ChaincodeStub chaincodeStub, SIGNAL_ASPECT aspect, boolean forceResend) {
        if (this.getIntendedAspect(chaincodeStub) != aspect || forceResend) {
            this.setIntendedAspect(chaincodeStub, aspect);
            queueSignalCommand(chaincodeStub, this, aspect);
        }
    }

    private void initAspectChange(ChaincodeStub chaincodeStub, SIGNAL_ASPECT aspect) {
        this.initAspectChange(chaincodeStub, aspect, false);
    }

    public void updateAspect(ChaincodeStub chaincodeStub, SIGNAL_ASPECT aspect) {
        this.setCurrentAspect(chaincodeStub, aspect);
        LOG.info(this.format("Updated to " + this.getCurrentAspect(chaincodeStub).getName()));
    }

    SignalAspectVariableSplit getCurrentAspectBcVariable() {
        return this.currentAspect;
    }

    SignalAspectVariableSplit getIntendedAspectBcVariable() {
        return this.intendedAspect;
    }

    public SIGNAL_ASPECT getCurrentAspect(ChaincodeStub chaincodeStub) {
        return this.currentAspect.getValue(chaincodeStub);
    }

    void setCurrentAspect(ChaincodeStub chaincodeStub, SIGNAL_ASPECT aspect) {
        this.currentAspect.setValue(chaincodeStub, aspect);
    }

    private SIGNAL_ASPECT getIntendedAspect(ChaincodeStub chaincodeStub) {
        return this.intendedAspect.getValue(chaincodeStub);
    }

    void setIntendedAspect(ChaincodeStub chaincodeStub, SIGNAL_ASPECT aspect) {
        this.intendedAspect.setValue(chaincodeStub, aspect);
    }

    Sensor getAttached() {
        return this.attached;
    }

    void setAttached(Sensor attached) {
        this.attached = attached;
    }

    Section getLeaving() {
        return this.leaving;
    }

    void setLeaving(Section leaving) {
        this.leaving = leaving;
    }

    Section[] getEntering() {
        return this.entering;
    }

    void setEntering(Section[] entering) {
        this.entering = entering;
    }

    void setEntering(List<Section> entering) {
        this.setEntering(entering.toArray(new Section[0]));
    }

    public String toString(ChaincodeStub chaincodeStub) {
        assert (this.attached != null && this.entering != null && this.leaving != null);
        SIGNAL_ASPECT current = this.getCurrentAspect(chaincodeStub);
        SIGNAL_ASPECT intended = this.getIntendedAspect(chaincodeStub);
        StringBuilder enteringStr = new StringBuilder("[");
        for (int i = 0; i < this.entering.length; i++) {
            enteringStr.append(this.entering[i].getAssetId());
            if (i != this.entering.length - 1) {
                enteringStr.append(", ");
            }
        }
        enteringStr.append("]");
        return this.aId + " (current aspect " + (current == null ? "???" : current.getName()) + ", intended aspect " + (intended == null ? "???" : intended.getName()) + "), attached to " + this.attached.getAssetId() + ", guards " + enteringStr + " from " + this.leaving.getAssetId();
    }
}
