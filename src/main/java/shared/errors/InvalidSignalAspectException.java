package shared.errors;

public class InvalidSignalAspectException extends InvalidDataException {
    public InvalidSignalAspectException(String aspectStr) {
        super("signal aspect", aspectStr);
    }

    public InvalidSignalAspectException(int aspect) {
        this(String.valueOf(aspect));
    }
}
