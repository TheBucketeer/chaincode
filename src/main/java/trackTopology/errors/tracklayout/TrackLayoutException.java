package trackTopology.errors.tracklayout;

public class TrackLayoutException extends Exception {

    protected TrackLayoutException() {
        super();
    }

    public TrackLayoutException(Throwable cause) {
        super(cause);
    }

    protected TrackLayoutException(String message) {
        super(message);
    }

    public TrackLayoutException(String message, Throwable cause) {
        super(message, cause);
    }
}
