package chaincode.blockchainVariables;

import org.hyperledger.fabric.shim.ChaincodeStub;
import trackTopology.Section;
import trackTopology.Train;
import trackTopology.errors.blockchain.unchecked.invaliddata.BlockchainInvalidDataException;
import trackTopology.errors.tracklayout.assetnotfound.SectionNotFoundException;

import static chaincode.BlocktrainCC.trackLayouts;
import static shared.GlobalConfig.EMPTY_KEY_STATE;
import static shared.GlobalConfig.TRAIN_DESTINATION_KEY_POSTFIX;

public class TrainDestinationVariable extends BlockchainVariable<Section> {
    private Train train;

    public TrainDestinationVariable(Train train) {
        super();
        this.train = train;
    }

    public TrainDestinationVariable(TrainDestinationVariable old) {
        super(old);
        this.train = old.train;
    }

    Section fromBcString(ChaincodeStub chaincodeStub, String bcString) {
        if (bcString.equals(EMPTY_KEY_STATE)) {
            return null;
        }
        try {
            return trackLayouts.get(chaincodeStub).getSectionByAId(bcString);
        } catch (SectionNotFoundException e) {
            throw new BlockchainInvalidDataException(e);
        }
    }

    String toBcString(Section destination) {
        return destination == null ? EMPTY_KEY_STATE : destination.getAssetId();
    }

    String getKey() {
        return this.train.getAssetId() + TRAIN_DESTINATION_KEY_POSTFIX;
    }
}
