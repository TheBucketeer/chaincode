package trackTopology.errors.blockchain.checked.datanotfound;


public class BlockchainDataNotFoundException extends Exception {
    public BlockchainDataNotFoundException(String msg) {
        super("Data not on blockchain: " + msg);
    }
}
