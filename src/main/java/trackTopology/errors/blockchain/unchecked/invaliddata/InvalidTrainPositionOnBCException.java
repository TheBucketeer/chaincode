package trackTopology.errors.blockchain.unchecked.invaliddata;


public class InvalidTrainPositionOnBCException extends BlockchainInvalidDataException {
    public InvalidTrainPositionOnBCException(String positionState) {
        super("position state", positionState);
    }
}
