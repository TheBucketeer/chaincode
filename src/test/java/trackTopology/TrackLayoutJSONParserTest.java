package trackTopology;

import chaincode.BlocktrainCC;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import trackTopology.errors.tracklayout.assetnotfound.*;

import java.util.*;

import static chaincode.BlocktrainCC.ignoreBc;
import static chaincode.BlocktrainCC.timestamps;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static shared.GlobalConfig.*;

//import chaincode.blockchainVariables.BlockchainVariable;


@RunWith(MockitoJUnitRunner.class)
public class TrackLayoutJSONParserTest {
    private TrackLayout trackLayout;

    private final HashMap<String, String> blockchainStateMock = new HashMap<>();
    @Spy
    BlocktrainCC blocktrainCC;
    @Mock
    ChaincodeStub chaincodeStub;

    @Before
    public void doBefore() {
        timestamps.put(this.chaincodeStub, (long) 0);
        ignoreBc.put(this.chaincodeStub, true);
//        BlockchainVariable.setBlocktrainCC(this.blocktrainCC);
        this.trackLayout = new TrackLayout(this.chaincodeStub, TEST_TRACK_LAYOUT_JSON);
//        BlocktrainCC.trackLayout = this.trackLayout;
    }

    private static int listCountAssetWithAId(List<? extends TrackLayoutComponent> list, String assetId) {
        int count = 0;
        for (TrackLayoutComponent t : list) {
            if (t.getAssetId().equals(assetId)) {
                count++;
            }
        }
        return count;
    }

    private void checkUniqueExistenceOfAssetsInList(int nAssets, String assetIdPrefix, List<? extends TrackLayoutComponent> assetList) {
        for (int i = 0; i < nAssets; i++) {
            String assetId = String.format("%s%03d", assetIdPrefix, i);
            assertThat(listCountAssetWithAId(assetList, assetId)).isEqualTo(1);
        }
    }

    private int countItemInList(List<?> list, Object o) {
        int result = 0;
        for (Object item : list) {
            if (item == o) {
                result++;
            }
        }
        return result;
    }

    private void checkSectionHasDelimiters(String sectionAssetId, String... delimiterAssetIds) throws SectionNotFoundException, SensorNotFoundException {
        Section section = this.trackLayout.getSectionByAId(sectionAssetId);
        Sensor[] delimiters = new Sensor[delimiterAssetIds.length];
        for (int i = 0; i < delimiterAssetIds.length; i++) {
            delimiters[i] = this.trackLayout.getSensorByAId(delimiterAssetIds[i]);
        }
        ArrayList<Sensor> realDelimiters = new ArrayList<>(Arrays.asList(section.getDelimiters()));
        assertThat(realDelimiters.size()).isEqualTo(delimiters.length);
        for (Sensor sensor : delimiters) {
            assertThat(this.countItemInList(realDelimiters, sensor)).isEqualTo(1);
        }
    }

    private void checkSectionIsBundledWith(String... bundleAssetIds) throws SectionNotFoundException {
        ArrayList<Section> bundledSections = new ArrayList<>();
        for (String bundleAssetId : bundleAssetIds) {
            bundledSections.add(this.trackLayout.getSectionByAId(bundleAssetId));
        }
        for (Section section : bundledSections) {
            ArrayList<Section> bundleList = section.getBundledWith();
            assertThat(bundleList.size()).isEqualTo(bundledSections.size() - 1);
            for (Section otherSection : bundledSections) {
                if (section.equals(otherSection)) {
                    continue;
                }
                assertTrue(bundleList.contains(otherSection));
            }
        }
    }

    private void checkSensorHasSides(String sensorAssetId, String... sideAssetIds) throws SensorNotFoundException, SectionNotFoundException {
        Sensor sensor = this.trackLayout.getSensorByAId(sensorAssetId);
        ArrayList<Section> correctSides = new ArrayList<>();
        for (String sectionAssetId : sideAssetIds) {
            correctSides.add(this.trackLayout.getSectionByAId(sectionAssetId));
        }
        List<Section> realSides = sensor.getSides();
        assertThat(realSides.size()).isEqualTo(correctSides.size());
        for (Section side : correctSides) {
            assertTrue(realSides.contains(side));
        }
    }

    private boolean sectionSidesAreEqual(List<Section> realSide, List<Section> correctSide) {
        if (realSide.size() != correctSide.size()) {
            return false;
        }
        for (int i = 0; i < realSide.size(); i++) {
            if (!realSide.get(i).equals(correctSide.get(i))) {
                return false;
            }
        }
        return true;
    }

    private void checkSectionHasSides(String sectionAssetId, List<String> sideAAssetIds, List<String> sideBAssetIds) throws SectionNotFoundException {
        Section section = this.trackLayout.getSectionByAId(sectionAssetId);
        ArrayList<Section> sideA = new ArrayList<>();
        ArrayList<Section> sideB = new ArrayList<>();
        for (String sideAAssetId : sideAAssetIds) {
            sideA.add(this.trackLayout.getSectionByAId(sideAAssetId));
        }
        for (String sideBAssetId : sideBAssetIds) {
            sideB.add(this.trackLayout.getSectionByAId(sideBAssetId));
        }
        if (this.sectionSidesAreEqual(section.getSide0(), sideA)) {
            assertTrue(this.sectionSidesAreEqual(section.getSide1(), sideB));
        } else {
            assertTrue(this.sectionSidesAreEqual(section.getSide0(), sideB));
            assertTrue(this.sectionSidesAreEqual(section.getSide1(), sideA));
        }
    }

    private void checkLevelCrossingContainer(String levelCrossingAssetId, String sectionAssetId) throws LevelCrossingNotFoundException, SectionNotFoundException {
        LevelCrossing levelCrossing = this.trackLayout.getLevelCrossingByAId(levelCrossingAssetId);
        Section container = this.trackLayout.getSectionByAId(sectionAssetId);
        assertThat(levelCrossing.getContainer()).isEqualTo(container);
        assertThat(container.getLevelCrossing()).isEqualTo(levelCrossing);
    }

    private void checkSwitchContainer(String switchAssetId, String sectionAssetId) throws SwitchNotFoundException, SectionNotFoundException {
        Switch sw = this.trackLayout.getSwitchByAId(switchAssetId);
        Section container = this.trackLayout.getSectionByAId(sectionAssetId);
        assertThat(sw.getContainer()).isEqualTo(container);
        assertThat(container.getSwitch()).isEqualTo(sw);
    }

    private void checkSignalData(String signalAssetID, String attachedSensorAssetId, String leavingSectionAssetId, String... enteringSectionAssetIds) throws SignalNotFoundException, SensorNotFoundException, SectionNotFoundException {
        Signal signal = this.trackLayout.getSignalByAId(signalAssetID);
        Sensor attached = this.trackLayout.getSensorByAId(attachedSensorAssetId);
        Section[] enteringSections = new Section[enteringSectionAssetIds.length];
        Section leaving = this.trackLayout.getSectionByAId(leavingSectionAssetId);

        for (int i = 0; i < enteringSectionAssetIds.length; i++) {
            enteringSections[i] = this.trackLayout.getSectionByAId(enteringSectionAssetIds[i]);
        }

        assertThat(signal.getAttached()).isEqualTo(attached);
        assertThat(signal.getLeaving()).isEqualTo(leaving);

        assertThat(signal.getEntering().length).isEqualTo(enteringSections.length);

        List<Section> realEnteringSections = Arrays.asList(signal.getEntering());
        for (Section enteringSection : enteringSections) {
            assertThat(countItemInList(realEnteringSections, enteringSection)).isEqualTo(1);
        }
    }

    @Test
    public void checkExistenceOfAssets() {
        checkUniqueExistenceOfAssetsInList(1, LV_AID_PREFIX, this.trackLayout.getLevelCrossings());
        checkUniqueExistenceOfAssetsInList(15, SECTION_AID_PREFIX, this.trackLayout.getSections());
        checkUniqueExistenceOfAssetsInList(18, SENSOR_AID_PREFIX, this.trackLayout.getSensors());
        checkUniqueExistenceOfAssetsInList(16, SIGNAL_AID_PREFIX, this.trackLayout.getSignals());
        checkUniqueExistenceOfAssetsInList(6, SWITCH_AID_PREFIX, this.trackLayout.getSwitches());
        checkUniqueExistenceOfAssetsInList(2, TRAIN_AID_PREFIX, this.trackLayout.getTrains());
    }

    @Test
    public void checkSectionDelimiters() throws SectionNotFoundException, SensorNotFoundException {
        checkSectionHasDelimiters("Sc000", "Sn000", "Sn001");
        checkSectionHasDelimiters("Sc001", "Sn001", "Sn002");
        checkSectionHasDelimiters("Sc002", "Sn002", "Sn003", "Sn017");
        checkSectionHasDelimiters("Sc003", "Sn003", "Sn004");
        checkSectionHasDelimiters("Sc004", "Sn004", "Sn005", "Sn016");
        checkSectionHasDelimiters("Sc005", "Sn005", "Sn006");
        checkSectionHasDelimiters("Sc006", "Sn006", "Sn007", "Sn009");
        checkSectionHasDelimiters("Sc007", "Sn007", "Sn008");
        checkSectionHasDelimiters("Sc008", "Sn000", "Sn008", "Sn013");
        checkSectionHasDelimiters("Sc009", "Sn009", "Sn010");
        checkSectionHasDelimiters("Sc010", "Sn010", "Sn011", "Sn014");
        checkSectionHasDelimiters("Sc011", "Sn011", "Sn012", "Sn015");
        checkSectionHasDelimiters("Sc012", "Sn012", "Sn013");
        checkSectionHasDelimiters("Sc013", "Sn015", "Sn016");
        checkSectionHasDelimiters("Sc014", "Sn014", "Sn017");
    }

    @Test
    public void checkSectionBundles() throws SectionNotFoundException {
        checkSectionIsBundledWith("Sc013", "Sc014");
    }

    @Test
    public void checkSensorSides() throws SectionNotFoundException, SensorNotFoundException {
        checkSensorHasSides("Sn000", "Sc000", "Sc008");
        checkSensorHasSides("Sn001", "Sc000", "Sc001");
        checkSensorHasSides("Sn002", "Sc001", "Sc002");
        checkSensorHasSides("Sn003", "Sc002", "Sc003");
        checkSensorHasSides("Sn004", "Sc003", "Sc004");
        checkSensorHasSides("Sn005", "Sc004", "Sc005");
        checkSensorHasSides("Sn006", "Sc005", "Sc006");
        checkSensorHasSides("Sn007", "Sc006", "Sc007");
        checkSensorHasSides("Sn008", "Sc007", "Sc008");
        checkSensorHasSides("Sn009", "Sc006", "Sc009");
        checkSensorHasSides("Sn010", "Sc009", "Sc010");
        checkSensorHasSides("Sn011", "Sc010", "Sc011");
        checkSensorHasSides("Sn012", "Sc011", "Sc012");
        checkSensorHasSides("Sn013", "Sc008", "Sc012");
        checkSensorHasSides("Sn014", "Sc010", "Sc014");
        checkSensorHasSides("Sn015", "Sc011", "Sc013");
        checkSensorHasSides("Sn016", "Sc004", "Sc013");
        checkSensorHasSides("Sn017", "Sc002", "Sc014");
    }

    @Test
    public void checkSectionSides() throws SectionNotFoundException {
        //checkSectionHasSides(String sectionAssetId, ArrayList<String> sideAAssetIds, ArrayList<String> sideBAssetIds)
        checkSectionHasSides("Sc000", Collections.singletonList("Sc001"), Collections.singletonList("Sc008"));
        checkSectionHasSides("Sc001", Collections.singletonList("Sc000"), Collections.singletonList("Sc002"));
        checkSectionHasSides("Sc002", Collections.singletonList("Sc001"), Arrays.asList("Sc003", "Sc014"));
        checkSectionHasSides("Sc003", Collections.singletonList("Sc002"), Collections.singletonList("Sc004"));
        checkSectionHasSides("Sc004", Collections.singletonList("Sc005"), Arrays.asList("Sc003", "Sc013"));
        checkSectionHasSides("Sc005", Collections.singletonList("Sc004"), Collections.singletonList("Sc006"));
        checkSectionHasSides("Sc006", Collections.singletonList("Sc005"), Arrays.asList("Sc007", "Sc009"));
        checkSectionHasSides("Sc007", Collections.singletonList("Sc006"), Collections.singletonList("Sc008"));
        checkSectionHasSides("Sc008", Collections.singletonList("Sc000"), Arrays.asList("Sc007", "Sc012"));
        checkSectionHasSides("Sc009", Collections.singletonList("Sc006"), Collections.singletonList("Sc010"));
        checkSectionHasSides("Sc010", Collections.singletonList("Sc009"), Arrays.asList("Sc011", "Sc014"));
        checkSectionHasSides("Sc011", Collections.singletonList("Sc012"), Arrays.asList("Sc010", "Sc013"));
        checkSectionHasSides("Sc012", Collections.singletonList("Sc008"), Collections.singletonList("Sc011"));
        checkSectionHasSides("Sc013", Collections.singletonList("Sc004"), Collections.singletonList("Sc011"));
        checkSectionHasSides("Sc014", Collections.singletonList("Sc002"), Collections.singletonList("Sc010"));
    }

    @Test
    public void checkAssetContainers() throws LevelCrossingNotFoundException, SectionNotFoundException, SwitchNotFoundException {
        checkLevelCrossingContainer("Lv000", "Sc001");
        checkSwitchContainer("Sw000", "Sc002");
        checkSwitchContainer("Sw001", "Sc004");
        checkSwitchContainer("Sw002", "Sc006");
        checkSwitchContainer("Sw003", "Sc008");
        checkSwitchContainer("Sw004", "Sc010");
        checkSwitchContainer("Sw005", "Sc011");
    }

    @Test
    public void checkSignalData() throws SectionNotFoundException, SensorNotFoundException, SignalNotFoundException {
        checkSignalData("Sg000", "Sn000", "Sc000", "Sc008");
        checkSignalData("Sg001", "Sn002", "Sc001", "Sc002");
        checkSignalData("Sg002", "Sn003", "Sc003", "Sc002");
        checkSignalData("Sg003", "Sn004", "Sc003", "Sc004");
        checkSignalData("Sg004", "Sn005", "Sc005", "Sc004");
        checkSignalData("Sg005", "Sn006", "Sc005", "Sc006");
        checkSignalData("Sg006", "Sn007", "Sc007", "Sc006");
        checkSignalData("Sg007", "Sn008", "Sc007", "Sc008");
        checkSignalData("Sg008", "Sn009", "Sc009", "Sc006");
        checkSignalData("Sg009", "Sn010", "Sc009", "Sc010", "Sc011");
        checkSignalData("Sg010", "Sn012", "Sc012", "Sc011", "Sc010");
        checkSignalData("Sg011", "Sn013", "Sc012", "Sc008");
        checkSignalData("Sg012", "Sn017", "Sc014", "Sc002");
        checkSignalData("Sg013", "Sn016", "Sc013", "Sc004");
        checkSignalData("Sg014", "Sn014", "Sc014", "Sc010");
        checkSignalData("Sg015", "Sn015", "Sc013", "Sc011");
    }
}
