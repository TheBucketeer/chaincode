package trackTopology.errors.blockchain.unchecked.invaliddata;

public class InvalidSwitchStateOnBCException extends BlockchainInvalidDataException {
    public InvalidSwitchStateOnBCException(String stateStr) {
        super("switch state", stateStr);
    }

    public InvalidSwitchStateOnBCException(String currentStateStr, String intendedStateStr) {
        super("switch state", currentStateStr, intendedStateStr);
    }
}
