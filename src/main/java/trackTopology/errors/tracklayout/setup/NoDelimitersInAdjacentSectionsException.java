package trackTopology.errors.tracklayout.setup;

import trackTopology.Section;
import trackTopology.errors.tracklayout.TrackLayoutException;

public class NoDelimitersInAdjacentSectionsException extends TrackLayoutException {
    public NoDelimitersInAdjacentSectionsException(Section s0, Section s1) {
        super(s0.getAssetId() + " and " + s1.getAssetId() + " are adjacent but do not share any delimiters");
    }
}
