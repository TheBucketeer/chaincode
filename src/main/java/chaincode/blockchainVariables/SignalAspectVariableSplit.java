package chaincode.blockchainVariables;

import org.hyperledger.fabric.shim.ChaincodeStub;
import shared.assetdata.SignalConfig.SIGNAL_ASPECT;
import shared.errors.InvalidSignalAspectException;
import trackTopology.Signal;
import trackTopology.errors.blockchain.unchecked.invaliddata.InvalidSignalAspectOnBCException;

import static shared.GlobalConfig.ASSET_INTENDED_STATE_POSTFIX;
import static shared.GlobalConfig.SIGNAL_ASPECT_KEY_POSTFIX;

public class SignalAspectVariableSplit extends BlockchainVariable<SIGNAL_ASPECT> {
    private Signal signal;
    private boolean isIntended;

    public SignalAspectVariableSplit(Signal signal) {
        this(signal, false);
    }

    public SignalAspectVariableSplit(Signal signal, boolean isIntended) {
        super();
        this.signal = signal;
        this.isIntended = isIntended;
    }

    public SignalAspectVariableSplit(SignalAspectVariableSplit old) {
        super(old);
        this.signal = old.signal;
    }

    SIGNAL_ASPECT fromBcString(ChaincodeStub chaincodeStub, String bcString) {
        try {
            return SIGNAL_ASPECT.fromString(bcString);
        } catch (InvalidSignalAspectException e) {
            throw new InvalidSignalAspectOnBCException(bcString);
        }
    }

    String toBcString(SIGNAL_ASPECT aspect) {
        return aspect.toString();
    }

    String getKey() {
        return this.signal.getAssetId() + SIGNAL_ASPECT_KEY_POSTFIX + (this.isIntended ? ASSET_INTENDED_STATE_POSTFIX : "");
    }
}
