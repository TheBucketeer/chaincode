package trackTopology.errors.runtime.checked;

import trackTopology.Section;
import trackTopology.Train;

public class SectionAlreadyReservedException extends TrainManagementException {
    public SectionAlreadyReservedException(Section section, Train train, Train reservedFor) {
        super(train.getAssetId() + " attempted to reserve " + section.getAssetId() + ", but was already reserved for " + reservedFor.getAssetId() + ".");
    }
}
