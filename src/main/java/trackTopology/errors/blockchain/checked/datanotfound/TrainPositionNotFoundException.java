package trackTopology.errors.blockchain.checked.datanotfound;


import trackTopology.Train;

public class TrainPositionNotFoundException extends BlockchainDataNotFoundException {
    public TrainPositionNotFoundException(Train train) {
        super("Position of " + train.getAssetId() + " not found on blockchain.");
    }
}
