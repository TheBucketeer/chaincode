package trackTopology.errors.blockchain.checked.datanotfound;


import trackTopology.Train;

public class TrainDirectionNotFoundException extends BlockchainDataNotFoundException {
    public TrainDirectionNotFoundException(Train train) {
        super("Direction of " + train.getAssetId() + " not found on blockchain.");
    }
}
