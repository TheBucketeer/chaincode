package trackTopology.errors.tracklayout.assetnotfound;

import trackTopology.errors.tracklayout.TrackLayoutException;

public class AssetNotFoundException extends TrackLayoutException {
    AssetNotFoundException(String s) {
        super(s);
    }
}
