package trackTopology;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class SignalTest {
//    private TrackLayout tracklayout;
//    @Mock
//    BlocktrainCC blocktrainCC;
//
//    @Before
//    public void doBefore() throws InvalidDataException, TrackLayoutException {
//        EventHelper.setParent(blocktrainCC);
//        BlocktrainCC.trackLayout = tracklayout;
//        tracklayout = new TrackLayout(TEST_TRACK_LAYOUT_JSON);
//        // Test cases are based on a known weight of 1 for all sections
//        for (Section section : tracklayout.getSections()) {
//            section.setWeight(SECTION_WEIGHT.fromInt(1));
//        }
//    }

    @Test
    public void getRouteWeightTest() {
        assertThat(true).isEqualTo(true);
    }
//    @Test
//    public void routeIsCyclicShouldReturnTrue() throws TrackLayoutException {
//        Section[] route = {
//                tracklayout.getSectionByAId("Sc009"),
//                tracklayout.getSectionByAId("Sc010"),
//                tracklayout.getSectionByAId("Sc011"),
//                tracklayout.getSectionByAId("Sc012"),
//                tracklayout.getSectionByAId("Sc008"),
//                tracklayout.getSectionByAId("Sc000"),
//                tracklayout.getSectionByAId("Sc001"),
//                tracklayout.getSectionByAId("Sc002"),
//                tracklayout.getSectionByAId("Sc003"),
//                tracklayout.getSectionByAId("Sc004"),
//                tracklayout.getSectionByAId("Sc005"),
//                tracklayout.getSectionByAId("Sc006"),
//                tracklayout.getSectionByAId("Sc009"),
//                tracklayout.getSectionByAId("Sc010")
//        };
//        assertThat(routeIsCyclic(route)).isEqualTo(true);
//    }
//
//    @Test
//    public void routeIsCyclicShouldReturnFalse() throws TrackLayoutException {
//        Section[] route = new Section[]{
//                tracklayout.getSectionByAId("Sc009"),
//                tracklayout.getSectionByAId("Sc010"),
//                tracklayout.getSectionByAId("Sc011"),
//                tracklayout.getSectionByAId("Sc012")
//        };
//        assertThat(routeIsCyclic(route)).isEqualTo(false);
//        route = new Section[]{
//                tracklayout.getSectionByAId("Sc009"),
//                tracklayout.getSectionByAId("Sc010"),
//                tracklayout.getSectionByAId("Sc011"),
//                tracklayout.getSectionByAId("Sc012"),
//                tracklayout.getSectionByAId("Sc008"),
//                tracklayout.getSectionByAId("Sc000"),
//                tracklayout.getSectionByAId("Sc001"),
//                tracklayout.getSectionByAId("Sc002"),
//                tracklayout.getSectionByAId("Sc003"),
//                tracklayout.getSectionByAId("Sc004"),
//                tracklayout.getSectionByAId("Sc005"),
//                tracklayout.getSectionByAId("Sc006"),
//                tracklayout.getSectionByAId("Sc009")
//        };
//        assertThat(routeIsCyclic(route)).isEqualTo(false);
//    }
//
//    private void routeTestHelper(Section[] route, Section[] correctRoute) {
//        assertThat(route.length).isEqualTo(correctRoute.length);
//        for (int i = 0; i < route.length; i++) {
//            assertThat(route[i]).isEqualTo(correctRoute[i]);
//        }
//    }
//
//    @Test
//    public void routeTest() throws TrackLayoutException {
//        Section[] route, correctRoute;
//
//        route = Section.route(
//                tracklayout.getSectionByAId("Sc001"),
//                tracklayout.getSectionByAId("Sc000"),
//                null
//        );
//        correctRoute = new Section[]{};
//        routeTestHelper(route, correctRoute);
//
//        route = Section.route(
//                tracklayout.getSectionByAId("Sc001"),
//                tracklayout.getSectionByAId("Sc000"),
//                tracklayout.getSectionByAId("Sc009")
//        );
//        correctRoute = new Section[]{
//                tracklayout.getSectionByAId("Sc001"),
//                tracklayout.getSectionByAId("Sc000"),
//                tracklayout.getSectionByAId("Sc008"),
//                tracklayout.getSectionByAId("Sc012"),
//                tracklayout.getSectionByAId("Sc011"),
//                tracklayout.getSectionByAId("Sc010"),
//                tracklayout.getSectionByAId("Sc009")
//        };
//        routeTestHelper(route, correctRoute);
//        route = Section.route(
//                tracklayout.getSectionByAId("Sc004"),
//                tracklayout.getSectionByAId("Sc013"),
//                tracklayout.getSectionByAId("Sc009")
//        );
//        correctRoute = new Section[]{
//                tracklayout.getSectionByAId("Sc004"),
//                tracklayout.getSectionByAId("Sc013"),
//                tracklayout.getSectionByAId("Sc011"),
//                tracklayout.getSectionByAId("Sc012"),
//                tracklayout.getSectionByAId("Sc008"),
//                tracklayout.getSectionByAId("Sc000"),
//                tracklayout.getSectionByAId("Sc001"),
//                tracklayout.getSectionByAId("Sc002"),
//                tracklayout.getSectionByAId("Sc014"),
//                tracklayout.getSectionByAId("Sc010"),
//                tracklayout.getSectionByAId("Sc009"),
//        };
//        routeTestHelper(route, correctRoute);
//        route = Section.route(
//                tracklayout.getSectionByAId("Sc006"),
//                tracklayout.getSectionByAId("Sc007"),
//                tracklayout.getSectionByAId("Sc013")
//        );
//        correctRoute = new Section[]{
//                tracklayout.getSectionByAId("Sc006"),
//                tracklayout.getSectionByAId("Sc007"),
//                tracklayout.getSectionByAId("Sc008"),
//                tracklayout.getSectionByAId("Sc000"),
//                tracklayout.getSectionByAId("Sc001"),
//                tracklayout.getSectionByAId("Sc002"),
//                tracklayout.getSectionByAId("Sc014"),
//                tracklayout.getSectionByAId("Sc010"),
//                tracklayout.getSectionByAId("Sc009"),
//                tracklayout.getSectionByAId("Sc006"),
//                tracklayout.getSectionByAId("Sc005"),
//                tracklayout.getSectionByAId("Sc004"),
//                tracklayout.getSectionByAId("Sc013")
//        };
//        routeTestHelper(route, correctRoute);
//    }
//
//    @Test
//    public void calculateNextPredictedSectionTest() throws SectionNotFoundException {
//        Section testSectionA = this.tracklayout.getSectionByAId("Sc011");
//        Section testSectionB = this.tracklayout.getSectionByAId("Sc001");
//        try {
//            testSectionA.calculateExpectedNextSection(testSectionB);
//            failBecauseExceptionWasNotThrown(TrainManagementRuntimeException.class);
//        } catch (TrainManagementRuntimeException e) {
//            assertThat(e).hasCauseExactlyInstanceOf(NotAdjacentException.class);
//        }
//    }
}
