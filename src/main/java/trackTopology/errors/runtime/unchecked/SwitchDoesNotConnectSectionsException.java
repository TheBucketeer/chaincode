package trackTopology.errors.runtime.unchecked;

import trackTopology.Section;
import trackTopology.Switch;

public class SwitchDoesNotConnectSectionsException extends TrainManagementRuntimeException {
    public SwitchDoesNotConnectSectionsException(Switch sw, Section sIn, Section sOut) {
        super(sw.getAssetId() + " does not connect " + sIn.getAssetId() + " and " + sOut.getAssetId() + ".");
    }
}

