package chaincode;

import org.hyperledger.fabric.shim.ChaincodeStub;
import org.json.JSONObject;
import shared.assetdata.LevelCrossingConfig.LC_STATE;
import shared.assetdata.SignalConfig.SIGNAL_ASPECT;
import shared.assetdata.SwitchConfig.SWITCH_STATE;
import shared.assetdata.TrainConfig.TRAIN_CMD_TYPE;
import shared.assetdata.TrainConfig.TRAIN_DIRECTION;
import shared.assetdata.TrainConfig.TRAIN_SPEED;
import trackTopology.*;

import static chaincode.BlocktrainCC.timestamps;
import static shared.GlobalConfig.*;

public class EventHelper {
    private static BlocktrainCC daddy;

    public static void setParent(BlocktrainCC blocktrainCC) {
        daddy = blocktrainCC;
    }

    static void queueEmergencyStopEvent(ChaincodeStub chaincodeStub) {
        daddy.queueEvent(chaincodeStub, EMERGENCY_STOP_EVENT, "Emergency stop at " + timestamps.get(chaincodeStub));
    }

    public static void queueLCCommand(ChaincodeStub chaincodeStub, LevelCrossing levelCrossing, LC_STATE state) {
        JSONObject lcCmd = new JSONObject();
        lcCmd.put("state", state.toBool());
        daddy.queueEvent(chaincodeStub, LEVEL_CROSSING_EVENT + levelCrossing.getAssetId(), lcCmd);
    }

    static void queueSensorTriggerEvent(ChaincodeStub chaincodeStub, Sensor sensor) {
        daddy.queueEvent(chaincodeStub, SENSOR_TRIGGERED_EVENT + sensor.getAssetId(), "");
    }

    public static void queueSignalCommand(ChaincodeStub chaincodeStub, Signal signal, SIGNAL_ASPECT aspect) {
        JSONObject signalCmd = new JSONObject();
        signalCmd.put("aspect", aspect.toInt());
        daddy.queueEvent(chaincodeStub, SIGNAL_EVENT + signal.getAssetId(), signalCmd);
    }

    public static void queueSwitchCommand(ChaincodeStub chaincodeStub, Switch sw, SWITCH_STATE state) {
        JSONObject switchCmd = new JSONObject();
        switchCmd.put("state", state.toBool());
        daddy.queueEvent(chaincodeStub, SWITCH_EVENT + sw.getAssetId(), switchCmd);
    }

    public static void queueTrainDirectionCommand(ChaincodeStub chaincodeStub, Train train, TRAIN_DIRECTION direction) {
        JSONObject directionCmd = new JSONObject();
        directionCmd.put("type", TRAIN_CMD_TYPE.SET_DIRECTION.toString());
        directionCmd.put("direction", direction.toBool());
        daddy.queueEvent(chaincodeStub, TRAIN_CONTROL_EVENT + train.getAssetId(), directionCmd);
    }

    public static void queueTrainInitCommand(ChaincodeStub chaincodeStub, Train train) {
        JSONObject initCmd = new JSONObject();
        initCmd.put("type", TRAIN_CMD_TYPE.INIT.toString());
        daddy.queueEvent(chaincodeStub, TRAIN_CONTROL_EVENT + train.getAssetId(), initCmd);
    }

    public static void queueTrainSpeedCommand(ChaincodeStub chaincodeStub, Train train, TRAIN_SPEED speed) {
        JSONObject speedCmd = new JSONObject();
        speedCmd.put("type", TRAIN_CMD_TYPE.SET_SPEED.toString());
        speedCmd.put("speed", speed.toDouble());
        daddy.queueEvent(chaincodeStub, TRAIN_CONTROL_EVENT + train.getAssetId(), speedCmd);
    }
}
