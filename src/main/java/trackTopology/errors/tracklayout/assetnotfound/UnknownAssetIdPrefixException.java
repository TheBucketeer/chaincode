package trackTopology.errors.tracklayout.assetnotfound;

public class UnknownAssetIdPrefixException extends AssetNotFoundException {
    public UnknownAssetIdPrefixException(String aId) {
        super("Unknown asset ID prefix: " + aId);
    }
}
