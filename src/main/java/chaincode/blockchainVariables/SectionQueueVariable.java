package chaincode.blockchainVariables;

import org.hyperledger.fabric.shim.ChaincodeStub;
import trackTopology.Section;
import trackTopology.Train;
import trackTopology.errors.blockchain.unchecked.invaliddata.BlockchainInvalidDataException;
import trackTopology.errors.tracklayout.assetnotfound.TrainNotFoundException;

import java.util.LinkedList;

import static chaincode.BlocktrainCC.trackLayouts;
import static shared.GlobalConfig.*;

public class SectionQueueVariable extends BlockchainVariable<LinkedList<Train>> {
    private Section section;

    public SectionQueueVariable(Section section) {
        super();
        this.section = section;
        this.value = new LinkedList<>();
    }

    LinkedList<Train> fromBcString(ChaincodeStub chaincodeStub, String bcString) {
        if (bcString.equals(EMPTY_KEY_STATE)) {
            return new LinkedList<>();
        }
        try {
            String[] waitingQueueStr = parseCompositeString(bcString);
            LinkedList<Train> queue = new LinkedList<>();
            for (String trainAId : waitingQueueStr) {
                queue.add(trackLayouts.get(chaincodeStub).getTrainByAId(trainAId));
            }
            return queue;
        } catch (TrainNotFoundException e) {
            throw new BlockchainInvalidDataException(e);
        }
    }

    String toBcString(LinkedList<Train> queue) {
        if (queue.size() == 0) {
            return EMPTY_KEY_STATE;
        }
        LinkedList<String> queuedAIds = new LinkedList<>();
        for (Train train : queue) {
            queuedAIds.add(train.getAssetId());
        }
        return createCompositeString(queuedAIds.toArray(new String[0]));
    }

    LinkedList<Train> copyValue(LinkedList<Train> value) {
        return new LinkedList<>(value);
    }

    String getKey() {
        return this.section.getAssetId() + SECTION_QUEUE_KEY_POSTFIX;
    }
}
