package trackTopology.errors.blockchain.checked.datanotfound;

import trackTopology.Section;


public class SectionQueueNotFoundException extends BlockchainDataNotFoundException {
    public SectionQueueNotFoundException(Section section) {
        super("Queue of " + section.getAssetId() + " not found on blockchain.");
    }
}
