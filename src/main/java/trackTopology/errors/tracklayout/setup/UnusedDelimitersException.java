package trackTopology.errors.tracklayout.setup;

import trackTopology.Section;
import trackTopology.Sensor;
import trackTopology.errors.tracklayout.TrackLayoutException;

public class UnusedDelimitersException extends TrackLayoutException {
    public UnusedDelimitersException(Section section, Sensor delimiter) {
        super(section.getAssetId() + " has a delimiter that does not border any other section: " + delimiter.getAssetId());
    }
}
