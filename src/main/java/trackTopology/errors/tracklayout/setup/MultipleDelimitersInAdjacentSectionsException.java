package trackTopology.errors.tracklayout.setup;

import trackTopology.Section;
import trackTopology.Sensor;
import trackTopology.errors.tracklayout.TrackLayoutException;

public class MultipleDelimitersInAdjacentSectionsException extends TrackLayoutException {
    public MultipleDelimitersInAdjacentSectionsException(Section s0, Section s1, Sensor... sensors) {
        super(s0.getAssetId() + " and " + s1.getAssetId() + " are adjacent and share more than one delimiter! (" + sensorsToString(sensors) + ")");
    }

    private static String sensorsToString(Sensor... sensors) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < sensors.length; i++) {
            result.append(sensors[i].getAssetId());
            if (i != sensors.length - 1) {
                result.append(", ");
            }
        }
        return result.toString();
    }
}
