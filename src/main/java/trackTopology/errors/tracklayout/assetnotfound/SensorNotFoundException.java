package trackTopology.errors.tracklayout.assetnotfound;

public class SensorNotFoundException extends AssetNotFoundException {
    public SensorNotFoundException(String aId) {
        super("Sensor not found: " + aId);
    }
}
