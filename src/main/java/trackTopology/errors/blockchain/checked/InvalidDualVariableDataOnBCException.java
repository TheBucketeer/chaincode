package trackTopology.errors.blockchain.checked;


public class InvalidDualVariableDataOnBCException extends Exception {
    private final String current, intended;

    public InvalidDualVariableDataOnBCException(String currentStateStr, String intendedStateStr) {
        super("Invalid dual variable data on blockchain, current: " + currentStateStr + ", intended: " + intendedStateStr);
        this.current = currentStateStr;
        this.intended = intendedStateStr;
    }

    public String getCurrent() {
        return this.current;
    }

    public String getIntended() {
        return this.intended;
    }
}
