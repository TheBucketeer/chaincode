package trackTopology;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.shim.ChaincodeStub;
import trackTopology.errors.runtime.checked.MagnetMyrtleException;
import trackTopology.errors.runtime.checked.SensorReadingConflictException;
import trackTopology.errors.runtime.unchecked.TrackPredictionException;
import trackTopology.errors.tracklayout.NotAdjacentException;
import trackTopology.errors.tracklayout.TrackLayoutException;
import trackTopology.errors.tracklayout.assetnotfound.*;
import trackTopology.errors.tracklayout.setup.MissingSensorSideException;
import trackTopology.errors.tracklayout.setup.MultipleDelimitersInAdjacentSectionsException;
import trackTopology.errors.tracklayout.setup.NoDelimitersInAdjacentSectionsException;
import trackTopology.errors.tracklayout.setup.UnusedDelimitersException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static chaincode.BlocktrainCC.timestamps;
import static java.nio.charset.StandardCharsets.UTF_8;
import static shared.GlobalConfig.*;
import static trackTopology.Train.SENSOR_TRIGGER_LIKELIHOOD.NOT_EXPECTED;


public class TrackLayout extends TrackLayoutComponent {
    private static final Log LOG = LogFactory.getLog(TrackLayout.class);
    private List<Section> sections;
    private List<Sensor> sensors;
    private List<Switch> switches;
    private List<Train> trains;
    private List<LevelCrossing> levelCrossings;
    private List<Signal> signals;


    // Constructor that parses a topology-defining JSON string
    public TrackLayout(ChaincodeStub chaincodeStub, String json) {
        this.aId = "TrackLayout";
        TrackLayoutJSONParser trainLayOutJSONParser = new TrackLayoutJSONParser(chaincodeStub, json);
        this.levelCrossings = trainLayOutJSONParser.getLevelCrossings();
        this.sections = trainLayOutJSONParser.getSections();
        this.sensors = trainLayOutJSONParser.getSensors();
        this.signals = trainLayOutJSONParser.getSignals();
        this.switches = trainLayOutJSONParser.getSwitches();
        this.trains = trainLayOutJSONParser.getTrains();
        try {
            this.checkLayoutIntegrity();
        } catch (TrackLayoutException e) {
            // Should never happen, so make them unchecked
            throw new RuntimeException(e);
        }
        if (RANDOM_TRAIN_ROUTES) {
            for (Train train : this.trains) {
                train.setDestinationSection(chaincodeStub, this.getRandomSection(chaincodeStub));
            }
        }
    }

    public TrackLayout(ChaincodeStub chaincodeStub, TrackLayout old) throws TrackLayoutException {
        this.aId = old.aId;
        this.levelCrossings = new LinkedList<>();
        // Maps mapping assets from old to assets from this
        HashMap<LevelCrossing, LevelCrossing> levelCrossingMap = new HashMap<>();
        HashMap<Section, Section> sectionMap = new HashMap<>();
        HashMap<Sensor, Sensor> sensorMap = new HashMap<>();
        HashMap<Signal, Signal> signalMap = new HashMap<>();
        HashMap<Switch, Switch> switchMap = new HashMap<>();
        HashMap<Train, Train> trainMap = new HashMap<>();
        // Copy basic objects
        for (LevelCrossing levelCrossing : old.levelCrossings) {
            LevelCrossing newLc = new LevelCrossing(levelCrossing);
            levelCrossingMap.put(levelCrossing, newLc);
            this.levelCrossings.add(newLc);
        }
        this.sections = new LinkedList<>();
        for (Section section : old.sections) {
            Section newSection = new Section(section);
            sectionMap.put(section, newSection);
            this.sections.add(newSection);
        }
        this.sensors = new LinkedList<>();
        for (Sensor sensor : old.sensors) {
            Sensor newSensor = new Sensor(sensor);
            sensorMap.put(sensor, newSensor);
            this.sensors.add(newSensor);
        }
        this.signals = new LinkedList<>();
        for (Signal signal : old.signals) {
            Signal newSignal = new Signal(signal);
            signalMap.put(signal, newSignal);
            this.signals.add(newSignal);
        }
        this.switches = new LinkedList<>();
        for (Switch sw : old.switches) {
            Switch newSw = new Switch(sw);
            switchMap.put(sw, newSw);
            this.switches.add(newSw);
        }
        this.trains = new LinkedList<>();
        for (Train train : old.trains) {
            Train newTrain = new Train(train);
            trainMap.put(train, newTrain);
            this.trains.add(newTrain);
        }
        // Copy references
        for (int i = 0; i < this.levelCrossings.size(); i++) {
            this.levelCrossings.get(i).setContainer(sectionMap.get(old.levelCrossings.get(i).getContainer()));
        }
        for (int i = 0; i < this.sections.size(); i++) {
            Section oldSection = old.sections.get(i);
            Section newSection = this.sections.get(i);
            List<Sensor> newDelimiters = copyListHelper(Arrays.asList(oldSection.getDelimiters()), sensorMap);
            newSection.setDelimiters(newDelimiters);
            List<Section> newBundledWith = copyListHelper(oldSection.getBundledWith(), sectionMap);
            newSection.setBundledWith(new ArrayList<>(newBundledWith));
            List<Section> newSide0 = copyListHelper(oldSection.getSide0(), sectionMap);
            List<Section> newSide1 = copyListHelper(oldSection.getSide1(), sectionMap);
            newSection.setSide0(newSide0);
            newSection.setSide1(newSide1);
            newSection.setReservee(chaincodeStub, trainMap.get(oldSection.getReservee(chaincodeStub)));
            newSection.setWaitingQueue(chaincodeStub, copyListHelper(oldSection.getWaitingQueue(chaincodeStub), trainMap));
            if (oldSection.getSwitch() != null) {
                newSection.setSw(switchMap.get(oldSection.getSwitch()));
            }
            if (oldSection.getLevelCrossing() != null) {
                newSection.setLevelCrossing(levelCrossingMap.get(oldSection.getLevelCrossing()));
            }
        }
        for (int i = 0; i < this.sensors.size(); i++) {
            this.sensors.get(i).setSides(copyListHelper(old.sensors.get(i).getSides(), sectionMap));
        }
        for (int i = 0; i < this.signals.size(); i++) {
            Signal oldSignal = old.signals.get(i);
            Signal newSignal = this.signals.get(i);
            newSignal.setAttached(sensorMap.get(oldSignal.getAttached()));
            newSignal.setLeaving(sectionMap.get(oldSignal.getLeaving()));
            newSignal.setEntering(copyListHelper(Arrays.asList(oldSignal.getEntering()), sectionMap));
        }
        for (int i = 0; i < this.switches.size(); i++) {
            Switch oldSwitch = old.switches.get(i);
            Switch newSwitch = this.switches.get(i);
            newSwitch.setContainer(sectionMap.get(oldSwitch.getContainer()));
        }
        for (int i = 0; i < this.trains.size(); i++) {
            Train oldTrain = old.trains.get(i);
            Train newTrain = this.trains.get(i);
            newTrain.setAllSections(this.sections);
            newTrain.setCurrentSection(chaincodeStub, sectionMap.get(oldTrain.getCurrentSection(chaincodeStub)), false);
            newTrain.setSectionsPassed(chaincodeStub, copyListHelper(oldTrain.getSectionsPassed(chaincodeStub), sectionMap));
            newTrain.setDestinationSection(chaincodeStub, sectionMap.get(oldTrain.getDestinationSection(chaincodeStub)));
            newTrain.setSectionsAheadRouted(copyListHelper(oldTrain.getSectionsAheadRouted(), sectionMap));
            newTrain.setSectionsAheadReal(copyListHelper(oldTrain.getSectionsAheadReal(), sectionMap));
            newTrain.setSensorsAheadRouted(copyListHelper(oldTrain.getSensorsAheadRouted(), sensorMap));
            newTrain.setSensorsAheadReal(copyListHelper(oldTrain.getSensorsAheadReal(), sensorMap));
        }
    }

    // Helper function for the copy constructor
    private <T extends TrackLayoutComponent> LinkedList<T> copyListHelper(List<T> oldList, HashMap<T, T> map) {
        LinkedList<T> result = new LinkedList<>();
        for (T oldComponent : oldList) {
            result.add(map.get(oldComponent));
        }
        return result;
    }

    // Constructor that parses a topology-defining JSON file
    static TrackLayout fromJsonFile(ChaincodeStub chaincodeStub, String jsonFileName) throws IOException, TrackPredictionException {
        byte[] rawContents = Files.readAllBytes(Paths.get(jsonFileName));
        String contents = new String(rawContents, UTF_8);
        return new TrackLayout(chaincodeStub, contents);
    }

    private String formatTimeoutMsg(String assetId, String dataType, String currentStateStr, String intendedStateStr) {
        return this.format(dataType + " of " + assetId + " should have been " + intendedStateStr + " by now, but is still " + currentStateStr + "!");
    }

    // Update all current track layout data to the blockchain (if necessary)
    public void writeDataToBc(ChaincodeStub chaincodeStub) {
        logInfo(chaincodeStub, this.format("Writing data to blockchain"));
        for (LevelCrossing levelCrossing : this.levelCrossings) {
            levelCrossing.getCurrentBoomsStateBcVariable().updateToBcIfNeeded(chaincodeStub);
            levelCrossing.getIntendedBoomsStateBcVariable().updateToBcIfNeeded(chaincodeStub);
        }
        for (Section section : this.sections) {
            section.getReserveeBcVariable().updateToBcIfNeeded(chaincodeStub);
            section.getWeightBcVariable().updateToBcIfNeeded(chaincodeStub);
            section.getWaitingQueueBcVariable().updateToBcIfNeeded(chaincodeStub);
        }
        for (Signal signal : this.signals) {
            signal.getCurrentAspectBcVariable().updateToBcIfNeeded(chaincodeStub);
            signal.getIntendedAspectBcVariable().updateToBcIfNeeded(chaincodeStub);
        }
        for (Switch sw : this.switches) {
            sw.getCurrentStateBcVariable().updateToBcIfNeeded(chaincodeStub);
            sw.getIntendedStateBcVariable().updateToBcIfNeeded(chaincodeStub);
        }
        for (Train train : this.trains) {
            train.getDestinationSectionBcVariable().updateToBcIfNeeded(chaincodeStub);
            train.getCurrentDirectionBcVariable().updateToBcIfNeeded(chaincodeStub);
            train.getIntendedDirectionBcVariable().updateToBcIfNeeded(chaincodeStub);
            train.getInitialisedBcVariable().updateToBcIfNeeded(chaincodeStub);
            train.getPositionBcVariable().updateToBcIfNeeded(chaincodeStub);
            train.getCurrentSpeedBcVariable().updateToBcIfNeeded(chaincodeStub);
            train.getIntendedSpeedBcVariable().updateToBcIfNeeded(chaincodeStub);
        }
    }

    // Performs sanity checks on the layout to catch mistakes in the JSON
    private void checkLayoutIntegrity() throws NotAdjacentException, UnusedDelimitersException, NoDelimitersInAdjacentSectionsException, MissingSensorSideException, MultipleDelimitersInAdjacentSectionsException {
        // We can't reserve more section than we track
        assert (TRAIN_N_FUTURE_SECTIONS_RESERVED <= TRAIN_N_FUTURE_SECTIONS_PREDICTED);
        assert (TRAIN_N_PASSED_SECTIONS_RESERVED <= TRAIN_N_PASSED_SECTIONS_TRACKED);
        // We must keep track of at least one passed section, otherwise we can't predict the future
        assert (TRAIN_N_PASSED_SECTIONS_TRACKED >= 1);
        // We must keep track of at least one future section, otherwise turning around becomes difficult
        assert (TRAIN_N_FUTURE_SECTIONS_PREDICTED >= 1);
        for (Section section : this.sections) {
            ArrayList<Sensor> delimiters = new ArrayList<>(Arrays.asList(section.getDelimiters()));
            for (List<Section> side : section.getAllAdjacent()) {
                for (Section neighbour : side) {
                    Sensor border = section.getDelimitingSensor(neighbour);
                    List<Section> borderSides = border.getSides();
                    if (!borderSides.contains(section)) {
                        throw new MissingSensorSideException(border, section);
                    }
                    if (!borderSides.contains(neighbour)) {
                        throw new MissingSensorSideException(border, neighbour);
                    }
                    delimiters.remove(border);
                }
            }
            if (delimiters.size() > 0) {
                throw new UnusedDelimitersException(section, delimiters.get(0));
            }
        }
    }

    // Find train that expects this sensor reading to be its next.
    // If no trains were expecting it, or if more than one were, throw exception
    public Train findTrainForSensorTrigger(ChaincodeStub chaincodeStub, Sensor sensor) throws SensorReadingConflictException, MagnetMyrtleException {
        LinkedList<Train> bestCandidates = new LinkedList<>();
        Train.SENSOR_TRIGGER_LIKELIHOOD bestScore = NOT_EXPECTED;
        for (Train candidateTrain : this.trains) {
            if (candidateTrain.isReady(chaincodeStub)) {
                Train.SENSOR_TRIGGER_LIKELIHOOD score = candidateTrain.getLikelihoodOfSensorTrigger(chaincodeStub, sensor);
                if (bestScore == NOT_EXPECTED || score.toInt() > bestScore.toInt()) {
                    bestCandidates.clear();
                    bestCandidates.add(candidateTrain);
                    bestScore = score;
                } else if (score.toInt() == bestScore.toInt()) {
                    bestCandidates.add(candidateTrain);
                }
            }
        }
        if (bestScore == NOT_EXPECTED) {
            throw new MagnetMyrtleException(sensor);
        }
        if (bestCandidates.size() > 1) {
            throw new SensorReadingConflictException(chaincodeStub, sensor, bestCandidates);
        }
        return bestCandidates.getFirst();
    }

    public void updateSignals(ChaincodeStub chaincodeStub) {
        for (Signal signal : this.signals) {
            signal.determineAndUpdateAspect(chaincodeStub);
        }
    }

    public void updateTrainSpeeds(ChaincodeStub chaincodeStub) {
        for (Train train : this.trains) {
            if (train.isReady(chaincodeStub)) {
                train.goIfSafeOrStop(chaincodeStub);
            }
        }
    }

    public void prepareSectionsForReservees(ChaincodeStub chaincodeStub) {
        for (Section section : this.sections) {
            if (section.getReservee(chaincodeStub) != null) {
                section.prepareForReservee(chaincodeStub);
            }
        }
    }

    private Section getRandomSection(ChaincodeStub chaincodeStub) {
        Random random = new Random();
        random.setSeed(timestamps.get(chaincodeStub));
        return this.sections.get(new Random().nextInt(this.sections.size()));
    }

    public TrackLayoutComponent getAssetByAId(String aId) throws AssetNotFoundException {
        String prefix = aId.substring(0, 2);
        switch (prefix) {
            case LV_AID_PREFIX:
                return this.getLevelCrossingByAId(aId);
            case SECTION_AID_PREFIX:
                return this.getSectionByAId(aId);
            case SENSOR_AID_PREFIX:
                return this.getSensorByAId(aId);
            case SIGNAL_AID_PREFIX:
                return this.getSignalByAId(aId);
            case SWITCH_AID_PREFIX:
                return this.getSwitchByAId(aId);
            case TRAIN_AID_PREFIX:
                return this.getTrainByAId(aId);
            default:
                throw new UnknownAssetIdPrefixException(aId);
        }
    }

    public LevelCrossing getLevelCrossingByAId(String aId) throws LevelCrossingNotFoundException {
        for (LevelCrossing levelCrossing : this.levelCrossings) {
            if (levelCrossing.getAssetId().equals(aId)) {
                return levelCrossing;
            }
        }
        throw new LevelCrossingNotFoundException(aId);
    }

    public Section getSectionByAId(String aId) throws SectionNotFoundException {
        for (Section section : this.sections) {
            if (section.getAssetId().equals(aId)) {
                return section;
            }
        }
        throw new SectionNotFoundException(aId);
    }

    public Sensor getSensorByAId(String aId) throws SensorNotFoundException {
        for (Sensor sensor : this.sensors) {
            if (sensor.getAssetId().equals(aId)) {
                return sensor;
            }
        }
        throw new SensorNotFoundException(aId);
    }

    public Signal getSignalByAId(String aId) throws SignalNotFoundException {
        for (Signal signal : this.signals) {
            if (signal.getAssetId().equals(aId)) {
                return signal;
            }
        }
        throw new SignalNotFoundException(aId);
    }

    public Switch getSwitchByAId(String aId) throws SwitchNotFoundException {
        for (Switch sw : this.switches) {
            if (sw.getAssetId().equals(aId)) {
                return sw;
            }
        }
        throw new SwitchNotFoundException(aId);
    }

    public Train getTrainByAId(String aId) throws TrainNotFoundException {
        for (Train train : this.trains) {
            if (train.getAssetId().equals(aId)) {
                return train;
            }
        }
        throw new TrainNotFoundException(aId);
    }

    public List<Section> getSections() {
        return this.sections;
    }

    List<Sensor> getSensors() {
        return this.sensors;
    }

    List<Switch> getSwitches() {
        return this.switches;
    }

    public List<Train> getTrains() {
        return this.trains;
    }

    List<LevelCrossing> getLevelCrossings() {
        return this.levelCrossings;
    }

    List<Signal> getSignals() {
        return this.signals;
    }

    public String toString(ChaincodeStub chaincodeStub) {
        StringBuilder result = new StringBuilder("TrackLayout:\n");
        result.append("\tSensors:\n");
        for (Sensor sensor : this.sensors) {
            result.append("\t\t").append(sensor.toString()).append("\n");
        }
        result.append("\tSections:\n");
        for (Section section : this.sections) {
            result.append("\t\t").append(section.toString(chaincodeStub)).append("\n");
        }
        result.append("\tSignals:\n");
        for (Signal signal : this.signals) {
            result.append("\t\t").append(signal.toString(chaincodeStub)).append("\n");
        }
        result.append("\tLevel crossings:\n");
        for (LevelCrossing levelCrossing : this.levelCrossings) {
            result.append("\t\t").append(levelCrossing.toString(chaincodeStub)).append("\n");
        }
        result.append("\tSwitches:\n");
        for (Switch sw : this.switches) {
            result.append("\t\t").append(sw.toString(chaincodeStub)).append("\n");
        }
        result.append("\tTrains:\n");
        for (Train train : this.trains) {
            result.append("\t\t").append(train.toString(chaincodeStub)).append("\n");
        }
        return result.toString();
    }

    private static void logInfo(ChaincodeStub chaincodeStub, String s) {
        LOG.info("[" + chaincodeStub.getTxId() + "] " + s);
    }

    private static void logWarn(ChaincodeStub chaincodeStub, String s) {
        LOG.warn("[" + chaincodeStub.getTxId() + "] " + s);
    }

    private static void logError(ChaincodeStub chaincodeStub, String s) {
        LOG.error("[" + chaincodeStub.getTxId() + "] " + s);
    }
}
