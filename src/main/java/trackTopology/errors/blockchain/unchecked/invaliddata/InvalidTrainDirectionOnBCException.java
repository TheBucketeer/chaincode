package trackTopology.errors.blockchain.unchecked.invaliddata;


public class InvalidTrainDirectionOnBCException extends BlockchainInvalidDataException {
    public InvalidTrainDirectionOnBCException(String stateStr) {
        super("direction state", stateStr);
    }

    public InvalidTrainDirectionOnBCException(String currentStateStr, String intendedStateStr) {
        super("direction state", currentStateStr, intendedStateStr);
    }
}
