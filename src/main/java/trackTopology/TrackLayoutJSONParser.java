package trackTopology;

import org.hyperledger.fabric.shim.ChaincodeStub;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import shared.assetdata.LevelCrossingConfig.LC_STATE;
import shared.assetdata.SectionConfig.SECTION_WEIGHT;
import shared.assetdata.SignalConfig.SIGNAL_ASPECT;
import shared.assetdata.SwitchConfig.SWITCH_STATE;
import shared.assetdata.TrainConfig.TRAIN_SPEED;
import shared.errors.InvalidLevelCrossingStateException;
import shared.errors.InvalidSignalAspectException;
import shared.errors.InvalidSwitchStateException;
import shared.errors.InvalidTrainSpeedException;
import trackTopology.errors.runtime.unchecked.TrackPredictionException;
import trackTopology.errors.tracklayout.TrackLayoutException;
import trackTopology.errors.tracklayout.setup.AdjacentOutOfBoundsException;
import trackTopology.errors.tracklayout.setup.IncorrectNumberOfDelimitersException;
import trackTopology.errors.tracklayout.setup.IncorrectNumberOfSensorSidesException;

import java.util.*;

import static shared.GlobalConfig.SECTION_DEFAULT_VMAX;
import static shared.assetdata.TrainConfig.TRAIN_DIRECTION;


class TrackLayoutJSONParser {
    private List<LevelCrossing> levelCrossings;
    private List<Section> sections;
    private List<Sensor> sensors;
    private List<Signal> signals;
    private List<Switch> switches;
    private List<Train> trains;

    private final JSONArray levelCrossingsJson, sectionsJson, sectionGroupsJson, sensorsJson, signalsJson, switchesJson, trainsJson;
    private final HashMap<Integer, LevelCrossing> levelCrossingMap = new HashMap<>();
    private final HashMap<Integer, Sensor> sensorMap = new HashMap<>();
    private final HashMap<Integer, Section> sectionMap = new HashMap<>();
    private final HashMap<Integer, Signal> signalMap = new HashMap<>();
    private final HashMap<Integer, Switch> switchMap = new HashMap<>();
    private final HashMap<Integer, Train> trainMap = new HashMap<>();

    TrackLayoutJSONParser(ChaincodeStub chaincodeStub, String json) {
        JSONObject contentsJson = new JSONObject(json);
        this.levelCrossingsJson = contentsJson.getJSONArray("levelcrossings");
        this.sensorsJson = contentsJson.getJSONArray("sensors");
        this.sectionsJson = contentsJson.getJSONArray("sections");
        this.sectionGroupsJson = contentsJson.getJSONArray("section_groups");
        this.signalsJson = contentsJson.getJSONArray("signals");
        this.switchesJson = contentsJson.getJSONArray("switches");
        this.trainsJson = contentsJson.getJSONArray("trains");
        try {
            this.createLevelCrossings(chaincodeStub);
            this.createSensors();
            this.createSections(chaincodeStub);
            this.createSignals(chaincodeStub);
            this.createSwitches(chaincodeStub);
            this.createTrains(chaincodeStub);

            this.levelCrossings = new ArrayList<>(this.levelCrossingMap.values());
            this.sections = new ArrayList<>(this.sectionMap.values());
            this.sensors = new ArrayList<>(this.sensorMap.values());
            this.signals = new ArrayList<>(this.signalMap.values());
            this.switches = new ArrayList<>(this.switchMap.values());
            this.trains = new ArrayList<>(this.trainMap.values());

            this.setSectionDelimiters();
            this.setSectionSides();
            this.setSectionGroups();
            this.setSensorSides();
            this.registerLevelCrossings();
            this.registerSwitches();
            this.setupSignals();
            this.setupTrains(chaincodeStub);
        } catch (JSONException | TrackLayoutException | InvalidLevelCrossingStateException | InvalidSignalAspectException | InvalidSwitchStateException | InvalidTrainSpeedException e) {
            // None of these should ever occur, so make them unchecked
            throw new RuntimeException(e);
        }
    }

    private void createLevelCrossings(ChaincodeStub chaincodeStub) throws JSONException, InvalidLevelCrossingStateException {
        // Create level crossing objects
        for (int i = 0; i < this.levelCrossingsJson.length(); i++) {
            int id = this.levelCrossingsJson.getJSONObject(i).getInt("id");
            LC_STATE state = LC_STATE.fromBool(this.levelCrossingsJson.getJSONObject(i).getBoolean("state"));
            LevelCrossing levelCrossing = new LevelCrossing(id);
            levelCrossing.setCurrentBoomsState(chaincodeStub, state);
            levelCrossing.setIntendedBoomsState(chaincodeStub, state);
            this.levelCrossingMap.put(id, levelCrossing);
        }
    }

    private void createSections(ChaincodeStub chaincodeStub) throws JSONException {
        // Create section objects
        for (int i = 0; i < this.sectionsJson.length(); i++) {
            int id = this.sectionsJson.getJSONObject(i).getInt("id");
            int weight = this.sectionsJson.getJSONObject(i).getInt("weight");
            Section section = new Section(id);
            section.setVMax(SECTION_DEFAULT_VMAX);
            section.setWeight(chaincodeStub, SECTION_WEIGHT.fromInt(weight));
            this.sectionMap.put(id, section);
        }
    }

    private void createSensors() throws JSONException {
        // Create sensor objects
        for (int i = 0; i < this.sensorsJson.length(); i++) {
            int id = this.sensorsJson.getJSONObject(i).getInt("id");
            this.sensorMap.put(id, new Sensor(id));
        }
    }

    private void createSignals(ChaincodeStub chaincodeStub) throws JSONException, InvalidSignalAspectException {
        // Create signal objects
        for (int i = 0; i < this.signalsJson.length(); i++) {
            int id = this.signalsJson.getJSONObject(i).getInt("id");
            int aspectInt = this.signalsJson.getJSONObject(i).getInt("aspect");
            Signal signal = new Signal(chaincodeStub, id);
            SIGNAL_ASPECT aspect = SIGNAL_ASPECT.fromInt(aspectInt);
            signal.setCurrentAspect(chaincodeStub, aspect);
            signal.setIntendedAspect(chaincodeStub, aspect);
            this.signalMap.put(id, signal);
        }
    }

    private void createSwitches(ChaincodeStub chaincodeStub) throws JSONException, InvalidSwitchStateException {
        // Create switch objects
        for (int i = 0; i < this.switchesJson.length(); i++) {
            int id = this.switchesJson.getJSONObject(i).getInt("id");
            SWITCH_STATE state = SWITCH_STATE.fromBool(this.switchesJson.getJSONObject(i).getBoolean("state"));
            Switch sw = new Switch(id);
            sw.setCurrentState(chaincodeStub, state);
            sw.setIntendedState(chaincodeStub, state);
            this.switchMap.put(id, sw);
        }
    }

    private void createTrains(ChaincodeStub chaincodeStub) throws JSONException, InvalidTrainSpeedException {
        // Create train objects
        for (int i = 0; i < this.trainsJson.length(); i++) {
            JSONObject trainJson = this.trainsJson.getJSONObject(i);
            int id = this.trainsJson.getJSONObject(i).getInt("id");
            Train train = new Train(id);

            TRAIN_SPEED speed = TRAIN_SPEED.fromDouble(trainJson.getDouble("speed"));
            TRAIN_DIRECTION direction = TRAIN_DIRECTION.fromBool(trainJson.getBoolean("direction"));
            train.setCurrentSpeed(chaincodeStub, speed);
            train.setIntendedSpeed(chaincodeStub, speed);
            train.setCurrentDirection(chaincodeStub, direction);
            train.setIntendedDirection(chaincodeStub, direction);
            this.trainMap.put(id, train);
        }
    }

    private void setSectionDelimiters() throws IncorrectNumberOfDelimitersException, JSONException {
        // Set the delimiting sensors for each section
        for (int i = 0; i < this.sectionsJson.length(); i++) {
            JSONArray delimiterIds = this.sectionsJson.getJSONObject(i).getJSONArray("delimiters");
            ArrayList<Sensor> delimiters = new ArrayList<>();
            for (int j = 0; j < delimiterIds.length(); j++) {
                delimiters.add(this.sensorMap.get(delimiterIds.getInt(j)));
            }
            Sensor[] delimitersArray = delimiters.toArray(new Sensor[0]);
            int id = this.sectionsJson.getJSONObject(i).getInt("id");
            this.sectionMap.get(id).setDelimiters(delimitersArray);
        }
    }

    private void setSectionGroups() {
        // Register which sections are bundled together
        for (int i = 0; i < this.sectionGroupsJson.length(); i++) {
            JSONArray bundleJson = this.sectionGroupsJson.getJSONArray(i);
            ArrayList<Section> bundledSections = new ArrayList<>();
            for (int j = 0; j < bundleJson.length(); j++) {
                bundledSections.add(this.sectionMap.get(bundleJson.getInt(j)));
            }
            // For every section in the bundle, register the entire bundle minus the section itself
            for (Section section : bundledSections) {
                ArrayList<Section> bundledSectionsClone = new ArrayList<>(bundledSections);
                bundledSectionsClone.remove(section);
                section.setBundledWith(bundledSectionsClone);
            }
        }
    }

    private void setSensorSides() throws IncorrectNumberOfSensorSidesException, JSONException {
        // Register the neighbouring sections for every sensor
        for (int i = 0; i < this.sensorsJson.length(); i++) {
            JSONObject sensorJson = this.sensorsJson.getJSONObject(i);
            int id = sensorJson.getInt("id");
            JSONArray sideIds = sensorJson.getJSONArray("sides");
            this.sensorMap.get(id).setSides(Arrays.asList(this.sectionMap.get(sideIds.getInt(0)), this.sectionMap.get(sideIds.getInt(1))));
        }
    }

    // Helper function for setSectionSides
    private ArrayList<Section> JSONArrayToSectionList(JSONArray jsonArray) {
        ArrayList<Section> sectionList = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            sectionList.add(this.sectionMap.get(jsonArray.getInt(i)));
        }
        return sectionList;
    }

    private void setSectionSides() throws AdjacentOutOfBoundsException, JSONException {
        // Register the neighbouring sections for every section
        for (int i = 0; i < this.sectionsJson.length(); i++) {
            JSONObject sectionJson = this.sectionsJson.getJSONObject(i);
            int id = sectionJson.getInt("id");
            JSONArray side0Json = sectionJson.getJSONArray("side0");
            JSONArray side1Json = sectionJson.getJSONArray("side1");
            this.sectionMap.get(id).setSide0(JSONArrayToSectionList(side0Json));
            this.sectionMap.get(id).setSide1(JSONArrayToSectionList(side1Json));
        }
    }

    private void registerLevelCrossings() {
        // Register the level crossings with their containers and vice versa
        for (int i = 0; i < this.levelCrossingsJson.length(); i++) {
            JSONObject levelCrossingJson = this.levelCrossingsJson.getJSONObject(i);
            int levelCrossingId = levelCrossingJson.getInt("id");
            int containerId = levelCrossingJson.getInt("section");
            this.levelCrossingMap.get(levelCrossingId).setContainer(this.sectionMap.get(containerId));
            this.sectionMap.get(containerId).setLevelCrossing(this.levelCrossingMap.get(levelCrossingId));
        }
    }

    private void registerSwitches() throws AdjacentOutOfBoundsException {
        // Register the switches with their containers and vice versa
        for (int i = 0; i < this.switchesJson.length(); i++) {
            JSONObject switchJson = this.switchesJson.getJSONObject(i);
            int switchId = switchJson.getInt("id");
            int containerId = switchJson.getInt("container");
            this.switchMap.get(switchId).setContainer(this.sectionMap.get(containerId));
            this.sectionMap.get(containerId).setSw(this.switchMap.get(switchId));
        }
    }

    private void setupSignals() {
        // Set up the signals' leaving, entering and attached fields
        for (int i = 0; i < this.signalsJson.length(); i++) {
            JSONObject signalJson = this.signalsJson.getJSONObject(i);
            Signal signal = this.signalMap.get(signalJson.getInt("id"));
            int attachedSensorId = signalJson.getInt("attached");
            JSONArray enteringSectionIds = signalJson.getJSONArray("entering");
            int leavingSectionId = signalJson.getInt("leaving");

            Section[] entering = new Section[enteringSectionIds.length()];
            for (int j = 0; j < entering.length; j++) {
                entering[j] = this.sectionMap.get(enteringSectionIds.getInt(j));
            }

            signal.setAttached(this.sensorMap.get(attachedSensorId));
            signal.setEntering(entering);
            signal.setLeaving(this.sectionMap.get(leavingSectionId));
        }
    }

    private void setupTrains(ChaincodeStub chaincodeStub) throws TrackPredictionException {
        // Set up the trains' current and next section fields
        for (int i = 0; i < this.trainsJson.length(); i++) {
            JSONObject trainJson = this.trainsJson.getJSONObject(i);
            Train train = this.trainMap.get(trainJson.getInt("id"));
            train.setAllSections(this.sections);
            Section last = this.sectionMap.get(trainJson.getInt("last_section"));
            Section current = this.sectionMap.get(trainJson.getInt("current_section"));
            Section destination = this.sectionMap.get(trainJson.getInt("destination_section"));
            train.setDestinationSection(chaincodeStub, destination);
            LinkedList<Section> passedSections = Section.expandPassedSections(chaincodeStub, last, current);
            train.setPositionAndReserve(chaincodeStub, passedSections, current);
        }
    }

    List<LevelCrossing> getLevelCrossings() {
        return this.levelCrossings;
    }

    List<Section> getSections() {
        return this.sections;
    }

    List<Sensor> getSensors() {
        return this.sensors;
    }

    List<Signal> getSignals() {
        return this.signals;
    }

    List<Switch> getSwitches() {
        return this.switches;
    }

    List<Train> getTrains() {
        return this.trains;
    }
}
