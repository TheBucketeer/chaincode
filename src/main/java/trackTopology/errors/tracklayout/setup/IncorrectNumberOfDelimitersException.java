package trackTopology.errors.tracklayout.setup;

import trackTopology.errors.tracklayout.TrackLayoutException;

public class IncorrectNumberOfDelimitersException extends TrackLayoutException {
    public IncorrectNumberOfDelimitersException(int nDelimiters) {
        super("Attempting to register incorrect number of section delimiters: " + nDelimiters);
    }
}
