package shared.assetdata;

import shared.errors.InvalidSignalAspectException;

/*
 * Contains general signal configuration used by both the clients and chaincode
 */
public class SignalConfig {
    public enum SIGNAL_ASPECT {
        OFF(0, "off"),
        RED(1, "red"),
        RED_GREEN(2, "red+green"),
        GREEN(3, "green");

        private final int number;
        private final String name;

        SIGNAL_ASPECT(int number, String name) {
            this.number = number;
            this.name = name;
        }

        public static SIGNAL_ASPECT fromInt(int number) throws InvalidSignalAspectException {
            for (SIGNAL_ASPECT aspect : SIGNAL_ASPECT.values()) {
                if (aspect.toInt() == number) {
                    return aspect;
                }
            }
            throw new InvalidSignalAspectException(number);
        }

        public static SIGNAL_ASPECT fromString(String numberAsStr) throws InvalidSignalAspectException {
            try {
                int number = Integer.parseInt(numberAsStr);
                return fromInt(number);
            } catch (NumberFormatException e) {
                throw new InvalidSignalAspectException(numberAsStr);
            }
        }

        public int toInt() {
            return this.number;
        }

        public String getName() {
            return this.name;
        }

        public String toString() {
            return String.valueOf(this.number);
        }
    }
}
