package trackTopology.errors.blockchain.unchecked.invaliddata;


public class InvalidSignalAspectOnBCException extends BlockchainInvalidDataException {
    public InvalidSignalAspectOnBCException(String aspectStr) {
        super("signal aspect", aspectStr);
    }

    public InvalidSignalAspectOnBCException(String currentStateStr, String intendedStateStr) {
        super("signal aspect", currentStateStr, intendedStateStr);
    }
}
