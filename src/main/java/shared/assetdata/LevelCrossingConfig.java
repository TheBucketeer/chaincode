package shared.assetdata;

import shared.errors.InvalidLevelCrossingStateException;

import static shared.GlobalConfig.stringIsBoolean;

/*
 * Contains general level crossing configuration used by both the clients and chaincode
 */
public class LevelCrossingConfig {
    public enum LC_STATE {
        OPEN_FOR_TRAINS(true, "open for trains"),
        CLOSED_FOR_TRAINS(false, "closed for trains");

        private final boolean stateAsBool;
        private final String description;

        LC_STATE(boolean boolState, String description) {
            this.stateAsBool = boolState;
            this.description = description;
        }

        public static LC_STATE fromBool(boolean stateBool) throws InvalidLevelCrossingStateException {
            for (LC_STATE state : LC_STATE.values()) {
                if (state.toBool() == stateBool) {
                    return state;
                }
            }
            throw new InvalidLevelCrossingStateException(stateBool);
        }

        public static LC_STATE fromString(String stateStr) throws InvalidLevelCrossingStateException {
            if (stringIsBoolean(stateStr)) {
                return fromBool(Boolean.parseBoolean(stateStr));
            }
            throw new InvalidLevelCrossingStateException(stateStr);
        }

        public boolean toBool() {
            return this.stateAsBool;
        }

        public LC_STATE getOpposite() throws InvalidLevelCrossingStateException {
            return fromBool(!this.stateAsBool);
        }

        public String getName() {
            return this.description;
        }

        public String toString() {
            return String.valueOf(this.stateAsBool);
        }
    }
}
