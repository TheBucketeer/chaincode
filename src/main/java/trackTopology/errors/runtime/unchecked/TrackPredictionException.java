package trackTopology.errors.runtime.unchecked;

import trackTopology.Train;

public class TrackPredictionException extends TrainManagementRuntimeException {
    public TrackPredictionException(Train train, Throwable cause) {
        super("Error predicting track for " + train.getAssetId() + ": " + cause.getMessage());
    }
}
