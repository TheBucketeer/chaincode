package chaincode.blockchainVariables;

import chaincode.DualVariable;
import org.hyperledger.fabric.shim.ChaincodeStub;
import shared.assetdata.LevelCrossingConfig.LC_STATE;
import shared.errors.InvalidLevelCrossingStateException;
import trackTopology.LevelCrossing;
import trackTopology.errors.blockchain.checked.InvalidDualVariableDataOnBCException;
import trackTopology.errors.blockchain.unchecked.invaliddata.InvalidLevelCrossingStateOnBCException;

import java.lang.reflect.InvocationTargetException;

import static shared.GlobalConfig.LEVEL_CROSSING_KEY_POSTFIX;


public class LevelCrossingStateVariable extends DualBlockchainVariable<LC_STATE> {
    private LevelCrossing levelCrossing;

    public LevelCrossingStateVariable(LevelCrossing levelCrossing) {
        super();
        this.levelCrossing = levelCrossing;
    }

    public LevelCrossingStateVariable(LevelCrossingStateVariable levelCrossingStateVariable) {
        super(levelCrossingStateVariable);
        this.levelCrossing = levelCrossingStateVariable.levelCrossing;
    }

    DualVariable<LC_STATE> fromBcString(ChaincodeStub chaincodeStub, String bcStringCurrent, String bcStringIntended) {
        try {
            return getDualVariableFromBcString(LC_STATE.class, bcStringCurrent, bcStringIntended);
        } catch (InvalidDualVariableDataOnBCException e) {
            throw new InvalidLevelCrossingStateOnBCException(e.getCurrent(), e.getIntended());
        } catch (InvocationTargetException e) {
            if (e.getCause() instanceof InvalidLevelCrossingStateException) {
                InvalidLevelCrossingStateException cause = (InvalidLevelCrossingStateException) e.getCause();
                throw new InvalidLevelCrossingStateOnBCException(cause.getStateStr());
            } else {
                throw new IllegalStateException(e.getCause());
            }
        }
    }

    String getKey() {
        return this.levelCrossing.getAssetId() + LEVEL_CROSSING_KEY_POSTFIX;
    }
}
