package shared;

import shared.assetdata.TrainConfig.TRAIN_SPEED;

import java.util.List;
import java.util.regex.Pattern;

/*
 * This class contains chaincode configuration data shared by both the clients and the chaincode
 */
public class GlobalConfig {
    // Events
    public static final String COMPOSITE_EVENT = "compositeEvent";
    public static final String EMERGENCY_STOP_EVENT = "emergencyStopEvent";
    public static final String KILL_EVENT = "killEvent";
    public static final String LEVEL_CROSSING_EVENT = "levelCrossingEvent";
    public static final String SENSOR_TRIGGERED_EVENT = "sensorTriggeredEvent";
    public static final String SIGNAL_EVENT = "signalEvent";
    public static final String SWITCH_EVENT = "switchEvent";
    public static final String TRAIN_CONTROL_EVENT = "trainControlCommandEvent";

    // Invocable functions
    public enum CC_FUNCTION {
        EMERGENCY_STOP("emergencyStop", 0, false, false),
        KILL_ALL_CLIENTS("killAllClients", 0, false, false),
        PING("ping", 0, false, false),
        QUERY_HISTORY("queryHistory", 1, false, false),
        QUERY_LEVEL_CROSSING_STATE("queryLevelCrossingState", 1, false, false),
        QUERY_SIGNAL_ASPECT("querySignalAspect", 1, false, false),
        QUERY_SECTION_RESERVEE("querySectionReservee", 1, false, false),
        QUERY_SECTION_QUEUE("querySectionQueue", 1, false, false),
        QUERY_SECTION_WEIGHT("querySectionWeight", 1, false, false),
        QUERY_SWITCH_STATE("querySwitchState", 1, false, false),
        QUERY_TEST_VAR("queryTestVar", 1, false, false),
        QUERY_TRACK_LAYOUT("queryTrackLayout", 0, true, false),
        QUERY_TRAIN_DESTINATION("queryTrainDestination", 1, false, false),
        QUERY_TRAIN_DIRECTION("queryTrainDirection", 1, false, false),
        QUERY_TRAIN_INITIALISED("queryTrainInitialised", 1, false, false),
        QUERY_TRAIN_POSITION("queryTrainPosition", 1, false, false),
        QUERY_TRAIN_SPEED("queryTrainSpeed", 1, false, false),
        SENSOR_TRIGGER("sensorTrigger", 1, true, true),
        SET_LEVEL_CROSSING_STATE("setLevelCrossingState", 2, false, false),
        SET_SECTION_WEIGHT("setSectionWeight", 2, true, true),
        SET_SIGNAL_ASPECT("setSignalAspect", 2, false, false),
        SET_SWITCH_STATE("setSwitchState", 2, false, false),
        SET_TEST_VAR("setTestVar", 2, false, false),
        SET_TRAIN_DESTINATION("setTrainDestination", 2, false, true),
        SET_TRAIN_DIRECTION("setTrainDirection", 2, false, false),
        SET_TRAIN_POSITION("setTrainPosition", 3, false, true),
        SET_TRAIN_SPEED("setTrainSpeed", 2, false, false),
        TRAIN_INIT("initTrain", 1, false, false),
        TRAIN_INIT_CONFIRM("trainInitConfirm", 1, true, true),
        UPDATE_LEVEL_CROSSING_STATE("levelCrossingStateUpdate", 2, true, true),
        UPDATE_SIGNAL_ASPECT("signalAspectUpdate", 2, false, false),
        UPDATE_SWITCH_STATE("switchStateUpdate", 2, true, true),
        UPDATE_TRAIN_DIRECTION("trainDirectionUpdate", 2, true, true),
        UPDATE_TRAIN_SPEED("trainSpeedUpdate", 2, false, false);

        private final String name;
        private final int nArgs;
        private final boolean mustCalculateTrainRoutes;
        private final boolean mustUpdateAssets;

        CC_FUNCTION(String name, int nArgs, boolean mustCalculateTrainRoutes, boolean mustUpdateAssets) {
            this.name = name;
            this.nArgs = nArgs;
            this.mustCalculateTrainRoutes = mustCalculateTrainRoutes;
            this.mustUpdateAssets = mustUpdateAssets;
        }

        // Given the assetId retrieved from the invoker's certificate and the list of provided parameters,
        // returns whether or not this invocation is allowed
        public boolean mayBeInvoked(String assetId, List<String> params) {
            // This method should only be called after checking the number of parameters
            assert (params.size() == this.nArgs);
            if (assetId.startsWith(SCC_AID_PREFIX) || assetId.startsWith(EXECUTER_AID_PREFIX) || this == QUERY_TEST_VAR || this == SET_TEST_VAR || this == QUERY_HISTORY) {
                // Either an SCC called this function, or the function is a testing function that may be called by anyone
                return true;
            } else if (this == EMERGENCY_STOP || this == PING || this == KILL_ALL_CLIENTS) {
                // These functions are special cases because they do not take any arguments, so we handle them here.
                // They may all only be invoked by SCCs, so return false here since we already checked for that
                return false;
            }
            // For all other events, the first parameter (indicating the subject of the invocation) must be equal to their username
            String subjectAId = params.get(0);
            return subjectAId.equals(assetId);
        }

        public String getName() {
            return this.name;
        }

        public int getNArgs() {
            return this.nArgs;
        }

        public boolean mustCalculateTrainRoutes() {
            return this.mustCalculateTrainRoutes;
        }

        public boolean mustUpdateAssets() {
            return this.mustUpdateAssets;
        }

        public String dumpInvocation(List<String> args) {
            return this.dumpInvocation(args.toArray(new String[0]));
        }

        public String dumpInvocation(String... args) {
            StringBuilder result = new StringBuilder("invokeCC(" + this.name + ", ");
            for (int i = 0; i < args.length; i++) {
                result.append(args[i]);
                if (i != args.length - 1) {
                    result.append(", ");
                }
            }
            result.append(")");
            return result.toString();
        }

        public static CC_FUNCTION fromString(String name) {
            for (CC_FUNCTION function : CC_FUNCTION.values()) {
                if (function.getName().equals(name)) {
                    return function;
                }
            }
            return null;
        }
    }

    // How many times should clients reattempt an invocation after failing?
    public static final int N_INVOCATION_RETRIES = 25;
    // How many seconds should we wait for a transaction to be confirmed by the ordering service?
    public static final int INVOCATION_TIMEOUT_S = 600;

    // Do we want to log a warning every time TrackLayout.readFromBlockchain misses a value from the blockchain?
    public static final boolean WARN_MISSING_BC_DATA = false;
    public static final boolean LOG_CC_CHANGES = true;
    public static final boolean IGNORE_EMERGENCY_STOPS = true;
    // Do we want clients to write their output to files?
    public static final boolean CLIENT_LOG_TO_FILE = true;
    // Do we want all assets to be managed automatically?

    // The maximum allowed time between a command being set and a client confirming its reception
    public static final int STATE_CONFIRMATION_TIMEOUT = 20000;

    // Number of sections trains calculate ahead
    public static final int TRAIN_N_FUTURE_SECTIONS_PREDICTED = 3;
    // Number of passed sections trains keep track of
    public static final int TRAIN_N_PASSED_SECTIONS_TRACKED = 2;
    // Number of sections calculated ahead kept clear, MUST be smaller than or equal to TRAIN_N_FUTURE_SECTIONS_PREDICTED
    public static final int TRAIN_N_FUTURE_SECTIONS_RESERVED = 2;
    // Number of passed sections kept clear behind a train, MUST be smaller than or equal to TRAIN_N_PASSED_SECTION_TRACKED
    public static final int TRAIN_N_PASSED_SECTIONS_RESERVED = 1;

    public static final boolean RANDOM_TRAIN_ROUTES = false;

    // The weight given to sections that are closed.
    public static final int SECTION_CLOSED_WEIGHT = 1000;

    public static final double SECTION_DEFAULT_VMAX_DOUBLE = 0.2;
    public static final TRAIN_SPEED SECTION_DEFAULT_VMAX = TRAIN_SPEED.getDefaultSpeed();

    // Type-defining asset ID prefixes
    public static final String EXECUTER_AID_PREFIX = "Ex";
    public static final String TRAIN_AID_PREFIX = "Tr";
    public static final String SECTION_AID_PREFIX = "Sc";
    public static final String SENSOR_AID_PREFIX = "Sn";
    public static final String SCC_AID_PREFIX = "Sm";
    public static final String SIGNAL_AID_PREFIX = "Sg";
    public static final String LV_AID_PREFIX = "Lv";
    public static final String SWITCH_AID_PREFIX = "Sw";

    public static final String COMPOSITE_STRING_SEPARATOR = "|";

    // Type-defining state key prefixes
    public static final String LAST_INVOKE_EXECUTED_KEY = "lastInvokeExecuted";
    public static final String INVOCATION_REGISTRATION_KEY = "registerInvocation";

    public static final String LEVEL_CROSSING_KEY_POSTFIX = "Is";
    public static final String TRAIN_DESTINATION_KEY_POSTFIX = "GoesTo";
    public static final String TRAIN_INITIALISED_KEY_POSTFIX = "IsInitialised";
    public static final String TRAIN_POSITION_KEY_POSTFIX = "IsAt";
    public static final String TRAIN_SPEED_KEY_POSTFIX = "HasSpeed";
    public static final String TRAIN_DIRECTION_KEY_POSTFIX = "IsGoing";
    public static final String SECTION_RESERVED_FOR_KEY_POSTFIX = "IsFor";
    public static final String SECTION_QUEUE_KEY_POSTFIX = "HasQueue";
    public static final String SECTION_WEIGHT_KEY_POSTFIX = "HasWeight";
    public static final String SIGNAL_ASPECT_KEY_POSTFIX = "HasAspect";
    public static final String SWITCH_STATE_KEY_POSTFIX = "IsSwitched";
    public static final String ASSET_INTENDED_STATE_POSTFIX = "(intended)";
    public static final String EMPTY_KEY_STATE = "None";

    public static final String TRACK_LAYOUT_JSON = "{\n" +
            "  \"sections\":\n" +
            "  [\n" +
            "    {\n" +
            "      \"id\": 0,\n" +
            "      \"side0\": [8],\n" +
            "      \"side1\": [1],\n" +
            "      \"weight\": 1,\n" +
            "      \"delimiters\": [0,1]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 1,\n" +
            "      \"side0\": [0],\n" +
            "      \"side1\": [2],\n" +
            "      \"weight\": 1,\n" +
            "      \"delimiters\": [1,2]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 2,\n" +
            "      \"side0\": [1],\n" +
            "      \"side1\": [3, 14],\n" +
            "      \"weight\": 1,\n" +
            "      \"delimiters\": [2,3,17]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 3,\n" +
            "      \"side0\": [2],\n" +
            "      \"side1\": [4],\n" +
            "      \"weight\": 1,\n" +
            "      \"delimiters\": [3,4]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 4,\n" +
            "      \"side0\": [3, 13],\n" +
            "      \"side1\": [5],\n" +
            "      \"weight\": 1,\n" +
            "      \"delimiters\": [4,5,16]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 5,\n" +
            "      \"side0\": [4],\n" +
            "      \"side1\": [6],\n" +
            "      \"weight\": 1,\n" +
            "      \"delimiters\": [5,6]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 6,\n" +
            "      \"side0\": [5],\n" +
            "      \"side1\": [7,9],\n" +
            "      \"weight\": 1,\n" +
            "      \"delimiters\": [6,7,9]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 7,\n" +
            "      \"side0\": [6],\n" +
            "      \"side1\": [8],\n" +
            "      \"weight\": 1,\n" +
            "      \"delimiters\": [7,8]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 8,\n" +
            "      \"side0\": [7,12],\n" +
            "      \"side1\": [0],\n" +
            "      \"weight\": 1,\n" +
            "      \"delimiters\": [0,8,13]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 9,\n" +
            "      \"side0\": [6],\n" +
            "      \"side1\": [10],\n" +
            "      \"weight\": 1,\n" +
            "      \"delimiters\": [9,10]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 10,\n" +
            "      \"side0\": [9],\n" +
            "      \"side1\": [11,14],\n" +
            "      \"weight\": 1,\n" +
            "      \"delimiters\": [10,11,14]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 11,\n" +
            "      \"side0\": [10,13],\n" +
            "      \"side1\": [12],\n" +
            "      \"weight\": 1,\n" +
            "      \"delimiters\": [11,12,15]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 12,\n" +
            "      \"side0\": [11],\n" +
            "      \"side1\": [8],\n" +
            "      \"weight\": 1,\n" +
            "      \"delimiters\": [12,13]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 13,\n" +
            "      \"side0\": [11],\n" +
            "      \"side1\": [4],\n" +
            "      \"weight\": 1,\n" +
            "      \"delimiters\": [15,16]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 14,\n" +
            "      \"side0\": [2],\n" +
            "      \"side1\": [10],\n" +
            "      \"weight\": 1,\n" +
            "      \"delimiters\": [14,17]\n" +
            "    }\n" +
            "  ],\n" +
            "  \"section_groups\":\n" +
            "  [\n" +
            "    [13, 14]\n" +
            "  ],\n" +
            "  \"sensors\":\n" +
            "  [\n" +
            "    {\n" +
            "      \"id\": 0,\n" +
            "      \"sides\": [8,0]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 1,\n" +
            "      \"sides\": [0,1]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 2,\n" +
            "      \"sides\": [1,2]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 3,\n" +
            "      \"sides\": [2,3]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 4,\n" +
            "      \"sides\": [3,4]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 5,\n" +
            "      \"sides\": [4,5]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 6,\n" +
            "      \"sides\": [5,6]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 7,\n" +
            "      \"sides\": [6,7]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 8,\n" +
            "      \"sides\": [7,8]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 9,\n" +
            "      \"sides\": [6,9]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 10,\n" +
            "      \"sides\": [9,10]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 11,\n" +
            "      \"sides\": [10,11]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 12,\n" +
            "      \"sides\": [11,12]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 13,\n" +
            "      \"sides\": [12,8]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 14,\n" +
            "      \"sides\": [10,14]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 15,\n" +
            "      \"sides\": [11,13]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 16,\n" +
            "      \"sides\": [4,13]\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 17,\n" +
            "      \"sides\": [2,14]\n" +
            "    }\n" +
            "  ],\n" +
            "  \"switches\":\n" +
            "  [\n" +
            "    {\n" +
            "      \"id\": 0,\n" +
            "      \"container\": 2,\n" +
            "      \"state\": false\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 1,\n" +
            "      \"container\": 4,\n" +
            "      \"state\": false\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 2,\n" +
            "      \"container\": 6,\n" +
            "      \"state\": false\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 3,\n" +
            "      \"container\": 8,\n" +
            "      \"state\": false\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 4,\n" +
            "      \"container\": 10,\n" +
            "      \"state\": false\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 5,\n" +
            "      \"container\": 11,\n" +
            "      \"state\": false\n" +
            "    }\n" +
            "  ],\n" +
            "  \"levelcrossings\":\n" +
            "  [\n" +
            "    {\n" +
            "      \"id\": 0,\n" +
            "      \"section\": 1,\n" +
            "      \"state\": false\n" +
            "    }\n" +
            "  ],\n" +
            "  \"signals\":\n" +
            "  [\n" +
            "    {\n" +
            "      \"id\": 0,\n" +
            "      \"leaving\": 0,\n" +
            "      \"entering\": [8],\n" +
            "      \"attached\": 0,\n" +
            "      \"aspect\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 1,\n" +
            "      \"leaving\": 1,\n" +
            "      \"entering\": [2],\n" +
            "      \"attached\": 2,\n" +
            "      \"aspect\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 2,\n" +
            "      \"leaving\": 3,\n" +
            "      \"entering\": [2],\n" +
            "      \"attached\": 3,\n" +
            "      \"aspect\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 3,\n" +
            "      \"leaving\": 3,\n" +
            "      \"entering\": [4],\n" +
            "      \"attached\": 4,\n" +
            "      \"aspect\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 4,\n" +
            "      \"leaving\": 5,\n" +
            "      \"entering\": [4],\n" +
            "      \"attached\": 5,\n" +
            "      \"aspect\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 5,\n" +
            "      \"leaving\": 5,\n" +
            "      \"entering\": [6],\n" +
            "      \"attached\": 6,\n" +
            "      \"aspect\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 6,\n" +
            "      \"leaving\": 7,\n" +
            "      \"entering\": [6],\n" +
            "      \"attached\": 7,\n" +
            "      \"aspect\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 7,\n" +
            "      \"leaving\": 7,\n" +
            "      \"entering\": [8],\n" +
            "      \"attached\": 8,\n" +
            "      \"aspect\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 8,\n" +
            "      \"leaving\": 9,\n" +
            "      \"entering\": [6],\n" +
            "      \"attached\": 9,\n" +
            "      \"aspect\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 9,\n" +
            "      \"leaving\": 9,\n" +
            "      \"entering\": [10, 11],\n" +
            "      \"attached\": 10,\n" +
            "      \"aspect\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 10,\n" +
            "      \"leaving\": 12,\n" +
            "      \"entering\": [10, 11],\n" +
            "      \"attached\": 12,\n" +
            "      \"aspect\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 11,\n" +
            "      \"leaving\": 12,\n" +
            "      \"entering\": [8],\n" +
            "      \"attached\": 13,\n" +
            "      \"aspect\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 12,\n" +
            "      \"leaving\": 14,\n" +
            "      \"entering\": [2],\n" +
            "      \"attached\": 17,\n" +
            "      \"aspect\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 13,\n" +
            "      \"leaving\": 13,\n" +
            "      \"entering\": [4],\n" +
            "      \"attached\": 16,\n" +
            "      \"aspect\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 14,\n" +
            "      \"leaving\": 14,\n" +
            "      \"entering\": [10],\n" +
            "      \"attached\": 14,\n" +
            "      \"aspect\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 15,\n" +
            "      \"leaving\": 13,\n" +
            "      \"entering\": [11],\n" +
            "      \"attached\": 15,\n" +
            "      \"aspect\": 1\n" +
            "    }\n" +
            "  ],\n" +
            "  \"trains\": [\n" +
            "    {\n" +
            "      \"id\": 0,\n" +
            "      \"last_section\": 8,\n" +
            "      \"current_section\": 0,\n" +
            "      \"destination_section\": 1,\n" +
            "      \"direction\": true,\n" +
            "      \"speed\": 0.0\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 1,\n" +
            "      \"last_section\": 4,\n" +
            "      \"current_section\": 5,\n" +
            "      \"destination_section\": 9,\n" +
            "      \"direction\": true,\n" +
            "      \"speed\": 0.0\n" +
            "    }\n" +
            "  ]\n" +
            "}";
    // Track layout used for unit tests
    public static final String TEST_TRACK_LAYOUT_JSON = TRACK_LAYOUT_JSON;

    // Functions used to generate/parse composite strings. Used in, for example, event names or chaincode states.
    public static String[] parseCompositeString(String state) {
        return state.split(Pattern.quote(COMPOSITE_STRING_SEPARATOR));
    }

    public static String createCompositeString(String... args) {
        return String.join(COMPOSITE_STRING_SEPARATOR, args);
    }

    public static boolean stringIsBoolean(String s) {
        return s.equals("true") || s.equals("false");
    }
}
