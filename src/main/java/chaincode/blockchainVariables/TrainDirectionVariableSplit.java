package chaincode.blockchainVariables;

import org.hyperledger.fabric.shim.ChaincodeStub;
import shared.assetdata.TrainConfig.TRAIN_DIRECTION;
import shared.errors.InvalidTrainDirectionException;
import trackTopology.Train;
import trackTopology.errors.blockchain.unchecked.invaliddata.InvalidTrainDirectionOnBCException;

import static shared.GlobalConfig.ASSET_INTENDED_STATE_POSTFIX;
import static shared.GlobalConfig.TRAIN_DIRECTION_KEY_POSTFIX;

public class TrainDirectionVariableSplit extends BlockchainVariable<TRAIN_DIRECTION> {
    private Train train;
    private boolean isIntended;

    public TrainDirectionVariableSplit(Train train) {
        this(train, false);
    }

    public TrainDirectionVariableSplit(Train train, boolean isIntended) {
        super();
        this.train = train;
        this.isIntended = isIntended;
    }

    public TrainDirectionVariableSplit(TrainDirectionVariableSplit old) {
        super(old);
        this.train = old.train;
    }

    TRAIN_DIRECTION fromBcString(ChaincodeStub chaincodeStub, String bcString) {
        try {
            return TRAIN_DIRECTION.fromString(bcString);
        } catch (InvalidTrainDirectionException e) {
            throw new InvalidTrainDirectionOnBCException(bcString);
        }
    }

    String toBcString(TRAIN_DIRECTION direction) {
        return direction.toString();
    }

    String getKey() {
        return this.train.getAssetId() + TRAIN_DIRECTION_KEY_POSTFIX + (this.isIntended ? ASSET_INTENDED_STATE_POSTFIX : "");
    }
}
