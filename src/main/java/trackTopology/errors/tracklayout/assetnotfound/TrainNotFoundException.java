package trackTopology.errors.tracklayout.assetnotfound;

public class TrainNotFoundException extends AssetNotFoundException {
    public TrainNotFoundException(String aId) {
        super("Train not found: " + aId);
    }
}
