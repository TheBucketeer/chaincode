package trackTopology.errors.runtime.unchecked;

import trackTopology.Section;
import trackTopology.Train;

public class InvalidAdditionToWaitingListException extends TrainManagementRuntimeException {
    public InvalidAdditionToWaitingListException(Train train, Section section) {
        super("Attempt was made to add " + section.getAssetId() + " to waiting list of " + train.getAssetId() + ", but was already present.");
    }
}
