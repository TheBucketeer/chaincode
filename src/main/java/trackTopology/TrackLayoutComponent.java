package trackTopology;

public class TrackLayoutComponent {
    String aId;

    public String getAssetId() {
        return this.aId;
    }

    String format(String s) {
        return "[" + this.aId + "]: " + s;
    }

    public boolean equals(Object other) {
        return other != null && other.getClass() == this.getClass() && this.aId.equals(((TrackLayoutComponent) other).aId);
    }

    public String toString() {
        return this.getAssetId();
    }
}
