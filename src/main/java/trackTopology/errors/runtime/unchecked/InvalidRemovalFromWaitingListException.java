package trackTopology.errors.runtime.unchecked;

import trackTopology.Section;
import trackTopology.Train;

public class InvalidRemovalFromWaitingListException extends TrainManagementRuntimeException {
    public InvalidRemovalFromWaitingListException(Train train, Section section) {
        super("Attempt was made to remove " + section.getAssetId() + " from waiting list of " + train.getAssetId() + ", but wasn't present.");
    }
}
