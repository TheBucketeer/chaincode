package shared.errors;

public class InvalidDataException extends Exception {
    private final String stateStr;

    InvalidDataException(String dataType, String stateStr) {
        super("Invalid " + dataType + ": " + stateStr);
        this.stateStr = stateStr;
    }

    public String getStateStr() {
        return this.stateStr;
    }
}
