package shared.errors;

public class InvalidTrainSpeedException extends InvalidDataException {
    public InvalidTrainSpeedException(String speedStr) {
        super("train speed", speedStr);
    }

    public InvalidTrainSpeedException(double speed) {
        this(String.valueOf(speed));
    }
}
