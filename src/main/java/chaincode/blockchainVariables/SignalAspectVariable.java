package chaincode.blockchainVariables;

import chaincode.DualVariable;
import org.hyperledger.fabric.shim.ChaincodeStub;
import shared.assetdata.SignalConfig.SIGNAL_ASPECT;
import shared.errors.InvalidSignalAspectException;
import trackTopology.Signal;
import trackTopology.errors.blockchain.checked.InvalidDualVariableDataOnBCException;
import trackTopology.errors.blockchain.unchecked.invaliddata.InvalidSignalAspectOnBCException;

import java.lang.reflect.InvocationTargetException;

import static shared.GlobalConfig.SIGNAL_ASPECT_KEY_POSTFIX;


public class SignalAspectVariable extends DualBlockchainVariable<SIGNAL_ASPECT> {
    private Signal signal;

    public SignalAspectVariable(Signal signal) {
        super();
        this.signal = signal;
    }

    public SignalAspectVariable(SignalAspectVariable signalAspectStateVariable) {
        super(signalAspectStateVariable);
        this.signal = signalAspectStateVariable.signal;
    }

    DualVariable<SIGNAL_ASPECT> fromBcString(ChaincodeStub chaincodeStub, String bcStringCurrent, String bcStringIntended) {
        try {
            return getDualVariableFromBcString(SIGNAL_ASPECT.class, bcStringCurrent, bcStringIntended);
        } catch (InvalidDualVariableDataOnBCException e) {
            throw new InvalidSignalAspectOnBCException(e.getCurrent(), e.getIntended());
        } catch (InvocationTargetException e) {
            if (e.getCause() instanceof InvalidSignalAspectException) {
                InvalidSignalAspectException cause = (InvalidSignalAspectException) e.getCause();
                throw new InvalidSignalAspectOnBCException(cause.getStateStr());
            } else {
                throw new IllegalStateException(e.getCause());
            }
        }
    }

    String getKey() {
        return this.signal.getAssetId() + SIGNAL_ASPECT_KEY_POSTFIX;
    }
}
