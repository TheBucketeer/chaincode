package trackTopology.errors.runtime.checked;

import trackTopology.Sensor;
import trackTopology.errors.tracklayout.TrackLayoutException;

public class MagnetMyrtleException extends TrackLayoutException {
    public MagnetMyrtleException(Sensor s) {
        super("NO train should have triggered " + s.getAssetId() + ", and yet it happened!");
    }
}
