package trackTopology;

import chaincode.blockchainVariables.LevelCrossingStateVariableSplit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.shim.ChaincodeStub;

import static chaincode.EventHelper.queueLCCommand;
import static shared.GlobalConfig.LV_AID_PREFIX;
import static shared.assetdata.LevelCrossingConfig.LC_STATE;
import static shared.assetdata.LevelCrossingConfig.LC_STATE.CLOSED_FOR_TRAINS;
import static shared.assetdata.LevelCrossingConfig.LC_STATE.OPEN_FOR_TRAINS;

public class LevelCrossing extends TrackLayoutComponent {
    private static final Log LOG = LogFactory.getLog(LevelCrossing.class);
    private Section container;
    private LevelCrossingStateVariableSplit currentBoomsState = new LevelCrossingStateVariableSplit(this);
    private LevelCrossingStateVariableSplit intendedBoomsState = new LevelCrossingStateVariableSplit(this, true);

    LevelCrossing(int lcId) {
        this.aId = String.format("%s%03d", LV_AID_PREFIX, lcId);
    }

    LevelCrossing(LevelCrossing old) {
        this.aId = old.aId;
        this.container = old.container;
        this.currentBoomsState = new LevelCrossingStateVariableSplit(old.currentBoomsState);
        this.intendedBoomsState = new LevelCrossingStateVariableSplit(old.intendedBoomsState);
    }

    boolean isOpenForTrains(ChaincodeStub chaincodeStub) {
        return this.getCurrentBoomsState(chaincodeStub) == OPEN_FOR_TRAINS;
    }

    void openForTrains(ChaincodeStub chaincodeStub) {
        this.initBoomsStateChange(chaincodeStub, OPEN_FOR_TRAINS);
    }

    void closeForTrains(ChaincodeStub chaincodeStub) {
        this.initBoomsStateChange(chaincodeStub, CLOSED_FOR_TRAINS);
    }

    public void initBoomsStateChange(ChaincodeStub chaincodeStub, LC_STATE state, boolean forceResend) {
        assert (state != null);
        if (this.getIntendedBoomsState(chaincodeStub) != state || forceResend) {
            this.setIntendedBoomsState(chaincodeStub, state);
            queueLCCommand(chaincodeStub, this, state);
        }
    }

    private void initBoomsStateChange(ChaincodeStub chaincodeStub, LC_STATE state) {
        this.initBoomsStateChange(chaincodeStub, state, false);
    }

    public void updateBoomsState(ChaincodeStub chaincodeStub, LC_STATE state) {
        this.setCurrentBoomsState(chaincodeStub, state);
        LOG.info(this.format("Changed to " + this.getCurrentBoomsState(chaincodeStub).getName()));
    }

    LevelCrossingStateVariableSplit getCurrentBoomsStateBcVariable() {
        return this.currentBoomsState;
    }

    LevelCrossingStateVariableSplit getIntendedBoomsStateBcVariable() {
        return this.intendedBoomsState;
    }

    public LC_STATE getCurrentBoomsState(ChaincodeStub chaincodeStub) {
        return this.currentBoomsState.getValue(chaincodeStub);
    }

    void setCurrentBoomsState(ChaincodeStub chaincodeStub, LC_STATE boomsState) {
        this.currentBoomsState.setValue(chaincodeStub, boomsState);
    }

    private LC_STATE getIntendedBoomsState(ChaincodeStub chaincodeStub) {
        return this.intendedBoomsState.getValue(chaincodeStub);
    }

    void setIntendedBoomsState(ChaincodeStub chaincodeStub, LC_STATE boomsState) {
        this.intendedBoomsState.setValue(chaincodeStub, boomsState);
    }

    Section getContainer() {
        return this.container;
    }

    void setContainer(Section container) {
        this.container = container;
    }

    public String toString(ChaincodeStub chaincodeStub) {
        LC_STATE current = this.getCurrentBoomsState(chaincodeStub);
        LC_STATE intended = this.getIntendedBoomsState(chaincodeStub);
        return this.aId +
                ", in " + (this.container == null ? "???" : this.container.getAssetId()) +
                ", current state " + (current == null ? "???" : current.getName()) +
                ", intended state " + (intended == null ? "???" : intended.getName());
    }
}
