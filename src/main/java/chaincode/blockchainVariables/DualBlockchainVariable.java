package chaincode.blockchainVariables;

import chaincode.DualVariable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.shim.ChaincodeStub;
import trackTopology.errors.blockchain.checked.InvalidDualVariableDataOnBCException;
import trackTopology.errors.blockchain.checked.datanotfound.BlockchainDataNotFoundException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static shared.GlobalConfig.ASSET_INTENDED_STATE_POSTFIX;
import static shared.GlobalConfig.parseCompositeString;

public abstract class DualBlockchainVariable<T> extends BlockchainVariable<DualVariable<T>> {
    private static final Log LOG = LogFactory.getLog(DualBlockchainVariable.class);

    // Store the current value as well as the most recently read value from the blockchain
    abstract DualVariable<T> fromBcString(ChaincodeStub chaincodeStub, String bcStringCurrent, String bcStringIntended);

    DualBlockchainVariable() {
        this.value = new DualVariable<>();
        this.valueOnBc = new DualVariable<>();
    }

    DualBlockchainVariable(DualBlockchainVariable<T> old) {
        this.value = new DualVariable<>(old.value);
        this.valueOnBc = new DualVariable<>(old.valueOnBc);
    }

    DualVariable<T> fromBcString(ChaincodeStub chaincodeStub, String bcString) {
        assert (false);
        return null;
    }

    DualVariable<T> getValueFromBc(ChaincodeStub chaincodeStub) throws BlockchainDataNotFoundException {
        return this.fromBcString(chaincodeStub, getStringStateWrapper(chaincodeStub, this.getKey()), getStringStateWrapper(chaincodeStub, this.getKey() + ASSET_INTENDED_STATE_POSTFIX));
    }

    String toBcString(DualVariable<T> value) {
        return null;
    }

    public T getCurrentState(ChaincodeStub chaincodeStub) {
        return this.getValue(chaincodeStub).getCurrentState();
    }

    public T getIntendedState(ChaincodeStub chaincodeStub) {
        return this.getValue(chaincodeStub).getIntendedState();
    }

    public void setCurrentState(ChaincodeStub chaincodeStub, T currentState) {
        this.getValue(chaincodeStub).setCurrentState(currentState);
    }

    public void setIntendedState(ChaincodeStub chaincodeStub, T intendedState) {
        this.getValue(chaincodeStub).setIntendedState(chaincodeStub, intendedState);
    }

    void updateToBC(ChaincodeStub chaincodeStub) {
        assert (this.value != null);
        putStringStateWrapper(chaincodeStub, this.getKey(), this.value.getCurrentStateBcValue());
        putStringStateWrapper(chaincodeStub, this.getKey() + ASSET_INTENDED_STATE_POSTFIX, this.value.getIntendedStateBcValue());
    }

    DualVariable<T> copyValue(DualVariable<T> value) {
        return new DualVariable<>(value);
    }

    DualVariable<T> getDualVariableFromBcString(Class<T> dataType, String bcStringCurrent, String bcStringIntendedRaw) throws InvalidDualVariableDataOnBCException, InvocationTargetException {
        String[] intendedStateStrSplit = parseCompositeString(bcStringIntendedRaw);
        if (intendedStateStrSplit.length != 2) {
            throw new InvalidDualVariableDataOnBCException(bcStringCurrent, bcStringIntendedRaw);
        }
        String bcStringIntended = intendedStateStrSplit[0];
        try {
            DualVariable<T> dualVariable = new DualVariable<>();
            long timestamp = Long.parseLong(intendedStateStrSplit[1]);
            Method method = dataType.getMethod("fromString", String.class);
            // Ugly cast, but I had to choose between this or 100 lines of duplicate code
            T currentState = (T) method.invoke(null, bcStringCurrent);
            T intendedState = (T) method.invoke(null, bcStringIntended);
            dualVariable.setCurrentState(currentState);
            dualVariable.setIntendedState(intendedState, timestamp);
            return dualVariable;
        } catch (NumberFormatException e) {
            throw new InvalidDualVariableDataOnBCException(bcStringCurrent, bcStringIntended);
        } catch (NoSuchMethodException | IllegalAccessException e) {
            // Should never happen, so make it unchecked
            throw new IllegalStateException(e);
        }
    }

    boolean hasEmptyValue() {
        return this.value.getCurrentState() == null && this.value.getIntendedState() == null;
    }

    boolean hasEmptyValueOnBc() {
        return this.valueOnBc.getCurrentState() == null && this.valueOnBc.getIntendedState() == null;
    }
}