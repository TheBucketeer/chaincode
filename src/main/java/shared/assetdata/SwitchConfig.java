package shared.assetdata;

import shared.errors.InvalidSwitchStateException;

import static shared.GlobalConfig.stringIsBoolean;

/*
 * Contains general switch configuration used by both the clients and chaincode
 */
public class SwitchConfig {
    public enum SWITCH_STATE {
        INWARDS(true, "inwards"),
        OUTWARDS(false, "outwards");

        private final boolean stateAsBool;
        private final String description;

        SWITCH_STATE(boolean boolState, String description) {
            this.stateAsBool = boolState;
            this.description = description;
        }

        public static SWITCH_STATE fromBool(boolean stateBool) throws InvalidSwitchStateException {
            for (SWITCH_STATE state : SWITCH_STATE.values()) {
                if (state.toBool() == stateBool) {
                    return state;
                }
            }
            throw new InvalidSwitchStateException(stateBool);
        }

        public static SWITCH_STATE fromString(String stateStr) throws InvalidSwitchStateException {
            if (stringIsBoolean(stateStr)) {
                return fromBool(Boolean.parseBoolean(stateStr));
            }
            throw new InvalidSwitchStateException(stateStr);
        }

        public SWITCH_STATE getOpposite() {
            try {
                return fromBool(!this.stateAsBool);
            } catch (InvalidSwitchStateException e) {
                System.out.println("NOOOO, this can't happen, behave!!@!!!!! " + e.getMessage());
                return null;
            }
        }

        public boolean toBool() {
            return this.stateAsBool;
        }

        public String getName() {
            return this.description;
        }

        public boolean equals(SWITCH_STATE other) {
            return this.toBool() == other.toBool();
        }

        public String toString() {
            return String.valueOf(this.stateAsBool);
        }
    }
}
