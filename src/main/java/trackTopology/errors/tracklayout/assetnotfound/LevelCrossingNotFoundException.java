package trackTopology.errors.tracklayout.assetnotfound;

public class LevelCrossingNotFoundException extends AssetNotFoundException {
    public LevelCrossingNotFoundException(String aId) {
        super("Level crossing not found: " + aId);
    }
}
