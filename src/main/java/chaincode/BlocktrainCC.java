package chaincode;

//import chaincode.blockchainVariables.BlockchainVariable;

import com.google.protobuf.ByteString;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math3.util.Pair;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.openssl.PEMParser;
import org.hyperledger.fabric.protos.msp.Identities;
import org.hyperledger.fabric.shim.ChaincodeBase;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.hyperledger.fabric.shim.ledger.CompositeKey;
import org.hyperledger.fabric.shim.ledger.KeyModification;
import org.hyperledger.fabric.shim.ledger.KeyValue;
import org.hyperledger.fabric.shim.ledger.QueryResultsIterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import shared.assetdata.LevelCrossingConfig.LC_STATE;
import shared.assetdata.SectionConfig.SECTION_WEIGHT;
import shared.assetdata.SwitchConfig.SWITCH_STATE;
import shared.errors.*;
import trackTopology.*;
import trackTopology.errors.blockchain.checked.datanotfound.BlockchainDataNotFoundException;
import trackTopology.errors.blockchain.checked.datanotfound.TrainPositionNotFoundException;
import trackTopology.errors.runtime.checked.MagnetMyrtleException;
import trackTopology.errors.runtime.checked.SensorReadingConflictException;
import trackTopology.errors.runtime.checked.TrainManagementException;
import trackTopology.errors.tracklayout.TrackLayoutException;
import trackTopology.errors.tracklayout.assetnotfound.*;

import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

import static chaincode.CCHelper.*;
import static chaincode.EventHelper.queueEmergencyStopEvent;
import static chaincode.EventHelper.queueSensorTriggerEvent;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.bouncycastle.asn1.x500.style.BCStyle.CN;
import static shared.GlobalConfig.*;
import static shared.GlobalConfig.CC_FUNCTION.*;
import static shared.assetdata.SignalConfig.SIGNAL_ASPECT;
import static shared.assetdata.TrainConfig.TRAIN_DIRECTION;
import static shared.assetdata.TrainConfig.TRAIN_SPEED;

public class BlocktrainCC extends ChaincodeBase {
    static final Log LOG = LogFactory.getLog(BlocktrainCC.class);
    private final HashMap<ChaincodeStub, ConcurrentLinkedQueue<Pair<String, JSONObject>>> eventQueues = new HashMap<>();
    public static HashMap<ChaincodeStub, TrackLayout> trackLayouts = new HashMap<>();
    public static HashMap<ChaincodeStub, Long> timestamps = new HashMap<>();
    private static HashMap<ChaincodeStub, String> invokingAIds = new HashMap<>();
    private static HashMap<ChaincodeStub, CC_FUNCTION> invokedFunctions = new HashMap<>();
    public static HashMap<ChaincodeStub, Boolean> ignoreBc = new HashMap<>();
    public static HashMap<ChaincodeStub, Boolean> forceWriteToBc = new HashMap<>();

    // Error message strings
    static final String INCORRECT_N_ARGS_MSG = "Incorrect number of arguments!";
    private static final String INVOCATION_PROHIBITED_MSG = "Invocation of this function with these parameters not allowed by this client.";
    static final String UNSUPPORTED_FUNCTION_MSG = " function is currently not supported!";

    private void setup(ChaincodeStub chaincodeStub) {
        if (chaincodeStub.getTxTimestamp() == null) {
            timestamps.put(chaincodeStub, (long) 0);
            logWarn(chaincodeStub, chaincodeStub.getTxTimestamp() + " == null?!");
        } else {
            timestamps.put(chaincodeStub, chaincodeStub.getTxTimestamp().toEpochMilli());
        }
        EventHelper.setParent(this);
        this.eventQueues.put(chaincodeStub, new ConcurrentLinkedQueue<>());
        String invokingAId = "";
        try {
            // Parse certificate, retrieve common name (CN) and retrieve the asset id of the invoking client
            ByteString idBytes = Identities.SerializedIdentity.parseFrom(chaincodeStub.getCreator()).getIdBytes();
            logInfo(chaincodeStub, "Found ID " + Arrays.toString(chaincodeStub.getCreator()));
            Reader pemReader = new StringReader(new String(idBytes.toByteArray()));
            PEMParser pemParser = new PEMParser(pemReader);
            X509CertificateHolder cert = (X509CertificateHolder) pemParser.readObject();
            pemParser.close();
            String cn = cert.getSubject().getRDNs(CN)[0].getFirst().getValue().toString();
            logInfo(chaincodeStub, "Found CN = " + cn);
            invokingAId = cn.substring(0, 5);
            logInfo(chaincodeStub, "Set invokingAId to " + invokingAId);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            invokingAIds.put(chaincodeStub, invokingAId);
        }
        ignoreBc.put(chaincodeStub, true);
        forceWriteToBc.put(chaincodeStub, false);
        TrackLayout trackLayout = new TrackLayout(chaincodeStub, TRACK_LAYOUT_JSON);
        trackLayouts.put(chaincodeStub, trackLayout);
        ignoreBc.put(chaincodeStub, false);
    }

    @Override
    public Response init(ChaincodeStub chaincodeStub) {
        // init no longer takes any parameters
        if (chaincodeStub.getParameters().size() != 0) {
            return newErrorResponse(INCORRECT_N_ARGS_MSG);
        }
        this.setup(chaincodeStub);
        forceWriteToBc.put(chaincodeStub, true);
        trackLayouts.get(chaincodeStub).writeDataToBc(chaincodeStub);
        return newSuccessResponse();
    }

    private void dumpInvocation(ChaincodeStub chaincodeStub) {
        String methodName = chaincodeStub.getFunction();
        logInfo(chaincodeStub, "Invoke function name received: " + methodName);
        List<String> paramList = chaincodeStub.getParameters();
        StringBuilder paramString = new StringBuilder();
        for (int i = 0; i < paramList.size(); i++) {
            paramString.append("\tValue of param ").append(i).append(" = ").append(paramList.get(i)).append("\n");
        }
        logInfo(chaincodeStub, "Params:\n" + paramString);
    }

    @Override
    public Response invoke(ChaincodeStub chaincodeStub) {
        logWarn(chaincodeStub, "\n\n======= NEW INVOCATION =======\n\n");
        this.setup(chaincodeStub);
        this.dumpInvocation(chaincodeStub);

        // Extract function and caller ID, prepare to execute
        String methodName = chaincodeStub.getFunction();
        List<String> paramList = chaincodeStub.getParameters();
        Response response;
        CC_FUNCTION function = CC_FUNCTION.fromString(methodName);
        invokedFunctions.put(chaincodeStub, function);
        String invokingAId = invokingAIds.get(chaincodeStub);
        // Check if we support this function with this many parameters, for this invoking identity:
        if (function == null) {
            // Nope :(
            return newErrorResponse(methodName + UNSUPPORTED_FUNCTION_MSG);
        } else if (paramList.size() != function.getNArgs()) {
            // Still nope :(
            return newErrorResponse(INCORRECT_N_ARGS_MSG);
        } else if (!function.mayBeInvoked(invokingAId, paramList)) {
            // Forbidden
            logWarn(chaincodeStub, "Invocation prohibited for aId " + invokingAId);
            return newErrorResponse(INVOCATION_PROHIBITED_MSG);
        }
        // We do :)
        // Read and update track layout from BC
        TrackLayout trackLayout = trackLayouts.get(chaincodeStub);
        //if (invokedFunctions.get(chaincodeStub).mustCalculateTrainRoutes()) {
        if (invokedFunctions.get(chaincodeStub) == UPDATE_TRAIN_DIRECTION || invokedFunctions.get(chaincodeStub) == SENSOR_TRIGGER || invokedFunctions.get(chaincodeStub) == QUERY_TRACK_LAYOUT) {
            for (Train train : trackLayout.getTrains()) {
                train.calculatePredictedTrack(chaincodeStub);
            }
        }
        try {
            response = this.executeMethodByString(chaincodeStub, methodName, paramList);
        } catch (InvocationTargetException e) {
            return newSuccessResponse(e.getTargetException().getMessage());
        } catch (Exception e) {
            return newErrorResponse(e.getMessage());
        }
        // Prepare sections and move trains if it is safe to do so
        //if (invokedFunctions.get(chaincodeStub).mustUpdateAssets()) {
        if (invokedFunctions.get(chaincodeStub) == SENSOR_TRIGGER) {
            logInfo(chaincodeStub, "Executed " + invokedFunctions.get(chaincodeStub).getName() + ", so will update assets!");
            trackLayout.prepareSectionsForReservees(chaincodeStub);
            trackLayout.updateTrainSpeeds(chaincodeStub);
            // Update signals
            trackLayout.updateSignals(chaincodeStub);
            // Check if no commands have been ignored for too long
//            trackLayout.checkCommandTimeouts(chaincodeStub);
            //logInfo(chaincodeStub, trackLayout.toString(chaincodeStub));
        }
        // Update track layout data on blockchain
        trackLayouts.get(chaincodeStub).writeDataToBc(chaincodeStub);
        // Send all events generated by this invoke
        this.sendEventQueue(chaincodeStub);
        assert (response != null);
        return response;
    }

    private Response executeMethodByString(ChaincodeStub chaincodeStub, String methodName, List<String> paramList) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {
        StringBuilder output = new StringBuilder("executeMethodByString(" + methodName);
        for (String param : paramList) {
            output.append(", ").append(param);
        }
        logInfo(chaincodeStub, output + ")");
        try {
            Method method = this.getClass().getDeclaredMethod(methodName, ChaincodeStub.class, List.class);
            method.setAccessible(true);
            return (Response) method.invoke(this, chaincodeStub, paramList);
        } catch (InvocationTargetException e) {
            // General exception for when something went wrong
            logError(chaincodeStub, e.getTargetException().getMessage());
            e.getTargetException().printStackTrace();
            // Return success anyways, since these things can happen and we don't need the client to crash
            throw e;
        } catch (Exception e) {
            // This should never happen!
            e.printStackTrace();
            throw e;
        }
    }

    // Turns a list of KeyValues into a more usable List of Pairs
    /*private LinkedList<Pair<CC_FUNCTION, LinkedList<String>>> parseInvocationPairList(LinkedList<KeyValue> kvPairList) {
        LinkedList<Pair<CC_FUNCTION, LinkedList<String>>> result = new LinkedList<>();
        for (KeyValue kvPair : kvPairList) {
            // Parse function, timestamp and txId from key
            CompositeKey key = CompositeKey.parseCompositeKey(kvPair.getKey());
            CC_FUNCTION function = fromString(key.getAttributes().get(0));
            Pair<CC_FUNCTION, LinkedList<String>> pair = new Pair<>(function, new LinkedList<>());
            // Parse params from value
            for (String param : parseCompositeString(kvPair.getStringValue())) {
                pair.getValue().add(param);
            }
            result.add(pair);
        }
        return result;
    }

    // Exposed, invokable functions

    @ChaincodeAPI
    private Response executeInvocations(ChaincodeStub chaincodeStub, List<String> paramList) {
        long lastInvocationExecutedTimestamp = getLastInvocationExecutedTimestamp(chaincodeStub);
        long lastTrackLayoutUpdateTimestamp = getLastTrackLayoutStateTimestamp(chaincodeStub);
        String lastInvocationExecutedTxId = getLastInvocationExecutedTxId(chaincodeStub);
        String lastTrackLayoutUpdateTxId = getLastTrackLayoutStateTxId(chaincodeStub);
        // Read trackLayout up until the correct point
        TrackLayout trackLayout = new TrackLayout(chaincodeStub, TRACK_LAYOUT_JSON);
        TrackLayout oldLayout;
        trackLayouts.put(chaincodeStub, trackLayout);
        trackLayout.forceReadDataFromBc(chaincodeStub, lastTrackLayoutUpdateTimestamp, lastTrackLayoutUpdateTxId);
        LOG.info("Reading track layout resulted in " + trackLayout);
        LinkedList<KeyValue> invocationsToHandleKvPairs = getUnhandledRegisteredInvocations(chaincodeStub, lastInvocationExecutedTimestamp, lastInvocationExecutedTxId);
        LinkedList<Pair<CC_FUNCTION, LinkedList<String>>> invocationsToHandle = parseInvocationPairList(invocationsToHandleKvPairs);

        // Execute transactions
        StringBuilder output = new StringBuilder("Results of invocations:\n");
        for (Pair<CC_FUNCTION, LinkedList<String>> invocationPair : invocationsToHandle) {
            output.append("\t").append(invocationPair.getKey().dumpInvocation(invocationPair.getValue())).append(": ");
            // Make a copy of the current track layout, and restore if an exception occurs during the invocation
            try {
                oldLayout = new TrackLayout(chaincodeStub, trackLayouts.get(chaincodeStub));
            } catch (TrackLayoutException e) {
                // Should never happen!
                throw new RuntimeException(e);
            }
            try {
                Response response = this.executeMethodByString(chaincodeStub, invocationPair.getKey().getName(), invocationPair.getValue());
                output.append(response.getMessage() == null ? "null" : response.getMessage());
            } catch (InvocationTargetException e) {
                LOG.error(e.getTargetException().getMessage());
                output.append(e.getTargetException().getMessage());
                trackLayouts.put(chaincodeStub, oldLayout);
            } catch (Exception e) {
                e.printStackTrace();
                LOG.error(e.getMessage());
                output.append(e.getMessage());
                trackLayouts.put(chaincodeStub, oldLayout);
            }
            output.append("\n");
        }

        // Update last executed transaction
        output.append("Transactions handled until timestamp ").append(CompositeKey.parseCompositeKey(invocationsToHandleKvPairs.getLast().getKey()).getAttributes().get(1)).append("\n");
        if (invocationsToHandleKvPairs.size() > 0) {
            updateLastExecutedTxDataToBc(chaincodeStub, invocationsToHandleKvPairs.getLast());
        }
        // Prepare sections and move trains if it is safe to do so
        trackLayout.prepareSectionsForReservees(chaincodeStub);
        trackLayout.updateTrainSpeeds(chaincodeStub);
        // Update signals
        trackLayout.updateSignals(chaincodeStub);
        // Check if no commands have been ignored for too long
        trackLayout.checkCommandTimeouts(chaincodeStub);
        // Update track layout data on blockchain
        trackLayouts.get(chaincodeStub).writeDataToBc(chaincodeStub);
        output.append("\tNew TrackLayout:\n").append(trackLayout.toString());
        logInfo(chaincodeStub, output.toString());
        return newSuccessResponse(output.toString());
    }*/

    // TESTING ONLY
    @ChaincodeAPI
    private Response queryHistory(ChaincodeStub chaincodeStub, List<String> paramList) {
        QueryResultsIterator<KeyModification> queryResultsIterator = chaincodeStub.getHistoryForKey(paramList.get(0));
        return newSuccessResponse(buildJsonFromQueryResult(queryResultsIterator));
    }

    // Helper, TESTING ONLY
    private String buildJsonFromQueryResult(QueryResultsIterator<KeyModification> queryResultsIterator) {
        JSONArray jsonArray = new JSONArray();
        queryResultsIterator.forEach(keyModification -> {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("transactionId", keyModification.getTxId());
            map.put("timestamp", keyModification.getTimestamp().toString());
            map.put("value", keyModification.getStringValue());
            map.put("isDeleted", keyModification.isDeleted());
            jsonArray.put(map);
        });

        JSONObject jsonObject = new JSONObject();
        jsonObject.accumulate("transactions", jsonArray);
        return jsonObject.toString();
    }

    private String buildJsonFromCompositeQueryResult(QueryResultsIterator<KeyValue> queryResultsIterator) {
        JSONArray jsonArray = new JSONArray();
        queryResultsIterator.forEach(kvPair -> {
            Map<String, Object> map = new LinkedHashMap<>();
            CompositeKey key = CompositeKey.parseCompositeKey(kvPair.getKey());
            map.put("keyObjectType", key.getObjectType());
            JSONArray attrArray = new JSONArray();
            for (String attr : key.getAttributes()) {
                attrArray.put(attr);
            }
            map.put("attributes", attrArray);
            map.put("value", kvPair.getStringValue());
            jsonArray.put(map);
        });
        JSONObject jsonObject = new JSONObject();
        jsonObject.accumulate("transactions", jsonArray);
        return jsonObject.toString();
    }

    // TESTING ONLY
    @ChaincodeAPI
    private Response queryTestVar(ChaincodeStub chaincodeStub, List<String> paramList) throws BlockchainDataNotFoundException {
        return newSuccessResponse(getStringStateWrapper(chaincodeStub, paramList.get(0)));
    }

    // TESTING ONLY
    @ChaincodeAPI
    private Response setTestVar(ChaincodeStub chaincodeStub, List<String> paramList) {
        String varName = paramList.get(0);
        String newValue = paramList.get(1);
        CompositeKey c = new CompositeKey(varName, newValue, chaincodeStub.getTxId(), String.valueOf(timestamps.get(chaincodeStub)));
        putStringStateWrapper(chaincodeStub, c.toString(), newValue);
        return newSuccessResponse(buildJsonFromCompositeQueryResult(chaincodeStub.getStateByPartialCompositeKey(new CompositeKey(varName))));
    }

    // TESTING ONLY
    @ChaincodeAPI
    private Response checkRegisteredInvocations(ChaincodeStub chaincodeStub, List<String> paramList) {
        CompositeKey partialKey = new CompositeKey(INVOCATION_REGISTRATION_KEY);
        return newSuccessResponse(buildJsonFromCompositeQueryResult(chaincodeStub.getStateByPartialCompositeKey(partialKey)));
    }

    // Stop clients
    @ChaincodeAPI
    private Response killAllClients(ChaincodeStub chaincodeStub, List<String> paramList) {
        this.stopAllClients(chaincodeStub);
        return newSuccessResponse();
    }

    // Query functions
    @ChaincodeAPI
    private Response queryLevelCrossingState(ChaincodeStub chaincodeStub, List<String> paramList) throws LevelCrossingNotFoundException {
        String levelCrossingAssetId = paramList.get(0);
        LevelCrossing levelCrossing = trackLayouts.get(chaincodeStub).getLevelCrossingByAId(levelCrossingAssetId);
        return newSuccessResponse(levelCrossing.getCurrentBoomsState(chaincodeStub).toString());
    }

    @ChaincodeAPI
    private Response querySectionReservee(ChaincodeStub chaincodeStub, List<String> paramList) throws SectionNotFoundException {
        String sectionAssetId = paramList.get(0);
        Section section = trackLayouts.get(chaincodeStub).getSectionByAId(sectionAssetId);
        Train reservee = section.getReservee(chaincodeStub);
        return newSuccessResponse(reservee == null ? "none" : reservee.getAssetId());
    }

    @ChaincodeAPI
    private Response querySectionQueue(ChaincodeStub chaincodeStub, List<String> paramList) throws SectionNotFoundException {
        String sectionAssetId = paramList.get(0);
        Section section = trackLayouts.get(chaincodeStub).getSectionByAId(sectionAssetId);
        return newSuccessResponse(section.getWaitingQueueAsString(chaincodeStub));
    }

    @ChaincodeAPI
    private Response querySectionWeight(ChaincodeStub chaincodeStub, List<String> paramList) throws SectionNotFoundException {
        String sectionAssetId = paramList.get(0);
        Section section = trackLayouts.get(chaincodeStub).getSectionByAId(sectionAssetId);
        return newSuccessResponse(section.getWeight(chaincodeStub).toString());
    }

    @ChaincodeAPI
    private Response querySignalAspect(ChaincodeStub chaincodeStub, List<String> paramList) throws SignalNotFoundException {
        String signalAssetId = paramList.get(0);
        Signal signal = trackLayouts.get(chaincodeStub).getSignalByAId(signalAssetId);
        return newSuccessResponse(signal.getCurrentAspect(chaincodeStub).toString());
    }

    @ChaincodeAPI
    private Response querySwitchState(ChaincodeStub chaincodeStub, List<String> paramList) throws SwitchNotFoundException {
        String switchAssetId = paramList.get(0);
        Switch sw = trackLayouts.get(chaincodeStub).getSwitchByAId(switchAssetId);
        return newSuccessResponse(sw.getCurrentState(chaincodeStub).toString());
    }

    @ChaincodeAPI
    private Response queryTrackLayout(ChaincodeStub chaincodeStub, List<String> paramList) {
        return newSuccessResponse(trackLayouts.get(chaincodeStub).toString(chaincodeStub));
    }

    @ChaincodeAPI
    private Response queryTrainDestination(ChaincodeStub chaincodeStub, List<String> paramList) throws TrainNotFoundException {
        String trainAssetId = paramList.get(0);
        Train train = trackLayouts.get(chaincodeStub).getTrainByAId(trainAssetId);
        return newSuccessResponse(train.getDestinationSection(chaincodeStub).getAssetId());
    }

    @ChaincodeAPI
    private Response queryTrainDirection(ChaincodeStub chaincodeStub, List<String> paramList) throws TrainNotFoundException {
        String trainAssetId = paramList.get(0);
        Train train = trackLayouts.get(chaincodeStub).getTrainByAId(trainAssetId);
        return newSuccessResponse(train.getCurrentDirection(chaincodeStub).toString());
    }

    @ChaincodeAPI
    private Response queryTrainInitialised(ChaincodeStub chaincodeStub, List<String> paramList) throws TrainNotFoundException {
        String trainAssetId = paramList.get(0);
        Train train = trackLayouts.get(chaincodeStub).getTrainByAId(trainAssetId);
        return newSuccessResponse(String.valueOf(train.isInitialised(chaincodeStub)));
    }

    @ChaincodeAPI
    private Response queryTrainPosition(ChaincodeStub chaincodeStub, List<String> paramList) throws TrainNotFoundException, TrainPositionNotFoundException {
        String trainAssetId = paramList.get(0);
        Train train = trackLayouts.get(chaincodeStub).getTrainByAId(trainAssetId);
        return newSuccessResponse(getRawTrainPositionFromBC(chaincodeStub, train));
    }

    @ChaincodeAPI
    private Response queryTrainSpeed(ChaincodeStub chaincodeStub, List<String> paramList) throws TrainNotFoundException {
        String trainAssetId = paramList.get(0);
        Train train = trackLayouts.get(chaincodeStub).getTrainByAId(trainAssetId);
        return newSuccessResponse(train.getCurrentSpeed(chaincodeStub).toString());
    }

    // Commands from SCC
    @ChaincodeAPI
    private Response emergencyStop(ChaincodeStub chaincodeStub, List<String> paramList) {
        emergencyStopTrains(chaincodeStub);
        return newSuccessResponse();
    }

    @ChaincodeAPI
    private Response setLevelCrossingState(ChaincodeStub chaincodeStub, List<String> paramList) throws JSONException, AssetNotFoundException, InvalidLevelCrossingStateException {
        String levelCrossingAssetId = paramList.get(0);
        String state = paramList.get(1);
        LevelCrossing levelCrossing = trackLayouts.get(chaincodeStub).getLevelCrossingByAId(levelCrossingAssetId);
        LC_STATE lcState = LC_STATE.fromString(state);
        levelCrossing.initBoomsStateChange(chaincodeStub, lcState, true);
        return newSuccessResponse();
    }

    @ChaincodeAPI
    private Response setSectionWeight(ChaincodeStub chaincodeStub, List<String> paramList) throws SectionNotFoundException, InvalidSectionWeightException {
        String sectionAssetId = paramList.get(0);
        String sectionWeightStr = paramList.get(1);
        SECTION_WEIGHT sectionWeight = SECTION_WEIGHT.fromString(sectionWeightStr);
        Section section = trackLayouts.get(chaincodeStub).getSectionByAId(sectionAssetId);
        section.setWeight(chaincodeStub, sectionWeight);
        return newSuccessResponse();
    }

    @ChaincodeAPI
    private Response setSignalAspect(ChaincodeStub chaincodeStub, List<String> paramList) throws JSONException, AssetNotFoundException, InvalidSignalAspectException {
        String signalAssetId = paramList.get(0);
        String aspectStr = paramList.get(1);
        Signal signal = trackLayouts.get(chaincodeStub).getSignalByAId(signalAssetId);
        SIGNAL_ASPECT aspect = SIGNAL_ASPECT.fromString(aspectStr);
        signal.initAspectChange(chaincodeStub, aspect, true);
        return newSuccessResponse();
    }

    @ChaincodeAPI
    private Response setSwitchState(ChaincodeStub chaincodeStub, List<String> paramList) throws JSONException, AssetNotFoundException, InvalidSwitchStateException {
        String switchAssetId = paramList.get(0);
        String state = paramList.get(1);
        Switch sw = trackLayouts.get(chaincodeStub).getSwitchByAId(switchAssetId);
        SWITCH_STATE switchState = SWITCH_STATE.fromString(state);
        sw.initSwitchStateChange(chaincodeStub, switchState, true);
        return newSuccessResponse();
    }

    @ChaincodeAPI
    private Response initTrain(ChaincodeStub chaincodeStub, List<String> paramList) throws TrainNotFoundException {
        String trainAssetId = paramList.get(0);
        Train train = trackLayouts.get(chaincodeStub).getTrainByAId(trainAssetId);
        train.sendInitCommand(chaincodeStub);
        return newSuccessResponse();
    }

    @ChaincodeAPI
    private Response setTrainDestination(ChaincodeStub chaincodeStub, List<String> paramList) throws AssetNotFoundException {
        String trainAssetId = paramList.get(0);
        String sectionAssetId = paramList.get(1);
        TrackLayout trackLayout = trackLayouts.get(chaincodeStub);
        Train train = trackLayout.getTrainByAId(trainAssetId);
        Section destination = trackLayout.getSectionByAId(sectionAssetId);
        train.setDestinationSection(chaincodeStub, destination);
        return newSuccessResponse();
    }

    @ChaincodeAPI
    private Response setTrainPosition(ChaincodeStub chaincodeStub, List<String> paramList) throws JSONException, TrackLayoutException {
        String trainAssetId = paramList.get(0);
        String sectionsPassedData = paramList.get(1);
        String currentSectionAssetId = paramList.get(2);

        TrackLayout trackLayout = trackLayouts.get(chaincodeStub);
        Train train = trackLayout.getTrainByAId(trainAssetId);
        Section current = trackLayout.getSectionByAId(currentSectionAssetId);

        String[] sectionsPassedAssetIds = parseCompositeString(sectionsPassedData);
        if (sectionsPassedAssetIds.length == 1) {
            Section last = trackLayout.getSectionByAId(sectionsPassedAssetIds[0]);
            train.position(chaincodeStub, last, current);
        } else {
            LinkedList<Section> sectionsPassed = new LinkedList<>();
            for (String sectionAssetId : sectionsPassedAssetIds) {
                // Reverse order because trains store passed sections in reverse order
                sectionsPassed.addFirst(trackLayout.getSectionByAId(sectionAssetId));
            }
            train.position(chaincodeStub, sectionsPassed, current);
        }
        return newSuccessResponse();
    }

    @ChaincodeAPI
    private Response setTrainDirection(ChaincodeStub chaincodeStub, List<String> paramList) throws JSONException, AssetNotFoundException, InvalidTrainDirectionException {
        String trainAssetId = paramList.get(0);
        String directionStr = paramList.get(1);
        Train train = trackLayouts.get(chaincodeStub).getTrainByAId(trainAssetId);
        TRAIN_DIRECTION direction = TRAIN_DIRECTION.fromString(directionStr);
        train.initDirectionChange(chaincodeStub, direction, true);
        return newSuccessResponse();
    }

    @ChaincodeAPI
    private Response setTrainSpeed(ChaincodeStub chaincodeStub, List<String> paramList) throws JSONException, AssetNotFoundException, InvalidTrainSpeedException {
        String trainAssetId = paramList.get(0);
        String speedStr = paramList.get(1);
        Train train = trackLayouts.get(chaincodeStub).getTrainByAId(trainAssetId);
        TRAIN_SPEED speed = TRAIN_SPEED.fromString(speedStr);
        train.initSpeedChange(chaincodeStub, speed, true);
        return newSuccessResponse();
    }


    // Feedback from clients
    @ChaincodeAPI
    private Response levelCrossingStateUpdate(ChaincodeStub chaincodeStub, List<String> paramList) throws JSONException, AssetNotFoundException, InvalidLevelCrossingStateException {
        String levelCrossingAssetId = paramList.get(0);
        String state = paramList.get(1);
        LevelCrossing lc = trackLayouts.get(chaincodeStub).getLevelCrossingByAId(levelCrossingAssetId);
        LC_STATE lcState = LC_STATE.fromString(state);
        lc.updateBoomsState(chaincodeStub, lcState);
        return newSuccessResponse();
    }

    @ChaincodeAPI
    private Response sensorTrigger(ChaincodeStub chaincodeStub, List<String> paramList) throws JSONException, TrainManagementException, TrackLayoutException {
        String sensorAssetId = paramList.get(0);
        TrackLayout trackLayout = trackLayouts.get(chaincodeStub);
        Sensor sensor = trackLayout.getSensorByAId(sensorAssetId);
        Train train;
        try {
            train = trackLayout.findTrainForSensorTrigger(chaincodeStub, sensor);
        } catch (MagnetMyrtleException | SensorReadingConflictException e) {
            emergencyStopTrains(chaincodeStub);
            throw e;
        }
        // If this train should not have moved, it must be out of control - stop all trains
        // Both the current and intended speed are checked, because otherwise there may be a good reason for the train moving
        if (!IGNORE_EMERGENCY_STOPS && (train.getCurrentSpeed(chaincodeStub).equals(TRAIN_SPEED.getStopSpeed()) && train.getIntendedSpeed(chaincodeStub).equals(TRAIN_SPEED.getStopSpeed()))) {
            emergencyStopTrains(chaincodeStub);
        } else {
            train.processSensorReading(chaincodeStub, sensor);
        }
        queueSensorTriggerEvent(chaincodeStub, sensor);
        return newSuccessResponse();
    }

    @ChaincodeAPI
    private Response signalAspectUpdate(ChaincodeStub chaincodeStub, List<String> paramList) throws SignalNotFoundException, InvalidSignalAspectException {
        String signalAssetId = paramList.get(0);
        String aspectStr = paramList.get(1);
        Signal signal = trackLayouts.get(chaincodeStub).getSignalByAId(signalAssetId);
        SIGNAL_ASPECT aspect = SIGNAL_ASPECT.fromString(aspectStr);
        //if (invokedFunctions.get(chaincodeStub) == EXECUTE_INVOCATIONS) {
        signal.updateAspect(chaincodeStub, aspect);
//        }
        return newSuccessResponse();
    }

    @ChaincodeAPI
    private Response switchStateUpdate(ChaincodeStub chaincodeStub, List<String> paramList) throws SwitchNotFoundException, InvalidSwitchStateException {
        String switchAssetId = paramList.get(0);
        String stateStr = paramList.get(1);
        Switch sw = trackLayouts.get(chaincodeStub).getSwitchByAId(switchAssetId);
        SWITCH_STATE switchState = SWITCH_STATE.fromString(stateStr);
        //if (invokedFunctions.get(chaincodeStub) == EXECUTE_INVOCATIONS) {
        sw.updateState(chaincodeStub, switchState);
//        }
        return newSuccessResponse();
    }

    @ChaincodeAPI
    private Response trainDirectionUpdate(ChaincodeStub chaincodeStub, List<String> paramList) throws TrainNotFoundException, InvalidTrainDirectionException {
        String trainAssetId = paramList.get(0);
        String directionStr = paramList.get(1);
        Train train = trackLayouts.get(chaincodeStub).getTrainByAId(trainAssetId);
        TRAIN_DIRECTION direction = TRAIN_DIRECTION.fromString(directionStr);
        //if (invokedFunctions.get(chaincodeStub) == EXECUTE_INVOCATIONS) {
            train.updateDirection(chaincodeStub, direction);
//        }
        return newSuccessResponse();
    }

    @ChaincodeAPI
    private Response trainInitConfirm(ChaincodeStub chaincodeStub, List<String> paramList) throws TrainNotFoundException {
        String trainAssetId = paramList.get(0);
        Train train = trackLayouts.get(chaincodeStub).getTrainByAId(trainAssetId);
        //if (invokedFunctions.get(chaincodeStub) == EXECUTE_INVOCATIONS) {
        train.updateSetInitialised(chaincodeStub);
//        }
        return newSuccessResponse();
    }

    @ChaincodeAPI
    private Response trainSpeedUpdate(ChaincodeStub chaincodeStub, List<String> paramList) throws TrainNotFoundException, InvalidTrainSpeedException {
        String trainAssetId = paramList.get(0);
        String speedStr = paramList.get(1);
        Train train = trackLayouts.get(chaincodeStub).getTrainByAId(trainAssetId);
        TRAIN_SPEED speed = TRAIN_SPEED.fromString(speedStr);
        train.updateSpeed(chaincodeStub, speed);
        return newSuccessResponse();
    }

    // Internal helper functions

    private static void emergencyStopTrains(ChaincodeStub chaincodeStub) {
        if (IGNORE_EMERGENCY_STOPS) {
            return;
        }
        queueEmergencyStopEvent(chaincodeStub);
        for (Train train : trackLayouts.get(chaincodeStub).getTrains()) {
            // At this point we don't care anymore, just try stopping the train regardless of whether or not it is initialised
            boolean wasInitialised = train.isInitialised(chaincodeStub);
            train.setInitialised(chaincodeStub, true);
            train.stop(chaincodeStub);
            train.setInitialised(chaincodeStub, wasInitialised);
        }
    }

    private void stopAllClients(ChaincodeStub chaincodeStub) {
        this.queueEvent(chaincodeStub, KILL_EVENT, "");
    }

    void queueEvent(ChaincodeStub chaincodeStub, String eventName, String contents) {
        JSONObject jsonContents = new JSONObject();
        jsonContents.put("contents", contents);
        this.queueEvent(chaincodeStub, eventName, jsonContents);
    }

    // Adds a new event to the queue; removes any previously queued events with the same name
    public void queueEvent(ChaincodeStub chaincodeStub, String eventName, JSONObject contents) {
        LinkedList<Pair<String, JSONObject>> toRemove = new LinkedList<>();
        ConcurrentLinkedQueue<Pair<String, JSONObject>> eventQueue = this.eventQueues.get(chaincodeStub);
        for (Pair<String, JSONObject> pair : eventQueue) {
            if (pair.getKey().equals(eventName)) {
                toRemove.add(pair);
            }
        }
        for (Pair<String, JSONObject> pair : toRemove) {
            logInfo(chaincodeStub, "Removing outdated event " + pair.getKey() + " (" + pair.getValue().toString() + ")");
            eventQueue.remove(pair);
        }
        logInfo(chaincodeStub, "Queueing " + eventName + " (" + contents.toString() + ")");
        eventQueue.add(new Pair<>(eventName, contents));
    }

    // Empties and subsequently removes the current event queue and creates/sets a composite event
    private void sendEventQueue(ChaincodeStub chaincodeStub) {
        JSONArray eventsArrayJson = new JSONArray();
        ConcurrentLinkedQueue<Pair<String, JSONObject>> eventQueue = this.eventQueues.get(chaincodeStub);
        while (eventQueue.size() > 0) {
            Pair<String, JSONObject> eventPair = eventQueue.poll();
            JSONObject eventJson = new JSONObject();
            eventJson.put(eventPair.getKey(), eventPair.getValue());
            eventsArrayJson.put(eventJson);
        }
        if (eventsArrayJson.length() == 0) {
            return;
        }
        JSONObject compositeEventJson = new JSONObject();
        compositeEventJson.put("txId", chaincodeStub.getTxId());
        compositeEventJson.put("txTimestamp", timestamps.get(chaincodeStub));
        compositeEventJson.put("events", eventsArrayJson);
        logInfo(chaincodeStub, "Setting event: " + COMPOSITE_EVENT + " (" + compositeEventJson.toString() + ")");
        chaincodeStub.setEvent(COMPOSITE_EVENT, compositeEventJson.toString().getBytes(UTF_8));
        this.eventQueues.remove(chaincodeStub);
    }

    public static void logInfo(ChaincodeStub chaincodeStub, String s) {
        LOG.info("[" + chaincodeStub.getTxId() + "] " + s);
    }

    public static void logWarn(ChaincodeStub chaincodeStub, String s) {
        LOG.warn("[" + chaincodeStub.getTxId() + "] " + s);
    }

    private static void logError(ChaincodeStub chaincodeStub, String s) {
        LOG.error("[" + chaincodeStub.getTxId() + "] " + s);
    }

    public static void main(String[] args) {
        new BlocktrainCC().start(args);
    }
}
