package chaincode.blockchainVariables;

import org.hyperledger.fabric.shim.ChaincodeStub;
import trackTopology.Section;
import trackTopology.TrackLayout;
import trackTopology.Train;
import trackTopology.TrainPosition;
import trackTopology.errors.blockchain.unchecked.invaliddata.BlockchainInvalidDataException;
import trackTopology.errors.blockchain.unchecked.invaliddata.InvalidTrainPositionOnBCException;
import trackTopology.errors.tracklayout.assetnotfound.SectionNotFoundException;

import java.util.LinkedList;

import static chaincode.BlocktrainCC.trackLayouts;
import static shared.GlobalConfig.*;

public class TrainPositionVariable extends BlockchainVariable<TrainPosition> {
    private Train train;

    public TrainPositionVariable(Train train) {
        super();
        this.train = train;
        this.value = new TrainPosition();
    }

    public TrainPositionVariable(TrainPositionVariable old) {
        super(old);
        this.train = old.train;
        this.value = new TrainPosition(old.value);
    }

    TrainPosition fromBcString(ChaincodeStub chaincodeStub, String bcString) {
        String[] position = parseCompositeString(bcString);
        if (position.length != (TRAIN_N_PASSED_SECTIONS_TRACKED + 1)) {
            throw new InvalidTrainPositionOnBCException(bcString);
        }
        LinkedList<Section> result = new LinkedList<>();
        TrackLayout trackLayout = trackLayouts.get(chaincodeStub);
        try {
            for (String s : position) {
                result.add(trackLayout.getSectionByAId(s));
            }
        } catch (SectionNotFoundException e) {
            throw new BlockchainInvalidDataException(e);
        }
        // Take and remove first element, which is the current section
        Section current = result.pollFirst();
        return new TrainPosition(result, current);
    }

    String toBcString(TrainPosition trainPosition) {
        assert (trainPosition != null);
        return createCompositeString(trainPosition.toStringArray());
    }

    public boolean mustUpdateBc(ChaincodeStub chaincodeStub) {
        return super.mustUpdateBc(chaincodeStub);
    }

    void afterBcUpdate(ChaincodeStub chaincodeStub) {
        this.train.setPositionAndReserve(chaincodeStub, this.value.getSectionsPassed(), this.value.getCurrentSection());
    }

    public void setCurrentSection(ChaincodeStub chaincodeStub, Section currentSection) {
        this.getValue(chaincodeStub).setCurrentSection(currentSection);
    }

    public void setSectionsPassed(ChaincodeStub chaincodeStub, LinkedList<Section> sectionsPassed) {
        this.getValue(chaincodeStub).setSectionsPassed(sectionsPassed);
    }

    TrainPosition copyValue(TrainPosition value) {
        return new TrainPosition(value);
    }

    String getKey() {
        return this.train.getAssetId() + TRAIN_POSITION_KEY_POSTFIX;
    }
}
