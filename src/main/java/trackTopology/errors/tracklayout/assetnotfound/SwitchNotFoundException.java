package trackTopology.errors.tracklayout.assetnotfound;

public class SwitchNotFoundException extends AssetNotFoundException {
    public SwitchNotFoundException(String aId) {
        super("Switch not found: " + aId);
    }
}
