package trackTopology.errors.runtime.unchecked;

// Exception that really should not occur and don't require special precautions don't need to be checked
public class TrainManagementRuntimeException extends RuntimeException {
    public TrainManagementRuntimeException() {
        super();
    }

    public TrainManagementRuntimeException(Throwable cause) {
        super(cause);
    }

    TrainManagementRuntimeException(String message) {
        super("Train management runtime exception: " + message);
    }

    public TrainManagementRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
