package shared.assetdata;

import shared.errors.InvalidSectionWeightException;

import static shared.GlobalConfig.SECTION_CLOSED_WEIGHT;

public class SectionConfig {
    public static class SECTION_WEIGHT {
        private final int weightInt;

        private SECTION_WEIGHT(int weight) {
            this.weightInt = weight;
        }

        private SECTION_WEIGHT(String weightStr) throws InvalidSectionWeightException {
            try {
                this.weightInt = Integer.parseInt(weightStr);
            } catch (NumberFormatException e) {
                throw new InvalidSectionWeightException(weightStr);
            }
        }

        public static SECTION_WEIGHT getClosedWeight() {
            return new SECTION_WEIGHT(SECTION_CLOSED_WEIGHT);
        }

        public static SECTION_WEIGHT fromInt(int weight) {
            return new SECTION_WEIGHT(weight);
        }

        public static SECTION_WEIGHT fromString(String weightStr) throws InvalidSectionWeightException {
            return new SECTION_WEIGHT(weightStr);
        }

        public double toInt() {
            return this.weightInt;
        }

        public String toString() {
            return String.valueOf(this.weightInt);
        }

        public boolean equals(SECTION_WEIGHT other) {
            return this.weightInt == other.weightInt;
        }

        public boolean equals(Object o) {
            return (o instanceof SECTION_WEIGHT && this.equals((SECTION_WEIGHT) o));
        }
    }
}
