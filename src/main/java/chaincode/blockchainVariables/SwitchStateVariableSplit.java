package chaincode.blockchainVariables;

import org.hyperledger.fabric.shim.ChaincodeStub;
import shared.assetdata.SwitchConfig.SWITCH_STATE;
import shared.errors.InvalidSwitchStateException;
import trackTopology.Switch;
import trackTopology.errors.blockchain.unchecked.invaliddata.InvalidSwitchStateOnBCException;

import static shared.GlobalConfig.ASSET_INTENDED_STATE_POSTFIX;
import static shared.GlobalConfig.SWITCH_STATE_KEY_POSTFIX;

public class SwitchStateVariableSplit extends BlockchainVariable<SWITCH_STATE> {
    private Switch sw;
    private boolean isIntended;

    public SwitchStateVariableSplit(Switch sw) {
        this(sw, false);
    }

    public SwitchStateVariableSplit(Switch sw, boolean isIntended) {
        super();
        this.sw = sw;
        this.isIntended = isIntended;
    }

    public SwitchStateVariableSplit(SwitchStateVariableSplit old) {
        super(old);
        this.sw = old.sw;
    }

    SWITCH_STATE fromBcString(ChaincodeStub chaincodeStub, String bcString) {
        try {
            return SWITCH_STATE.fromString(bcString);
        } catch (InvalidSwitchStateException e) {
            throw new InvalidSwitchStateOnBCException(bcString);
        }
    }

    String toBcString(SWITCH_STATE aspect) {
        return aspect.toString();
    }

    String getKey() {
        return this.sw.getAssetId() + SWITCH_STATE_KEY_POSTFIX + (this.isIntended ? ASSET_INTENDED_STATE_POSTFIX : "");
    }
}
