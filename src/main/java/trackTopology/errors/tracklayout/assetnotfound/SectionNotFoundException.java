package trackTopology.errors.tracklayout.assetnotfound;

public class SectionNotFoundException extends AssetNotFoundException {
    public SectionNotFoundException(String aId) {
        super("Section not found: " + aId);
    }
}
