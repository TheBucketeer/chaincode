package chaincode.blockchainVariables;

import org.hyperledger.fabric.shim.ChaincodeStub;
import shared.assetdata.LevelCrossingConfig.LC_STATE;
import shared.errors.InvalidLevelCrossingStateException;
import trackTopology.LevelCrossing;
import trackTopology.errors.blockchain.unchecked.invaliddata.InvalidLevelCrossingStateOnBCException;

import static shared.GlobalConfig.ASSET_INTENDED_STATE_POSTFIX;
import static shared.GlobalConfig.LEVEL_CROSSING_KEY_POSTFIX;

public class LevelCrossingStateVariableSplit extends BlockchainVariable<LC_STATE> {
    private LevelCrossing levelCrossing;
    private boolean isIntended;

    public LevelCrossingStateVariableSplit(LevelCrossing levelCrossing) {
        this(levelCrossing, false);
    }

    public LevelCrossingStateVariableSplit(LevelCrossing levelCrossing, boolean isIntended) {
        super();
        this.levelCrossing = levelCrossing;
        this.isIntended = isIntended;
    }

    public LevelCrossingStateVariableSplit(LevelCrossingStateVariableSplit old) {
        super(old);
        this.levelCrossing = old.levelCrossing;
    }

    LC_STATE fromBcString(ChaincodeStub chaincodeStub, String bcString) {
        try {
            return LC_STATE.fromString(bcString);
        } catch (InvalidLevelCrossingStateException e) {
            throw new InvalidLevelCrossingStateOnBCException(bcString);
        }
    }

    String toBcString(LC_STATE state) {
        return state.toString();
    }

    String getKey() {
        return this.levelCrossing.getAssetId() + LEVEL_CROSSING_KEY_POSTFIX + (this.isIntended ? ASSET_INTENDED_STATE_POSTFIX : "");
    }
}
