package trackTopology.errors.blockchain.checked.datanotfound;

import trackTopology.Signal;

public class SignalAspectNotFoundException extends BlockchainDataNotFoundException {
    public SignalAspectNotFoundException(Signal signal) {
        super("Aspect of " + signal.getAssetId() + " not found on blockchain.");
    }
}
