package trackTopology.errors.runtime.unchecked;

import trackTopology.Train;

public class TrainNotReadyException extends TrainManagementRuntimeException {
    public TrainNotReadyException(Train train) {
        super("Train not initialised: " + train.getAssetId());
    }
}
