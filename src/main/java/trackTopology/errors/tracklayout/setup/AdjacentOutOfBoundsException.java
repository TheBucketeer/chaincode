package trackTopology.errors.tracklayout.setup;

import trackTopology.errors.tracklayout.TrackLayoutException;

public class AdjacentOutOfBoundsException extends TrackLayoutException {
    public AdjacentOutOfBoundsException(int aS0, int aS1, int total) {
        super(total > 3 ? "Too many adjacent sections (" + total + ")" :
                (aS0 > 2 ? "Too many adjacent sections on side 0 (" + aS0 + ")" :
                        (aS1 > 2 ? "Too many adjacent sections on side 1 (" + aS1 + ")"
                                : "Not enough adjacent sections (" + total + ")")));
    }
}
