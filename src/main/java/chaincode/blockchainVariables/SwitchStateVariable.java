package chaincode.blockchainVariables;

import chaincode.DualVariable;
import org.hyperledger.fabric.shim.ChaincodeStub;
import shared.assetdata.SwitchConfig.SWITCH_STATE;
import shared.errors.InvalidSwitchStateException;
import trackTopology.Switch;
import trackTopology.errors.blockchain.checked.InvalidDualVariableDataOnBCException;
import trackTopology.errors.blockchain.unchecked.invaliddata.InvalidSwitchStateOnBCException;

import java.lang.reflect.InvocationTargetException;

import static shared.GlobalConfig.SWITCH_STATE_KEY_POSTFIX;


public class SwitchStateVariable extends DualBlockchainVariable<SWITCH_STATE> {
    private Switch sw;

    public SwitchStateVariable(Switch sw) {
        super();
        this.sw = sw;
    }

    public SwitchStateVariable(SwitchStateVariable switchStateVariable) {
        super(switchStateVariable);
        this.sw = switchStateVariable.sw;
    }

    DualVariable<SWITCH_STATE> fromBcString(ChaincodeStub chaincodeStub, String bcStringCurrent, String bcStringIntended) {
        try {
            return getDualVariableFromBcString(SWITCH_STATE.class, bcStringCurrent, bcStringIntended);
        } catch (InvalidDualVariableDataOnBCException e) {
            throw new InvalidSwitchStateOnBCException(e.getCurrent(), e.getIntended());
        } catch (InvocationTargetException e) {
            if (e.getCause() instanceof InvalidSwitchStateException) {
                InvalidSwitchStateException cause = (InvalidSwitchStateException) e.getCause();
                throw new InvalidSwitchStateOnBCException(cause.getStateStr());
            } else {
                throw new IllegalStateException(e.getCause());
            }
        }
    }

    String getKey() {
        return this.sw.getAssetId() + SWITCH_STATE_KEY_POSTFIX;
    }
}
