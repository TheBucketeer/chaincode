package chaincode.blockchainVariables;

import org.hyperledger.fabric.shim.ChaincodeStub;
import trackTopology.Section;
import trackTopology.Train;
import trackTopology.errors.blockchain.unchecked.invaliddata.BlockchainInvalidDataException;
import trackTopology.errors.tracklayout.assetnotfound.TrainNotFoundException;

import static chaincode.BlocktrainCC.trackLayouts;
import static shared.GlobalConfig.EMPTY_KEY_STATE;
import static shared.GlobalConfig.SECTION_RESERVED_FOR_KEY_POSTFIX;

public class SectionReserveeVariable extends BlockchainVariable<Train> {
    private Section section;

    public SectionReserveeVariable(Section section) {
        super();
        this.section = section;
    }

    Train fromBcString(ChaincodeStub chaincodeStub, String bcString) {
        try {
            return bcString.equals(EMPTY_KEY_STATE) ? null : trackLayouts.get(chaincodeStub).getTrainByAId(bcString);
        } catch (TrainNotFoundException e) {
            throw new BlockchainInvalidDataException(e);
        }
    }

    String toBcString(Train train) {
        return train == null ? EMPTY_KEY_STATE : train.getAssetId();
    }

    String getKey() {
        return this.section.getAssetId() + SECTION_RESERVED_FOR_KEY_POSTFIX;
    }
}
